<?php
class helper
{
	public static function randomKey()
    {
        $crypt = new CkCrypt2() ;
    
        $success = $crypt->UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
        if ($success != true) {
            printf("%s\n",$crypt->lastErrorText());
            return "";
        }
    
        $crypt->put_CryptAlgorithm("aes");
        $crypt->put_CipherMode("cbc");
        $crypt->put_KeyLength(256);
        $crypt->put_PaddingScheme(0);
        $crypt->put_EncodingMode("hex");
    
        return $crypt->genRandomBytesENC(32);
    }
	
	public static function encrypt($str)
	{
		$crypt = new CkCrypt2();

		$success = $crypt->UnlockComponent('WILLAWCrypt_KM8tJZPHMRLn');
		if ($success != true) {
		    print $crypt->lastErrorText() . "\n";
		    return "";
		}

		$crypt->put_CryptAlgorithm('aes');
		$crypt->put_CipherMode('cbc');
		$crypt->put_KeyLength(256);
		$crypt->put_PaddingScheme(0);
		$crypt->put_EncodingMode('hex');
		$ivHex = '000102030405060708090A0B0C0D0E0F';
		$crypt->SetEncodedIV($ivHex,'hex');

		$keyHex = '000102030405060709080A0B0C0D0E0F101122131515161718191A1B1C1D1E1F';
		$crypt->SetEncodedKey($keyHex,'hex');

		$encStr = $crypt->encryptStringENC($str);
		return $encStr;
	}
	
	
	public static function createCookie()
	{
		$crypt = new CkCrypt2() ;

	    $success = $crypt->UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
	    if ($success != true) {
	        printf("%s\n",$crypt->lastErrorText());
	        return "";
	    }

		$crypt->put_CryptAlgorithm("aes");
	    $crypt->put_CipherMode("cbc");
	    $crypt->put_KeyLength(256);
	    $crypt->put_PaddingScheme(0);
	    $crypt->put_EncodingMode("hex");

    	return $crypt->genRandomBytesENC(32);
	}
	
	
	
	public static function decrypt($str)
	{
		$crypt = new CkCrypt2();

		$success = $crypt->UnlockComponent('WILLAWCrypt_KM8tJZPHMRLn');
		if ($success != true) {
		    print $crypt->lastErrorText() . "\n";
		    return "";
		}

		$crypt->put_CryptAlgorithm('aes');
		$crypt->put_CipherMode('cbc');
		$crypt->put_KeyLength(256);
		$crypt->put_PaddingScheme(0);
		$crypt->put_EncodingMode('hex');
		$ivHex = '000102030405060708090A0B0C0D0E0F';
		$crypt->SetEncodedIV($ivHex,'hex');

		$keyHex = '000102030405060709080A0B0C0D0E0F101122131515161718191A1B1C1D1E1F';
		$crypt->SetEncodedKey($keyHex,'hex');

		$decStr = $crypt->decryptStringENC($str);
		return $decStr;
	}
	
	public static function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
	public static function cekCookie($userid,$cookie)
	{
		$db = Db::init();
		$cookiedb = $db->cookies;
		$cookiecount = $cookiedb->count(array("user_id" => $userid,"cookie" => $cookie));
		if($cookiecount>0)
		{
			return TRUE;
		}
		return FALSE;
	}
}
