<?php
class sendfile
{
	private static $dbs;
	
	public static function init() {
		if (self::$dbs == null)
		{
			$filename = DOCROOT."folder.config";
			$handle = fopen($filename, "r");
			$contents = fread($handle, filesize($filename));
			fclose($handle);
			$dbs = json_decode($contents, true);
				
			self::$dbs = $dbs;
		}
		return self::$dbs;
	}
	
	public static function getPathName($name)
	{
		$dbs = self::init();
		foreach($dbs as $dtt)
		{
			if ($dtt['name'] == $name) {
				return $dtt['path'];
			}
		}
		return false;
	}
	
	public static function getPath()
	{
		$dbs = self::init();
		foreach($dbs as $dtt)
		{
			$pos = strpos($_SERVER['request_uri'], $dtt['url']);
			if ($pos !== false) {
				$p = str_replace($dtt['url'], '', $_SERVER['request_uri']);
				$path = $dtt['path'].$p;
				if(file_exists($path) && ($_SERVER['request_uri'] != '/'))
					return $path;
			}
		}
		return false;
	}
}