<?php
date_default_timezone_set("Asia/Jakarta");
ini_set('display_errors',1);
ini_set('display_startup_errors',1); 
error_reporting(E_ALL);
define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('BASEPATH', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('DOCVIEW', DOCROOT."view".DIRECTORY_SEPARATOR); 
define('DOCVIEWFRONT', DOCROOT."view".DIRECTORY_SEPARATOR."front".DIRECTORY_SEPARATOR); 
define('CACHE', "/tmp/cache/");
define("USERACTIVE", strtotime('-2 minute',time()));
define('PHPASS_HASH_STRENGTH', 8);
define('PHPASS_HASH_PORTABLE', FALSE);
define('EMAIL', DOCROOT.'lib/PHPMailer/class.phpmailer.php');
define('CDN', "http://track.digibeat.co.id/");
define('CDN_IMAGE', "http://cdn.digibeat.co.id/contributor");

define('PASSCRYPT', DOCROOT.'lib/phpass_helper.php');

include_once(DOCROOT."lib/chilkat/chilkat_9_5_0.php");

define('DOMAIN', "http://api.streaming.deboxs.com/");
//define('DOMAIN', "https://www.tebakaja.com/");

//include(DOCROOT."lib/chilkat_9_5_02.php");
//include(DOCROOT."/mongodb/functions.php");

function errorHandler($errno, $errstr, $errfile, $errline)
    {
        //don't display error if no error number
        if (!(error_reporting() & $errno)) {
            return;
        }

        //display errors according to the error number
        switch ($errno)
        {
            case E_USER_ERROR:
                echo "<b>ERROR</b> [$errno] $errstr<br />\n";
                echo "  Fatal error on line $errline in file $errfile";
                echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                echo "Aborting...<br />\n";
                exit(1);
                break;

            case E_USER_WARNING:
                echo "<b>WARNING</b> [$errno] $errstr<br />\n";
                break;

            case E_USER_NOTICE:
                echo "<b>NOTICE</b> [$errno] $errstr<br />\n";
                break;

            default:
                echo "<b>UNKNOWN ERROR</b> [$errno] $errstr<br />\n";
                break;
        }

        //don't execute PHP internal error handler
        return true;
    }
	
function autoload_mongodb($class)
{
  $parts = explode('\\', $class);
  if(file_exists(DOCROOT."/mongodb/".end($parts).".php"))
        require DOCROOT."/mongodb/".end($parts).".php";
  else if(file_exists(DOCROOT."/mongodb/Model/".end($parts).".php"))
        require DOCROOT."/mongodb/Model/".end($parts).".php";
  else if(file_exists(DOCROOT."/mongodb/Exception/".end($parts).".php"))
        require DOCROOT."/mongodb/Exception/".end($parts).".php";
  else if(file_exists(DOCROOT."/mongodb/Operation/".end($parts).".php"))
        require DOCROOT."/mongodb/Operation/".end($parts).".php";
}
	
function autoload_my($class_name) {
	$s = explode("_", $class_name);
	if(count($s) == 1)
	{
		if(file_exists(DOCROOT."/classes/".$class_name . '.php'))
		{
	    	include DOCROOT."/classes/".$class_name . '.php';
		}
		else if(file_exists(DOCROOT."/lib/".$class_name . '.php'))
		{
			include DOCROOT."/lib/".$class_name . '.php';
		}
	}
	else {
		if(file_exists(DOCROOT."/classes/".$s[1]."/".$s[0] . '.php'))
		{
			include DOCROOT."/classes/".$s[1]."/".$s[0] . '.php';
		}
	}
	
}

//set_error_handler('errorHandler');
//spl_autoload_register("autoload_mongodb");
spl_autoload_register("autoload_my");

function fatal_handler() {
  $errfile = "unknown file";
  $errstr  = "shutdown";
  $errno   = E_CORE_ERROR;
  $errline = 0;

  $error = error_get_last();

  if( $error !== NULL) {
    $errno   = $error["type"];
    $errfile = $error["file"];
    $errline = $error["line"];
    $errstr  = $error["message"];

    errorHandler($errno, $errstr, $errfile, $errline);
  }
}
//register_shutdown_function( "fatal_handler" );

$http = new swoole_http_server("0.0.0.0", 8033, SWOOLE_BASE, SWOOLE_SOCK_TCP);
$http->set([
	'daemonize' => TRUE,
	'worker_num' => 2,
	'task_worker_num' => 2,
]);

$http->on('request', function ($request, $response) {
	//print_r($request);
	unset($_COOKIE);
	unset($_GET);
	unset($_POST);
	unset($_FILES);
	unset($_SESSION);
	unset($_SERVER);
	
	if(isset($request->cookie))
	{
		$_COOKIE = $request->cookie;
		if(isset($_COOKIE['DIGIBEAT']))
			$mysession = new mysession($_COOKIE['DIGIBEAT']);
	}
	if(isset($request->server))
		$_SERVER = $request->server;
	if(isset($request->get))
		$_GET = $request->get;
	if(isset($request->post))
		$_POST = $request->post;
	if(isset($request->files))
		$_FILES = $request->files;
	
	//$path = __DIR__.$request->server['request_uri'];
	$path = sendfile::getPath();
	if($path)
	{
		$path_parts = pathinfo($path);
		$extension = '';
		if(isset($path_parts['extension']))
			$extension = $path_parts['extension'];
		$filename = $path_parts['filename'];
		$dirname = $path_parts['dirname'];
		
		$w = 0;
		$h = 0;
		$t = "";
		if(isset($request->get['w']) && strlen($extension) > 0)
		{
			if(isset($request->get['w']))
				$w = intval($request->get['w']);
			if(isset($request->get['h']))
				$h = intval($request->get['h']);
			if(isset($request->get['t']))
				$t = trim($request->get['t']);
		
			if (!file_exists(CACHE)) {
				mkdir(CACHE, 0777, true);
			}
				
			if(($w > 0) && ($h > 0))
			{
				if($t == "")
				{
					$pathname = CACHE.$filename.'.'.$w.'x'.$h.'.'.$extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($path);
						$image->resizeToBestFit($w, $h);
						$image->save($pathname);
					}
					$path = $pathname;
				}
				else if($t == "h")
				{
					$pathname = CACHE.$filename.'.'.$w.'x'.$h.'-maxHeight.'.$extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($path);
						$image->resizeToHeight($h);
						$image->save($pathname);
					}
					$path = $pathname;
				}
				else if($t == "w")
				{
					$pathname = CACHE.$filename.'.'.$w.'x'.$h.'-maxWidth.'.$extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($path);
						$image->resizeToWidth($w);
						$image->save($pathname);
					}
					$path = $pathname;
				}
				else if($t == "e")
				{
					$pathname = CACHE.$filename.'.'.$w.'x'.$h.'-exact.'.$extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($path);
						$image->crop($w, $h);
						$image->save($pathname);
					}
					$path = $pathname;
				}
			}
		}
		
		if(file_exists($path) && ($request->server['request_uri'] != '/'))
		{
		
			$last_modified_time = filemtime($path);
			$etag = md5_file($path);
			if (array_key_exists("if-modified-since", $request->header)) {
				if (@strtotime($request->header['if-modified-since']) == $last_modified_time) {
					$response->status(304);
					$response->end();
					return;
				}
			}
			else if (array_key_exists("if-none-match", $request->header)) {
				if (trim($request->header['if-none-match']) == $etag) {
					$response->status(304);
					$response->end();
					return;
				}
			}
		
			$response->header("Last-Modified", gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
			$response->header("Etag", $etag);
			$response->header('Cache-Control', 'public');
		
			$mime = mime::get($extension);
		
			$response->header('Content-Type', $mime);
			$content = file_get_contents($path);
			$response->end($content);
			
			return;
		}
	}
	else {

		ob_start();
		
		$adacontent = false;
		$path = parse_url($request->server['request_uri'], PHP_URL_PATH);
		if($path == "/")
		{
			$r = new welcome_controller($request, $response);
			$r->index();
			$adacontent = true;
		}
		
		else {
			$pp = explode("/", $path);
			if(count($pp) > 2)
			{
					
				if (class_exists($pp[1].'_controller')) {
					$rr = $pp[1].'_controller';
					$r = new $rr($request, $response);
					if(method_exists($r, $pp[2]))
					{
						$r->$pp[2]();
						$adacontent = true;
					}
				}
			}
		}		
		
		$content = ob_get_contents();
	    ob_end_clean ();
		
		if($adacontent)
		{
			$response->header("Content-Type", "text/html; charset=utf-8");
	    	$response->end($content);
			//echo 'ada content';
	    	//print_r($_COOKIE);
	    	if(!isset($_COOKIE))
	    		$_COOKIE = array();
	    	if(!isset($_SESSION))
	    		$_SESSION = array();
	    	$p = array('cookie' => $_COOKIE, 'session' => $_SESSION);
	    	global $http;
	    	$http->task($p);
			return;
		}
		
	}
	$response->status(404);
	$response->end("gak ada!");
});

$http->on('finish', function ()
{
	//echo "task finish ".time()."\n";
});
	
$http->on('task', function ($serv, $task_id, $from_id, $data)
{
	$cookie = $data['cookie'];
	$session = $data['session'];
	if(isset($cookie['DIGIBEAT']))
	{
		$mysession = new mysession($cookie['DIGIBEAT']);
		foreach($session as $key => $value)
		{
			$mysession->saveSession($key, $value);
		}
		
		if(isset($session['userid']))
		{
			if(strlen(trim($session['userid'])) > 0)
			{
				$db = Db::init();
        		$users = $db->users;
				$p = array('last_time' => time());
				$users->update(array('_id' => new MongoId($session['userid'])), array('$set' => $p));
			}
		}
		
		$db = Db::init();
        $cook = $db->cookies;
		$c = $cook->findone(array('cookie' => $cookie['DIGIBEAT']));
		if(isset($c['_id']))
		{
			$p = array('last_time' => time());
			$cook->update(array('_id' => new MongoId($c['_id'])), array('$set' => $p));
		}
	}
	
	global $http;
	$http->finish("");
});
	
$http->start();