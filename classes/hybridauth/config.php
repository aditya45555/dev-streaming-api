<?php
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

return 
	array(
		"base_url" => "http://api.streaming.dboxid.com/loginoauth", 

		"providers" => array ( 
			// openid providers
			"OpenID" => array (
				"enabled" => false
			),

			"Yahoo" => array ( 
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" )
			),

			"AOL"  => array ( 
				"enabled" => false 
			),

			"Google" => array ( 
				"enabled" => true,
				"keys"    => array ( "id" => "887499863783-3fe3lp6c5mfmeo0j6f7pe13cm0pgniv1.apps.googleusercontent.com", "secret" => "rv9p29HDuOCP4EYvBpjNndYs" ) 
			),

			"Facebook" => array ( 
				"enabled" => true,
				"keys"    => array ( "id" => "234032339948069", "secret" => "da67248cbb290b4dd5314125d0038ec6" )
			),

			"Twitter" => array ( 
				"enabled" => true,
				"keys"    => array ( "key" => "R6ucvOqX3EtQjODSDqEUg", "secret" => "ZPplmZFZ6bwgJ8TVmsPqwfErLXSs5d7dYHmRJcMLmO8" ) 
			),

			// windows live
			"Live" => array ( 
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" ) 
			),

			"MySpace" => array ( 
				"enabled" => false,
				"keys"    => array ( "key" => "", "secret" => "" ) 
			),

			"LinkedIn" => array ( 
				"enabled" => false,
				"keys"    => array ( "key" => "", "secret" => "" ) 
			),

			"Foursquare" => array (
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" ) 
			),
		),

		// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
		"debug_mode" => true,

		"debug_file" => "/home/digibeat/bc/log.txt",
	);
