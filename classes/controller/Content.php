<?php

class Content extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        
    }
    
    public function search()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
        $search = Base::instance()->get('GET.search');
		$userid = Base::instance()->get('GET.userid');
		$limit = Base::instance()->get('GET.limit');
        
        $db = Db::init();
        $art = $db->artists;
		$ai = $db->artist_images;
        $alb = $db->albums;
		$albart = $db->album_artists;
		$albgen = $db->album_genres;
        $col = $db->musics;
        
		$search = urldecode($search);
        if((strlen(trim($search)) > 0) && (strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
        	$curl = new Curl();
			$curl->get('https://sso.dboxid.com/login/cekcookie', array(
					'userid' => $userid,
					'cookie' => $cookie,
			));
			$res = $curl->response;
			$r = json_decode($res, true);
			if(is_array($r))
			{
				if($r['error'] == null)
                {
                	$cek = '';
	                foreach($r['result'] as $rt)
					{
						$cek = $rt['cek'];
					}
							
					//echo 'isi cek ADALAH '.$cek;
	                $ada = false;
	                if($cek == "OK")
                    {
                    	$sch = explode(" ", $search);
						$arr = array();
						foreach($sch as $sc)
						{
							$regex = new MongoRegex("/".$sc."/i");
							$p = array('name' => $regex);
							$arr[] = $p;
							$p = array('seo' => $regex);
							$arr[] = $p;
						}
                    	
						$quer = array(
                            '$or' => $arr
						);
						//$quer = array(
                       //     '$or' => array( 
                        //    array('name' => $regex),
                        //    array('seo' => $regex),
                        //));
						
                        $mart = $art->find($quer)->limit($limit);
						$arrart = array();           
                        foreach($mart as $dart)
                        {
                        	$mai = $ai->find(array("artist_id" => new MongoId($dart['_id']), "foto_default" => "yes"));
												
							$foto = '';
							foreach($mai as $dai) {
								$foto = $dai['foto'];
								break;
							}
							
                        	$artist = array(
                                "artistid" => trim($dart['_id']),
                                "seo" => $dart['seo'],
                                "name" => $dart['name'],
                                "imageurl" => CDN_IMAGE.'/image/',
                                "imagename" => $foto,
                            );
                            $arrart[] = $artist;
                        } 
						
						$sch = explode(" ", $search);
						$arr = array();
						foreach($sch as $sc)
						{
							$regex = new MongoRegex("/".$sc."/i");
							$p = array('title' => $regex);
							$arr[] = $p;
							$p = array('seo' => $regex);
							$arr[] = $p;
						}
                    	
						$quer = array(
                            '$or' => $arr
						);
						/*$quer = array(
                            '$or' => array( 
                            array('title' => $regex),
                            array('seo' => $regex),
                        ));     */      
                        $malb = $alb->find($quer)->limit($limit);
                        $arralb = array();
                        foreach($malb as $dalb)
                        {
                        	$malbart = $albart->findOne(array('album_id' => new MongoId($dalb['_id'])));
							$dart = $art->findOne(array('_id' => new MongoId($malbart['artist_id'])));
							
							$mai = $ai->find(array("artist_id" => new MongoId($malbart['artist_id']), "foto_default" => "yes"));
												
							$fotoartist = '';
							foreach($mai as $dai) {
								$fotoartist = $dai['foto'];
								break;
							}
							
							$malbgen = $albgen->findOne(array('album_id' => new MongoId($dalb['_id'])));
                        	$album = array(
                                "albumid" => trim($dalb['_id']),
                                "title" => $dalb['title'],
                                "seo" => $dalb['seo'],
                                "artistname" => $dart['name'],
                                "description" => $dalb['description'],
                                "artistid" => trim($malbart['artist_id']),
                                "genreid" => trim($malbgen['genre_id']),
                                "imageurl" => CDN_IMAGE.'/image/',
                                "imagename" => $dalb['cover_front'],
                                "imageartist" => $fotoartist,
                            );
                            $arralb[] = $album;
                        }

						$sch = explode(" ", $search);
						$arr = array();
						foreach($sch as $sc)
						{
							$regex = new MongoRegex("/".$sc."/i");
							$p = array('title' => $regex);
							$arr[] = $p;
							$p = array('seo' => $regex);
							$arr[] = $p;
						}
                    	
						$q = array(
                            '$or' => $arr, 'approve_status' => 'approved', 'status_publish' => 'yes'
						);
						/*$q = array(
                            '$or' => array( 
                            array('title' => $regex),
                            array('seo' => $regex),
                        ));*/
                        $mmus = $col->find($q)->limit($limit);
                        $arrcol = array();
                        foreach($mmus as $dmus)
                        {
                        	$mmart = $art->findOne(array('_id' => new MongoId($dmus['artist_id'])));
							$mmalb = $alb->findOne(array('_id' => new MongoId($dmus['album_id'])));
							
							//$userid = USER_SEMENTARA; // sementara supaya bisa setel
							$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$dmus['key']."/index.m3u8";
                        	$streamV0 .= "?userid=".$userid."&songid=".$dmus['key']."&cookie=".$cookie."&appid=".$appid;
							
							$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$dmus['key']."/index.m3u8";
                        	$streamV2 .= "?userid=".$userid."&songid=".$dmus['key']."&cookie=".$cookie."&appid=".$appid;
							
							$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$dmus['key']."/index.m3u8";
                        	$streamV4 .= "?userid=".$userid."&songid=".$dmus['key']."&cookie=".$cookie."&appid=".$appid;

				    		$zipnew  = "http://track.digibeat.co.id/get.zip";
                        	$zipnew .= "?userid=".$userid."&songid=".$dmus['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
							
                        	$collection = array(
                                "collectionid" => trim($dmus['_id']),
                                "title" => $dmus['title'],
                                "seo" => $dmus['seo'],
                                "track" => $dmus['track'],
                                "length" => intval($dmus['length']),
                                "artistname" => $mmart['name'],
                                "artistid" => trim($dmus['artist_id']),
                                "genreid" => trim($dmus['genre_id']),
                                "albumid" => trim($dmus['album_id']),
                                "albumname" => $mmalb['title'],
                                "imageurl" => CDN_IMAGE.'/image/',
                                "imagename" => $mmalb['cover_front'],
                                "streamv0" => $streamV0,
                                "streamv2" => $streamV2,
                                "streamv4" => $streamV4,
                                "zip_file" => $zipnew 
                            );
                            $arrcol[] = $collection;
                        }
                        
						$rpc = array(
                            "artist" => $arrart,
                            "album" => $arralb,
                            "track" => $arrcol
                        );
             
                        echo json_encode($rpc);
                    }
				}
			}			
            /*$h = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
            $hh = json_decode($h, true);
            if(isset($hh['appname']))
            {
                $cari = "";
                        if(strlen(trim($search)) > 0)
                            $cari = $search;
                        
                        $regex = new MongoRegex("/".$cari."/i");
                        
                        $quer = array(
                            '$or' => array( 
                            array('name' => $regex),
                            array('seo' => $regex),
                        ));
                        $mart = $art->find($quer)->limit(5); 
                        $arrart = array();           
                        foreach($mart as $dart)
                        {
                            if(isset($dart['stream_id']))
                            {
                                if(strlen(trim($dart['stream_id'])) > 0)
                                {
                                    $a = Http::getStr(DOMAIN."artist/search?id=".$dart['stream_id']."&type=hexa");
                                    $aa = json_decode($a, true);
                                    
                                    $imagelink = DOMAIN_STREAM;
                                    $imagelink .= "images.jpg?id=".$aa["image"];
                                    $imagelink .= "&appid=".$appid;
                                    $imagelink .= "&cookie=".$cookie;
                                    $artist = array(
                                        "artistid" => $aa['id'],
                                        "seo" => $dart['seo'],
                                        "name" => $aa['name'],
                                        "imagelink" => $imagelink
                                    );
                                    $arrart[] = $artist;
                                }    
                            }                            
                        }
                         
                        $quer = array(
                            '$or' => array( 
                            array('title' => $regex),
                            array('seo' => $regex),
                        ));           
                        $malb = $alb->find($quer)->limit(5);
                        $arralb = array();
                        foreach($malb as $dalb)
                        {
                            if(isset($dalb['stream_id']))
                            {
                                if(strlen(trim($dalb['stream_id'])) > 0)
                                {
                                    $n = Http::getStr(DOMAIN."album/search?id=".$dalb['stream_id']."&type=hexa");
                                    $n = preg_replace('/[^[:print:]]/', '', $n);
                                    $nn = json_decode($n, true);
                                    
                                    $an = Http::getStr(DOMAIN."artist/search?id=".base_convert($nn['artistid'], 10, 16)."&type=hexa");
                                    $ann = json_decode($an, true);
                                    
                                    $name = "";
                                    if(isset($ann['name']))
                                    {
                                        $name = $ann['name'];
                                    }
                                    
                                    $imagelink = DOMAIN_STREAM;
                                    $imagelink .= "images.jpg?id=".$nn["image"];
                                    $imagelink .= "&appid=".$appid;
                                    $imagelink .= "&cookie=".$cookie;
                                    $album = array(
                                        "albumid" => $nn['id'],
                                        "title" => $nn['title'],
                                        "seo" => $dalb['seo'],
                                        "artistname" => $name,
                                        "description" => $nn['description'],
                                        "artistid" => base_convert($nn['artistid'], 10, 16),
                                        "genreid" => base_convert($nn['genreid'], 10, 16),
                                        "imagelink" => $imagelink
                                    );
                                    $arralb[] = $album;
                                }    
                            }                            
                        }
                        
                        //track 
                        $q = array(
                            '$or' => array( 
                            array('title' => $regex),
                            array('seo' => $regex),
                        ));
                        $mmus = $col->find($q)->limit(5);
                        $arrcol = array();
                        foreach($mmus as $dmus)
                        {
                            if(isset($dmus['key']))
                            {
                                if(strlen(trim($dmus['key'])) > 0)
                                {
                                    $m = Http::getStr(DOMAIN."collection/search?id=".trim($dmus['key'])."&type=hexa");
                                    $mm = json_decode($m, true);
                                    if(is_array($mm))
                                    {                                    
                                        if(count($mm) > 2)
                                        {
                                            $arr = array(
                                                "collectionid" => intval(base_convert(trim($dmus['key']), 16, 10))
                                            );
                                            
                                            $t = Http::kirimJson(DOMAIN."song_album/search", json_encode($arr));                        
                                            $tr = json_decode($t, true);
                                            
                                            $albumid = "";                                             
                                            $adadata = false;                                                        
                                            if(is_array($tr))
                                            {
                                                if(isset($tr['result']))
                                                {
                                                    foreach($tr["result"] as $ddf)
                                                    {   
                                                        $albumid = base_convert($ddf['albumid'], 10, 16);                                                          
                                                        $adadata = true;                                       
                                                    }    
                                                }                                    
                                            }
                                            
                                            if($adadata)
                                            {
                                                $name = "";                                
                                                $artisid = "";
                                                $genreid = "";
                                                $imagelink = "";
                                                $albumname = "";
                                                if(strlen(trim($albumid)) > 0)
                                                {
                                                    $ma = Http::getStr(DOMAIN."album/search?id=".$albumid."&type=hexa");
                                                    $ma = preg_replace('/[^[:print:]]/', '', $ma);
                                                    $maa = json_decode($ma, true);
                                                    
                                                    $artisid = base_convert($maa['artistid'], 10, 16);
                                                    $genreid = base_convert($maa['genreid'], 10, 16);
                                                    $albumname = $maa['title'];
                                                    
                                                    $imagelink = DOMAIN_STREAM;
                                                    $imagelink .= "images.jpg?id=".$maa["image"];
                                                    $imagelink .= "&appid=".$appid;
                                                    $imagelink .= "&cookie=".$cookie;
                                                    
                                                    $ar = Http::getStr(DOMAIN."artist/search?id=".$artisid."&type=hexa"); 
                                                    $mar = json_decode($ar, true);
                                                                     
                                                    if(isset($mar['name']))
                                                    {
                                                        $name = $mar['name'];
                                                    }
                                                }                                                                                                                                                             
                                                
                                                $v0 = DOMAIN_STREAM."stream.m3u8?id=".$mm['id']."&type=audio&rate=V0&appid=".$appid."&cookie=".$cookie;
                                                $v2 = DOMAIN_STREAM."stream.m3u8?id=".$mm['id']."&type=audio&rate=V2&appid=".$appid."&cookie=".$cookie;
                                                $v4 = DOMAIN_STREAM."stream.m3u8?id=".$mm['id']."&type=audio&rate=V4&appid=".$appid."&cookie=".$cookie;
                                                
                                                $ac = Http::getStr(DOMAIN."audio_collection/search?id=".$dmus['key']."&type=hexa");
                                                $acc = json_decode($ac, true);
                                                //print_r($acc);                             
                                                $length = 0;                                                                
                                                if(count($acc) > 2)
                                                    $length = $acc['length'];    
                                                    
                                                $collection = array(
                                                    "collectionid" => $mm['id'],
                                                    "title" => $dmus['title'],
                                                    "seo" => $dmus['seo'],
                                                    "track" => $dmus['track'],
                                                    "length" => intval($dmus['length']),
                                                    "artistname" => $name,
                                                    "artistid" => $artisid,
                                                    "genreid" => $genreid,
                                                    "albumid" => $albumid,
                                                    "albumname" => $albumname,
                                                    "album_art" => $imagelink,
                                                    "streamv0" => $v0,
                                                    "streamv2" => $v2,
                                                    "streamv4" => $v4                                    
                                                );
                                                $arrcol[] = $collection;
                                            }
                                        }
                                    }
                                    
                                }    
                            }                            
                        }
            
                        $rpc = array(
                            "artist" => $arrart,
                            "album" => $arralb,
                            "track" => $arrcol
                        );
             
                        echo json_encode($rpc);
            }*/            
        }
    }
    
    public function getartistbyid()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
        $id = Base::instance()->get('GET.id');
		$userid = Base::instance()->get('GET.userid');
        
        $db = Db::init();
        $ai = $db->artist_images;
		$art = $db->artists;
        $alb = $db->albums;
        $aart = $db->album_artists;
		$agen = $db->album_genres;
        $gen = $db->genres;
		
		if((strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
        	$curl = new Curl();
			$curl->get('https://sso.dboxid.com/login/cekcookie', array(
					'userid' => $userid,
					'cookie' => $cookie,
			));
			$res = $curl->response;
			$r = json_decode($res, true);
			if(is_array($r))
			{
				if($r['error'] == null)
                {
                	$cek = '';
	                foreach($r['result'] as $rt)
					{
						$cek = $rt['cek'];
					}
							
					//echo 'isi cek ADALAH '.$cek;	                
	                if($cek == "OK")
                    {
                    	$mart = $art->findOne(array("_id" => new MongoId($id)));
						$mai = $ai->find(array("artist_id" => new MongoId($mart['_id'])));
						$ada = false;
						$dtart = array();
						if($mart)
						{
							$ada = true;
							$genre = array();
							if(count($mart['genre_id']) > 0)
                            {
                                foreach($mart['genre_id'] as $dgen)
                                {
                                	$mgen = $gen->findOne(array("_id" => new MongoId($dgen['genre_id'])));
									$g = array(
                                        "name" => $mgen['name'],
                                    );
                                    $genre[] = $g;
                                }
							}
							$foto = array();
                            foreach($mai as $ft)
                            {
                                if(isset($ft['foto']))
                                {
                                    if(strlen(trim($ft['foto'])) > 0)
                                    {
                                        $f = array(
                                            "imageurl" => CDN_IMAGE."/image",
                                            "imagename" => $ft['foto'] 
                                        );
                                        $foto[] = $f;   
                                    }   
                                }                                                                               
                            }
                            $personel = array();
                            if(count($mart['personel_name']) > 0)
                            {
                                foreach($mart['personel_name'] as $pr)
                                {
                                    if(isset($pr['name']))
                                    {
                                        if(strlen(trim($pr['name'])) > 0)
                                        {
                                            $p = array(
                                                "name" => $pr['name'],
                                            );
                                            $personel[] = $p;
                                        }    
                                    }                                        
                                }
                            }
							$dataartist = array(
                                "name" => $mart['name'],
                                "background" => $mart['background'],
                                "description" => $mart['description'],
                                "birthplace" => $mart['birthplace'],
                                "birthdate" => "",//date('d-m-Y', $mart['date_of_birth']),
                                "died" => $mart['died'],
                                "googleid" => $mart['google_id'],
                                "facebookid" => $mart['facebook_id'],
                                "twitterid" => $mart['twitter_id'],
                                "blog" => $mart['blog'],
                                "website" => $mart['website'],
                                "tipe" => $mart['type'],
                                "seo" => $mart['seo'],
                                "genre" => $genre,
                                "personel" => $personel,
                                "foto" => $foto
                            );
                            
                            $dtart[] = $dataartist;
						}

	                    $dtalb = array();
	                    if($ada)
	                    {
	                    	$aralb = $aart->find(array("artist_id" => new MongoId($mart['_id'])));
                                
                            if($aralb->count() > 0)
                            {
                                $adaalbum = true;                                    
                                foreach($aralb as $aa)
                                {
                                	$malb = $alb->findOne(array("_id" => new MongoId($aa['album_id'])));
									$mgen = $agen->findOne(array('album_id' => new MongoId($aa['album_id'])));
									$dart = $art->findOne(array('_id' => new MongoId($mart['_id'])));
									if($malb)
                                    {
                                    	$dataalbum = array(
                                            "albumid" => trim($aa['album_id']),
                                            "albumname" => $malb['title'],
                                            "title" => $malb['title'],
                                            "seo" => $malb['seo'],
                                            "artistname" => $dart['name'],
                                            "description" => $malb['description'],
                                            "artistid" => trim($mart['_id']),
                                            "genreid" => trim($mgen['genre_id']),
                                            "imageurl" => CDN_IMAGE."/image/",
                                            "imagename" => $malb['cover_front']
                                        );
                                        $dtalb[] = $dataalbum;
                                    }
                                }
                            }
	                    }
						$rpc = array(
                            "artist" => $dtart,
                            "album" => $dtalb
                        );
                        echo json_encode($rpc);
                    }
				}
			}
		}
        
        /*if((strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
            $h = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
            $hh = json_decode($h, true);
            if(isset($hh['appname']))
            {
                $ex = "";
                if(strpos($cookie, "_") == true)
                {
                    $dc = explode("_", $cookie);
                    $ex = $dc[0];
                }
                
                $c = Http::getStr(DOMAIN."session/search?id=".$ex."&type=hexa");
                $cc = json_decode($c, true);
                
                if(is_array($cc))
                {
                    if(count($cc) > 0)
                    {
                        if(strlen(trim($id)) > 0)
                        {
                            $mart = $art->findOne(array("stream_id" => $id));
                            
                            $dtart = array();
                            $ada = false;
                            if($mart)
                            {
                                $ada = true;
                                $genre = array();
                                if(count($mart['genre_id']) > 0)
                                {
                                    foreach($mart['genre_id'] as $dgen)
                                    {
                                        if(isset($dgen['name']))
                                        {
                                            if(strlen(trim($dgen['name'])) > 0)
                                            {
                                                $mgen = $gen->findOne(array("_id" => new MongoId($dgen['genre_id'])));
                                                $g = array(
                                                    "name" => $dgen['name'],
                                                );
                                                $genre[] = $g;       
                                            }    
                                        }                                                                               
                                    }                                    
                                }
                                $foto = array();
                                if(count($mart['foto']) > 0)
                                {
                                    foreach($mart['foto'] as $ft)
                                    {
                                        if(isset($ft['foto']))
                                        {
                                            if(strlen(trim($ft['foto'])) > 0)
                                            {
                                                $f = array(
                                                    "imagelink" => "http://www.dboxid.com/get/upload?namafile=".$ft['foto']."&tipe=29",
                                                );
                                                $foto[] = $f;   
                                            }   
                                        }                                                                               
                                    }
                                }
                                $personel = array();
                                if(count($mart['personel_name']) > 0)
                                {
                                    foreach($mart['personel_name'] as $pr)
                                    {
                                        if(isset($pr['name']))
                                        {
                                            if(strlen(trim($pr['name'])) > 0)
                                            {
                                                $p = array(
                                                    "name" => $pr['name'],
                                                );
                                                $personel[] = $p;
                                            }    
                                        }                                        
                                    }
                                }
                                
                                $dataartist = array(
                                    "name" => $mart['name'],
                                    "background" => $mart['background'],
                                    "description" => $mart['description'],
                                    "birthplace" => $mart['birthplace'],
                                    "birthdate" => date('d-m-Y', $mart['date_of_birth']),
                                    "died" => $mart['died'],
                                    "googleid" => $mart['google_id'],
                                    "facebookid" => $mart['facebook_id'],
                                    "twitterid" => $mart['twitter_id'],
                                    "blog" => $mart['blog'],
                                    "website" => $mart['website'],
                                    "tipe" => $mart['type'],
                                    "seo" => $mart['seo'],
                                    "genre" => $genre,
                                    "personel" => $personel,
                                    "foto" => $foto
                                );
                                
                                $dtart[] = $dataartist;
                            }

                            $adaalbum = false;
                            $album = array();
                            if($ada)
                            {
                                $aralb = $aart->find(array("artist_id" => new MongoId($mart['_id'])));
                                
                                if($aralb->count() > 0)
                                {
                                    $adaalbum = true;                                    
                                    foreach($aralb as $aa)
                                    {
                                        $al = array(
                                            "albumid" => trim($aa['album_id'])
                                        );
                                        $album[] = $al;
                                    }
                                }
                            }
                            
                            $dtalb = array();
                            if($adaalbum)
                            {
                                if(is_array($album))
                                {
                                    if(count($album) > 0)
                                    {
                                        foreach($album as $aab)
                                        {
                                            $malb = $alb->findOne(array("_id" => new MongoId($aab['albumid'])));
                                            
                                            if($malb)
                                            {
                                                if(isset($malb['stream_id']))
                                                {
                                                    if(strlen(trim($malb['stream_id'])) > 0)
                                                    {
                                                        $n = Http::getStr(DOMAIN."album/search?id=".$malb['stream_id']."&type=hexa");
                                                        $n = preg_replace('/[^[:print:]]/', '', $n);
                                                        $nn = json_decode($n, true);
                                                        
                                                        $an = Http::getStr(DOMAIN."artist/search?id=".base_convert($nn['artistid'], 10, 16)."&type=hexa");
                                                        $ann = json_decode($an, true);
                                                        
                                                        $name = "";
                                                        if(isset($ann['name']))
                                                        {
                                                            $name = $ann['name'];
                                                        }
                                                        
                                                        $imagelink = DOMAIN_STREAM;
                                                        $imagelink .= "images.jpg?id=".$nn["image"];
                                                        $imagelink .= "&appid=".$appid;
                                                        $imagelink .= "&cookie=".$cookie;
                                                        $dataalbum = array(
                                                            "albumid" => $nn['id'],
                                                            "albumname" => $nn['title'],
                                                            "title" => $nn['title'],
                                                            "seo" => $malb['seo'],
                                                            "artistname" => $name,
                                                            "description" => $nn['description'],
                                                            "artistid" => base_convert($nn['artistid'], 10, 16),
                                                            "genreid" => base_convert($nn['genreid'], 10, 16),
                                                            "imagelink" => $imagelink
                                                        );
                                                        $dtalb[] = $dataalbum;       
                                                    }
                                                }                                                
                                            }
                                        }
                                    }
                                }
                            }
                            
                            $rpc = array(
                                "artist" => $dtart,
                                "album" => $dtalb
                            );
                            echo json_encode($rpc);
                        }
                    }
                }
            }
        }*/
    }

    public function getalbumbyid()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
		$userid = Base::instance()->get('GET.userid');
        $id = Base::instance()->get('GET.id');
        
        $db = Db::init();
        $alb = $db->albums;
        
        if((strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
        	$curl = new Curl();
			$curl->get('https://sso.dboxid.com/login/cekcookie', array(
					'userid' => $userid,
					'cookie' => $cookie,
			));
			$res = $curl->response;
			$r = json_decode($res, true);
			if(is_array($r))
            //$h = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
           // $hh = json_decode($h, true);
            //if(isset($hh['appname']))
            {
                //$ex = "";
                //if(strpos($cookie, "_") == true)
               // {
                //    $dc = explode("_", $cookie);
                //    $ex = $dc[0];
                //}
                
               // $c = Http::getStr(DOMAIN."session/search?id=".$ex."&type=hexa");
               // $cc = json_decode($c, true);
                
                //if(is_array($cc))
                if($r['error'] == null)
                {
                	$cek = '';
	                foreach($r['result'] as $rt)
					{
						$cek = $rt['cek'];
					}
							
					//echo 'isi cek ADALAH '.$cek;
	                $ada = false;
	                if($cek == "OK")
                   // if(count($cc) > 0)
                    {                    	
                        if(strlen(trim($id)) > 0)
                        {
                            $malb = $alb->findOne(array("_id" => new MongoId($id)));
                            
                            /*$seo = "";
                            $description = "";
                            if($malb)
                            {
                                $seo = $malb['seo'];
                                $description = $malb['description'];
                            }
                            
                            $a = Http::getStr(DOMAIN."album/search?id=".$id."&type=hexa");
                            $a = preg_replace('/[^[:print:]]/', '', $a);
                            $aa = json_decode($a, true);
                            
                            $an = Http::getStr(DOMAIN."artist/search?id=".base_convert($aa['artistid'], 10, 16)."&type=hexa");
                            $ann = json_decode($an, true);
                                    
                            $name = "";
                            if(isset($ann['name']))
                                $name = $ann['name'];
                            
                            $arralb = array();
                            $imagelink = DOMAIN_STREAM;
                            $imagelink .= "images.jpg?id=".$aa["image"];
                            $imagelink .= "&appid=".$appid;
                            $imagelink .= "&cookie=".$cookie;*/
                            if(isset($malb['_id']))
							{
								$art = $db->album_artists;
								$aat = $art->findone(
									array("album_id" => new MongoId($malb['_id']))
								);
								
								$art2 = $db->artists;
								$aat2 = $art2->findone(
									array("_id" => new MongoId($aat['artist_id']))
								);
									
								$fotoartist = '';
								if(isset($aat2['foto']))
								{
									foreach($aat2['foto'] as $dft)
									{
										$fotoartist = $dft['foto'];
										break;
									}
								}
								
	                            $album = array(
	                                "albumid" => $id,
	                                "albumname" => $malb['title'],
	                                "title" => $malb['title'],
	                                "seo" => $malb['seo'],
	                                "artistname" => $aat2['name'],
	                                "description" => $malb['description'],
	                                "artistid" => trim($aat2['_id']),
	                                "genreid" => trim($malb['genre_id']),
	                                "imageurl" => CDN_IMAGE."/image/",
	                                "imagename" => $malb['cover_front'],
	                                "imageartist" => $fotoartist,
	                            );
	                            $arralb[] = $album;
								$pp = array(
									'id' => 10,
									'result' => $arralb,
									'error' => null
								);
								echo json_encode($pp);
								return;
							}
                        }
                    }
                }
            }
        }

		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
    }

    public function gettrackbyid()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
        $id = Base::instance()->get('GET.id');
		$userid = Base::instance()->get('GET.userid');
        
        $db = Db::init();
        $mus = $db->musics;
        
        if((strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
        	$curl = new Curl();
			$curl->get('https://sso.dboxid.com/login/cekcookie', array(
					'userid' => $userid,
					'cookie' => $cookie,
			));
			$res = $curl->response;
			$r = json_decode($res, true);
			if(is_array($r))
            //$h = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
            //$hh = json_decode($h, true);
           // if(isset($hh['appname']))
            {
               // $ex = "";
                //if(strpos($cookie, "_") == true)
                //{
                //    $dc = explode("_", $cookie);
                //    $ex = $dc[0];
               // }
                
               // $c = Http::getStr(DOMAIN."session/search?id=".$ex."&type=hexa");
               // $cc = json_decode($c, true);
                
                //if(is_array($cc))
				if($r['error'] == null)
                {
                	$cek = '';
	                foreach($r['result'] as $rt)
					{
						$cek = $rt['cek'];
					}
							
					//echo 'isi cek ADALAH '.$cek;
	                $ada = false;
	                if($cek == "OK")
                    //if(count($cc) > 0)
                    {
                        if(strlen(trim($id)) > 0)
                        {
                            $mmus = $mus->findOne(array("_id" => new MongoId($id)));
                            
                            
                            //$hasil = Http::getStr(DOMAIN."collection/search?id=".$id."&type=hexa");
                           // $cccl = json_decode($hasil, true);
                            
                            //$arrcol = array();                    
                            if(isset($mmus['_id']))
                            {
                            	//echo 'test masuk';
                            	$title = $mmus['title'];
                                $seo = $mmus['seo'];
                                $description = $mmus['description'];
                                $track = $mmus['track'];
                                $length = $mmus['length'];
                                if(strlen(trim($title)) > 0)
                                {
                                   // $arr3 = array(
                                    //    "collectionid" => intval(base_convert(trim($id), 16, 10))
                                    //);
                                                        
                                   // $t = Http::kirimJson(DOMAIN."song_album/search", json_encode($arr3));
                                    //$tr = json_decode($t, true);
                                                        
                                   // $albumid = "";                                      
                                    $adadata = true;                                              
                                   // if(is_array($tr))
                                   // {
                                    //    if(isset($tr['result']))
                                    //    {
                                    //        foreach($tr["result"] as $ddf)
                                    //        {   
                                     //           $albumid = base_convert($ddf['albumid'], 10, 16);                                                 
                                     //           $adadata = true;                                       
                                     //       }    
                                     //   }                                    
                                    //}
                                                        
                                    if($adadata)
                                    {
                                        $name = "";                                
                                        $artisid = "";
                                        $genreid = "";
                                        $imagelink = "";
                                        $albumname = "";
										
										$alb = $db->albums;
										$aal = $alb->findone(
											array("_id" => new MongoId($mmus['album_id']))
										);
										
										$art = $db->artists;
										$artis = $art->findone(
											array("_id" => new MongoId($mmus['artist_id']))
										);
								
                                        if(isset($aal['_id']))
                                        {
                                            //$ma = Http::getStr(DOMAIN."album/search?id=".$albumid."&type=hexa");
                                            //$ma = preg_replace('/[^[:print:]]/', '', $ma);
                                           // $maa = json_decode($ma, true);
                                                                
                                            //$artisid = base_convert($maa['artistid'], 10, 16);
                                            //$genreid = base_convert($maa['genreid'], 10, 16);
                                            $albumname = $aal['title'];
											$albumid = trim($mmus['album_id']);
											$genreid = trim($mmus['genre_id']);
											
											//$userid = USER_SEMENTARA; // sementara supaya bisa setel
											$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$mmus['key']."/index.m3u8";
			                            	$streamV0 .= "?userid=".$userid."&songid=".$mmus['key']."&cookie=".$cookie."&appid=".$appid;
											
											$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$mmus['key']."/index.m3u8";
			                            	$streamV2 .= "?userid=".$userid."&songid=".$mmus['key']."&cookie=".$cookie."&appid=".$appid;
											
											$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$mmus['key']."/index.m3u8";
			                            	$streamV4 .= "?userid=".$userid."&songid=".$mmus['key']."&cookie=".$cookie."&appid=".$appid;
			
								    		$zipnew  = "http://track.digibeat.co.id/get.zip";
			                            	$zipnew .= "?userid=".$userid."&songid=".$mmus['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
                                                                
                                           // $imagelink = DOMAIN_STREAM;
                                            //$imagelink .= "images.jpg?id=".$maa["image"];
                                            //$imagelink .= "&appid=".$appid;
                                            //$imagelink .= "&cookie=".$cookie;
                                                                
                                           // $ar = Http::getStr(DOMAIN."artist/search?id=".$artisid."&type=hexa"); 
                                            //$mar = json_decode($ar, true);
                                                                                 
                                            //if(isset($mar['name']))
                                             //   $name = $mar['name'];
                                             
                                              $collection = array(
	                                            "collectionid" => $id,
	                                            "title" => $title,
	                                            "seo" => $seo,
	                                            "track" => $track,
	                                            "length" => intval($length),
	                                            "artistname" => trim($artis['name']),
	                                            "artistid" => trim($artis['_id']),
	                                            "genreid" => $genreid,
	                                            "albumid" => $albumid,
	                                            "albumname" => $albumname,
	                                            "imageurl" => CDN_IMAGE.'/image/',
				                                "imagename" => $aal["cover_front"],
	                                            "streamv0" => $streamV0,
	                                            "streamv2" => $streamV2,
	                                            "streamv4" => $streamV4,
	                                            "zip_file" => $zipnew,                                    
	                                        );
	                                        $arrcol[] = $collection;

											$pp = array(
												'id' => 10,
												'result' => $arrcol,
												'error' => null
											);
											echo json_encode($pp);
											return;
                                        }                                                                                                                                                             
                                                            
                                        //$v0 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V0&appid=".$appid."&cookie=".$cookie;
                                       // $v2 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V2&appid=".$appid."&cookie=".$cookie;
                                       // $v4 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V4&appid=".$appid."&cookie=".$cookie;
                                                            
                                        /*$ac = Http::getStr(DOMAIN."audio_collection/search?id=".$cccl['id']."&type=hexa");
                                        $acc = json_decode($ac, true);
                                                                                       
                                        $length = 0;                                                                
                                        if(count($acc) > 2)
                                            $length = $acc['length'];*/ 
                                                                
                                       
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
    }

    public function gettrackbygenreid()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
        $id = Base::instance()->get('GET.id');
        
        $db = Db::init();
        $mus = $db->musics;
        
        if((strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
            $h = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
            $hh = json_decode($h, true);
            if(isset($hh['appname']))
            {
                $ex = "";
                if(strpos($cookie, "_") == true)
                {
                    $dc = explode("_", $cookie);
                    $ex = $dc[0];
                }
                
                $c = Http::getStr(DOMAIN."session/search?id=".$ex."&type=hexa");
                $cc = json_decode($c, true);
                
                if(is_array($cc))
                {
                    if(count($cc) > 0)
                    {
                        if(strlen(trim($id)) > 0)
                        {
                            $arr = array(
                                "genreid" => intval(base_convert($id, 16, 10))
                            );
                            
                            $hasil = Http::kirimJson(DOMAIN."song_playlist/search", json_encode($arr));
                            $dh = json_decode($hasil, true);
                            
                            $coll = array();
                            if(is_array($dh))
                            {
                                if(isset($dh['result']))
                                {
                                    if(count($dh['result']) > 0)
                                    {
                                        foreach($dh['result'] as $ddh)
                                        {
                                            $c = array(
                                                "collectionid" => base_convert($ddh['collectionid'], 10, 16)
                                            );
                                            $coll[] = $c;
                                        }
                                    }
                                }
                            }
                            
                            $arrcol = array();
                            if(count($coll) > 0)
                            {
                                $no = 0;
                                foreach($coll as $dcol)
                                {
                                    $mmus = $mus->findOne(array("key" => $dcol['collectionid']));
                            
                                    $title = "";
                                    $seo = "";
                                    $description = "";
                                    $track = 0;
                                    $length = 0;
                                    if($mmus)
                                    {
                                        $title = $mmus['title'];
                                        $seo = $mmus['seo'];
                                        $description = $mmus['description'];
                                        $track = $mmus['track'];
                                        $length = $mmus['length'];
                                    }
                                    
                                    $hasil = Http::getStr(DOMAIN."collection/search?id=".$dcol['collectionid']."&type=hexa");
                                    $cccl = json_decode($hasil, true);
                                    
                                    //$arrcol = array();                    
                                    if(is_array($cccl))
                                    {
                                        if(count($cccl) > 2)
                                        {
                                            $arr3 = array(
                                                "collectionid" => intval(base_convert(trim($dcol['collectionid']), 16, 10))
                                            );
                                                                
                                            $t = Http::kirimJson(DOMAIN."song_album/search", json_encode($arr3));
                                            $tr = json_decode($t, true);
                                                                
                                            $albumid = "";                                             
                                            $adadata = false;                                              
                                            if(is_array($tr))
                                            {
                                                if(isset($tr['result']))
                                                {
                                                    foreach($tr["result"] as $ddf)
                                                    {   
                                                        $albumid = base_convert($ddf['albumid'], 10, 16);                                                          
                                                        $adadata = true;                                       
                                                    }    
                                                }                                    
                                            }
                                                                
                                            if($adadata)
                                            {
                                                $name = "";                                
                                                $artisid = "";
                                                $genreid = "";
                                                $imagelink = "";
                                                $albumname = "";
                                                if(strlen(trim($albumid)) > 0)
                                                {
                                                    $ma = Http::getStr(DOMAIN."album/search?id=".$albumid."&type=hexa");
                                                    $ma = preg_replace('/[^[:print:]]/', '', $ma);
                                                    $maa = json_decode($ma, true);
                                                                        
                                                    $artisid = base_convert($maa['artistid'], 10, 16);
                                                    $genreid = base_convert($maa['genreid'], 10, 16);
                                                    $albumname = $maa['title'];
                                                                        
                                                    $imagelink = DOMAIN_STREAM;
                                                    $imagelink .= "images.jpg?id=".$maa["image"];
                                                    $imagelink .= "&appid=".$appid;
                                                    $imagelink .= "&cookie=".$cookie;
                                                                        
                                                    $ar = Http::getStr(DOMAIN."artist/search?id=".$artisid."&type=hexa"); 
                                                    $mar = json_decode($ar, true);
                                                                                         
                                                    if(isset($mar['name']))
                                                        $name = $mar['name'];
                                                }                                                                                                                                                             
                                                                    
                                                $v0 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V0&appid=".$appid."&cookie=".$cookie;
                                                $v2 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V2&appid=".$appid."&cookie=".$cookie;
                                                $v4 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V4&appid=".$appid."&cookie=".$cookie;
                                                                    
                                                /*$ac = Http::getStr(DOMAIN."audio_collection/search?id=".$cccl['id']."&type=hexa");
                                                $acc = json_decode($ac, true);
                                                                                               
                                                $length = 0;                                                                
                                                if(count($acc) > 2)
                                                    $length = $acc['length'];*/ 
                                                                        
                                                $collection = array(
                                                    "index" => $no,
                                                    "collectionid" => $cccl['id'],
                                                    "title" => $title,
                                                    "seo" => $seo,
                                                    "track" => $track,
                                                    "length" => intval($length),
                                                    "artistname" => $name,
                                                    "artistid" => $artisid,
                                                    "genreid" => $genreid,
                                                    "albumid" => $albumid,
                                                    "albumname" => $albumname,
                                                    "album_art" => $imagelink,
                                                    "streamv0" => $v0,
                                                    "streamv2" => $v2,
                                                    "streamv4" => $v4                                    
                                                );
                                                $arrcol[] = $collection;
                                                $no++;
                                            }
                                        }
                                    }
                                }
                            }                            

                            $rpc = array(
                                "result" => $arrcol
                            );
                            
                            echo json_encode($rpc);
                        }
                    }
                }
                else {
                    $rpc = array(
                        "result" => "not found"
                    );
                    
                    echo json_encode($rpc);
                }
            }
        }
    }

    public function gettrackbyplaylistid()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
        $id = Base::instance()->get('GET.id');
        
        $db = Db::init();
        $mus = $db->musics;
        
        if((strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
            $h = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
            $hh = json_decode($h, true);
            if(isset($hh['appname']))
            {
                $ex = "";
                if(strpos($cookie, "_") == true)
                {
                    $dc = explode("_", $cookie);
                    $ex = $dc[0];
                }
                
                $c = Http::getStr(DOMAIN."session/search?id=".$ex."&type=hexa");
                $cc = json_decode($c, true);
                
                if(is_array($cc))
                {
                    if(count($cc) > 0)
                    {
                        if(strlen(trim($id)) > 0)
                        {
                            $arr = array(
                                "playlistid" => intval(base_convert($id, 16, 10))
                            );
                            
                            $hasil = Http::kirimJson(DOMAIN."song_playlist/search", json_encode($arr));
                            $dh = json_decode($hasil, true);
                            
                            $coll = array();
                            if(is_array($dh))
                            {
                                if(isset($dh['result']))
                                {
                                    if(count($dh['result']) > 0)
                                    {
                                        foreach($dh['result'] as $ddh)
                                        {
                                            $c = array(
                                                "collectionid" => base_convert($ddh['collectionid'], 10, 16)
                                            );
                                            $coll[] = $c;
                                        }
                                    }
                                }
                            }
                            
                            $arrcol = array();
                            if(count($coll) > 0)
                            {
                                $no = 0;
                                foreach($coll as $dcol)
                                {
                                    $mmus = $mus->findOne(array("key" => $dcol['collectionid']));
                            
                                    $title = "";
                                    $seo = "";
                                    $description = "";
                                    $track = 0;
                                    $length = 0;
                                    if($mmus)
                                    {
                                        $title = $mmus['title'];
                                        $seo = $mmus['seo'];
                                        $description = $mmus['description'];
                                        $track = $mmus['track'];
                                        $length = $mmus['length'];
                                    }
                                    
                                    $hasil = Http::getStr(DOMAIN."collection/search?id=".$dcol['collectionid']."&type=hexa");
                                    $cccl = json_decode($hasil, true);
                                    
                                    //$arrcol = array();                    
                                    if(is_array($cccl))
                                    {
                                        if(count($cccl) > 2)
                                        {
                                            $arr3 = array(
                                                "collectionid" => intval(base_convert(trim($dcol['collectionid']), 16, 10))
                                            );
                                                                
                                            $t = Http::kirimJson(DOMAIN."song_album/search", json_encode($arr3));
                                            $tr = json_decode($t, true);
                                                                
                                            $albumid = "";
                                              
                                            $adadata = false;                                              
                                            if(is_array($tr))
                                            {
                                                if(isset($tr['result']))
                                                {
                                                    foreach($tr["result"] as $ddf)
                                                    {   
                                                        $albumid = base_convert($ddf['albumid'], 10, 16);                                                         
                                                        $adadata = true;                                       
                                                    }    
                                                }                                    
                                            }
                                                                
                                            if($adadata)
                                            {
                                                $name = "";                                
                                                $artisid = "";
                                                $genreid = "";
                                                $imagelink = "";
                                                $albumname = "";
                                                if(strlen(trim($albumid)) > 0)
                                                {
                                                    $ma = Http::getStr(DOMAIN."album/search?id=".$albumid."&type=hexa");
                                                    $ma = preg_replace('/[^[:print:]]/', '', $ma);
                                                    $maa = json_decode($ma, true);
                                                                        
                                                    $artisid = base_convert($maa['artistid'], 10, 16);
                                                    $genreid = base_convert($maa['genreid'], 10, 16);
                                                    $albumname = $maa['title'];
                                                                        
                                                    $imagelink = DOMAIN_STREAM;
                                                    $imagelink .= "images.jpg?id=".$maa["image"];
                                                    $imagelink .= "&appid=".$appid;
                                                    $imagelink .= "&cookie=".$cookie;
                                                                        
                                                    $ar = Http::getStr(DOMAIN."artist/search?id=".$artisid."&type=hexa"); 
                                                    $mar = json_decode($ar, true);
                                                                                         
                                                    if(isset($mar['name']))
                                                        $name = $mar['name'];
                                                }                                                                                                                                                             
                                                                    
                                                $v0 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V0&appid=".$appid."&cookie=".$cookie;
                                                $v2 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V2&appid=".$appid."&cookie=".$cookie;
                                                $v4 = DOMAIN_STREAM."stream.m3u8?id=".$cccl['id']."&type=audio&rate=V4&appid=".$appid."&cookie=".$cookie;
                                                                    
                                                /*$ac = Http::getStr(DOMAIN."audio_collection/search?id=".$cccl['id']."&type=hexa");
                                                $acc = json_decode($ac, true);
                                                                                               
                                                $length = 0;                                                                
                                                if(count($acc) > 2)
                                                    $length = $acc['length'];*/ 
                                                                        
                                                $collection = array(
                                                    "index" => $no,
                                                    "collectionid" => $cccl['id'],
                                                    "title" => $title,
                                                    "seo" => $seo,
                                                    "track" => $track,
                                                    "length" => intval($length),
                                                    "artistname" => $name,
                                                    "artistid" => $artisid,
                                                    "genreid" => $genreid,
                                                    "albumid" => $albumid,
                                                    "albumname" => $albumname,
                                                    "album_art" => $imagelink,
                                                    "streamv0" => $v0,
                                                    "streamv2" => $v2,
                                                    "streamv4" => $v4                                    
                                                );
                                                $arrcol[] = $collection;
                                                $no++;
                                            }
                                        }
                                    }
                                }
                            }                            

                            $rpc = array(
                                "result" => $arrcol
                            );
                            
                            echo json_encode($rpc);
                        }
                    }
                }
                else {
                    $rpc = array(
                        "result" => "not found"
                    );
                    
                    echo json_encode($rpc);
                }
            }
        }
    }
}