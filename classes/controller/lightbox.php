<?php

class lightbox_controller extends  controller
{
	
	public function hapus()
	{
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "deleteLightbox")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = '';
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$lightboxid = "";
					if(isset($p['lightboxid']))
						$lightboxid = $p['lightboxid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))			
					{
						if($r['error'] == null)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
	                        $ada = false;
	                        if($cek == "OK")
							{
								if(strlen(trim($lightboxid)) > 0)
								{
									$db = Db::init();
									$light = $db->lightboxs;
									$mlight = $db->album_lightboxs;
									$mmlight = $mlight->find(array('lightbox_id' => new MongoId($lightboxid)));
									
									if($mmlight->count() > 0)
									{
										foreach($mmlight as $dmm)
										{
											$mlight->remove(array('_id' => new MongoId($dmm['_id'])));
										}
									}
									
									$light->remove(array('_id' => new MongoId($lightboxid)));
									
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array('delete' => 'SUKSES')
									);
									echo json_encode($hasil);
									return;
								}																								
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if((strlen(trim($lightboxid)) > 0) && (strlen(trim($appid))> 0))
							{
								$ahhga = Http::getStr(DOMAIN."lightbox/delete?id=".$lightboxid);
								$ahhgar= json_decode($ahhga, true);
								if(is_array($ahhgar))
								{
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array($ahhgar)
									);
									echo json_encode($hasil);
									return;
								}
							}
						}
					}*/
				}
			}
		}
		$hasil = array(
			'id' => 5,
			'error' => 204,
		);
		echo json_encode($hasil);
	}
	
	public function insert()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "insertLightbox")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$name = "";
					if(isset($p['name']))
						$name = $p['name'];
					$description = "";
					if(isset($p['description']))
						$description = $p['description'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if((strlen(trim($name)) > 0) && (strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        			{
        				$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$arrg = array(
			                    		"status" => "yes",
										"name" => trim($name),
										"description" => trim($description),
										"userid" => trim($userid)
									);
									$db = Db::init();
									$pl = $db->lightboxs;
									$hs = $pl->findone(array('userid' => trim($userid), "name" => trim($name)));
									if(!isset($hs['_id']))
									{
										$pl->insert($arrg);
										$arr = array(
											"id" => trim($arrg['_id'])
										);
									}
									else {
										$arr = array(
											"id" => trim($hs['_id'])
										);
									}
									$hasil = array(
										"error" => null,
										"result" => array($arr)
									);
									echo json_encode($hasil);
			                    }
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if(strlen(trim($name)) > 0)
							{
								$userid = 0;
								foreach($ddd["result"] as $res)
								{
									$userid = $res["userid"];
								}

								$arrg = array(
									"name" => trim($name),
									"description" => trim($description),
									"userid" => intval($userid)
								);
								$hhhg = Http::kirimJson(DOMAIN."lightbox/insert", json_encode($arrg));
								echo $hhhg;
							}
						}
					}*/
				}
			}
		}
	}
	
	public function getbyname()
	{
		if(!empty($_POST))
		{
			$appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$cookie = "";
			if(isset($_POST['cookie']))
				$cookie = $_POST['cookie'];
			$userid = "";
			if(isset($_POST['userid']))
				$userid = $_POST['userid'];
			$name = "";
			if(isset($_POST['name']))
				$name = $_POST['name'];
			
			$validator = new Validator();
	        $validator->addRule('userid', array('require'));
	        $validator->addRule('cookie', array('require'));
	        $validator->addRule('appid', array('require'));
			$validator->addRule('name', array('require'));
	        $validator->setData(array(
	        	'userid' => $userid,
	            'cookie' => $cookie,
	            'appid' => $appid,
	            'name' => $name
	        ));		
			
			if($validator->isValid())
			{
				$cekcookie = helper::cekCookie($userid, $cookie);
				if($cekcookie)
				{
					$db = Db::init();
					$gen = $db->lightboxs;
					$tunin = $gen->find(
						array("name" => ucfirst(trim($name)))
					);
					
					$arr = array();
					foreach($tunin as $dd)
					{
						$p = array(
							'id' => trim($dd['_id']),
							'name' => preg_replace("/#?[a-z0-9]+;/i", '', trim($dd['name'])),
							'description' => trim($dd['description']),
						);
						$arr[] = $p;
					}
					$pp = array(
						'result' => "SUCCESS",
						'data' => $arr,
						'error' => null
					);
                                
                    echo json_encode($pp);
					return;
				}
			}
		}
		
		$pp = array(
			'result' => "FAILED",
			'data' => array(),
			'error' => null
		);
	                
	    echo json_encode($pp);
		return;
	}
	
	public function get()
	{
		
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getLightboxByName")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$name = "";
					if(isset($p['name']))
						$name = $p['name'];
					//$ex = explode("_", $cookie);	
					//$arr = array(
					//	"cookie" => trim($ex[1])
					//);
					
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					//echo $res;
					//$r = json_decode(json_encode($res), true);
					$r = json_decode($res, true);
					if(is_array($r))				
					//$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					//$ddd = json_decode($hhh, true);
					//if(isset($ddd["result"]))
					{
						//if(count($ddd["result"]) > 0)
						//{
							if($r['error'] == null)
							{
								$cek = '';
		                    	foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
								
		                        $ada = false;
		                        if($cek == "OK")
								{
							
									if(strlen(trim($name)) > 0)
									{
										$db = Db::init();
										$gen = $db->lightboxs;
										$tunin = $gen->find(
											array("name" => ucfirst(trim($name)))
										);
										
										$arr = array();
										foreach($tunin as $dd)
										{
											$p = array(
												'id' => trim($dd['_id']),
												'name' => preg_replace("/#?[a-z0-9]+;/i", '', trim($dd['name'])),
												'description' => trim($dd['description']),
											);
											$arr[] = $p;
										}
										$pp = array(
											'id' => 10,
											'result' => $arr,
											'error' => null
										);
			                                        
			                            echo json_encode($pp);
										return;
								
										//$hhhg = Http::kirimJson(DOMAIN."lightbox/search", json_encode($arrg));
										//$dddg = json_decode($hhhg, true);
										//if(is_array($dddg))
										//{
										//	echo json_encode($dddg);
										//}
		                                //else {
		                                //    echo "Not Found";
		                                //}
									}
								}
							}
					}
				}
			}
		}

		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}

	public function getbyuser()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getAllLightboxByUser")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$name = "";
					if(isset($p['name']))
						$name = $p['name'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if((strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        			{
        				$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$db = Db::init();
									$play = $db->lightboxs;
									
									$quer = array(
										'userid' => trim($userid)
									);
									$mplay = $play->find($quer);
									
									$hasil = array(
										'error' => null,
										'result' => array()
									);
									if($mplay->count() > 0)
									{
										$data = array();
										foreach($mplay as $dt)
										{
											$h = array(
												'id' => trim($dt['_id']),
												'userid' => trim($dt['userid']),
												'name' => trim($dt['name']),
												'description' => trim($dt['description'])
											);
											$data[] = $h;
										}
										$hasil = array(
											'error' => null,
											'result' => $data
										);
									}
									echo json_encode($hasil);
			                    }
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							foreach($ddd["result"] as $res)
							{
								$userid = $res["userid"];
								$arrg = array(
									"userid" => intval($userid)
								);
								$hhhg = Http::kirimJson(DOMAIN."lightbox/search", json_encode($arrg));
								$dddg = json_decode($hhhg, true);
								if(is_array($dddg))
								{
									echo json_encode($dddg);
								}
                                else {
                                    echo "Not Found";
                                }
							}
						}
					}*/
				}
			}
		}
	}
}