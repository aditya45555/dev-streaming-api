<?php

class user_controller extends controller
{
    
	public function registerApp()
	{
		$email = Base::instance()->get('GET.email');
		$appid = Base::instance()->get('GET.appid');
				
		
		if((strlen(trim($appid)) > 0) && (strlen(trim($email)) > 0))
		{
			$link = CDN.'user/list?skip=0&limit=0';
			
			$hasil = Http2::getStr($link);
            $data = json_decode($hasil, true);
			
			$uid = '';
			if(is_array($data['result']))
			{
				foreach($data['result'] as $ddd)
				{
					if(isset($ddd['email']))
					{
						if((strtolower($ddd['email']) == strtolower($email)) && (strtolower($ddd['appid']) == strtolower($appid)))
						{
							$uid = $ddd['id'];
							break;
						}							
					}
				}												
			}
			else {
				$domain = CDN.'user/register?email='.$email.'&appid='.$appid;
			
				$hasil = Http::getStr($domain);
				
				$data = json_decode($hasil, true);
							
				if(is_array($data['result']))
				{
					$ddd = $data['result'][0];
					
					if(isset($ddd['insert']))
					{
						if(strtolower($ddd['insert']) == 'success')
							$uid = $ddd['id'];
					}
				}
			}			
			
			$dd = array(
				'userid' => trim($uid)
			);
			
			echo json_encode($dd);
		}
	}
	
	public function register()
    {
        $body = @file_get_contents('php://input');
        $b = json_decode($body, true);
        if(is_array($b))
        {
            if(isset($b['method']))
            {
            	$par = $b['params'];
                $p = $par[0];
                $appid = "";
                if(isset($p['appid']))
                    $appid = $p['appid'];
				$ssoid = "";
                if(isset($p['ssoid']))
                    $ssoid = $p['ssoid'];
				$userid = "";
                if(isset($p['userid']))
                    $userid = $p['userid'];
                $name = "";
                if(isset($p['name']))
                    $name = $p['name'];
				$image = "";
                if(isset($p['image']))
                    $image = $p['image'];
                $password = "";
                if(isset($p['password']))
                    $password = $p['password'];
                $email = "";
                if(isset($p['email']))
                    $email = $p['email'];
                $gender = "";
                if(isset($p['gender']))
                    $gender = $p['gender'];
                $date = "";
                if(isset($p['birthdate']))
                    $date = $p['birthdate'];
				
                if($b['method'] == "userRegister")
                {
                    if(strlen(trim($appid)) > 0)
                    {
                    	$db = Db::init();        
				        $user = $db->user_commons;
				        $muser = $user->findOne(array("email" => $email));
						
						if(strlen(trim($muser['email'])) > 0)
						{
							$p = array(
								'userid' => $muser['ssoid'],
								'result' => 'SUCCESS'
							);
							$pp[] = $p;
							$hasil = array(
								'id' => 5,
								'error' => null,
								'result' => $pp
							);
							echo json_encode($hasil);
							return;
						}
						else
						{
							$data = array(
								"appid" => $appid,
								"name" => $name,
		                        "password" => $password,
		                        "email" => $email,
		                        "gender" => $gender,
		                        "birthdate" => $date,
		                        "time_created" => time(),
							);
							$djson = array(
								'method' => 'registerUser',
								'params' => array($data)
							);
							
							$json = json_encode($djson);
							$curl = new Curl();
							$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
							$curl->post(SSO_URL.'user/registerjson', $json);
							$h = json_decode($curl->response, true);
							$hh = $h['result'][0];
							
							if (isset($hh['user_id']))
							{
								if(strlen(trim($hh['user_id'])) > 0)
								{
									$u = array(
										'ssoid' => $hh['user_id'],
										"name" => $name,
										"image" => $image,
				                        "password" => md5($password),
				                        "login_type" => "common",
				                        "email" => $email,
				                        "gender" => $gender,
				                        "birthdate" => $date,
				                        "time_created" => time(),
									);
									$user->insert($u);
									
									$p = array(
										'userid' => $hh['user_id'],
										'result' => 'SUCCESS'
									);
									$pp[] = $p;
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => $pp
									);
									echo json_encode($hasil);
									return;
								}
							}
						}
                    }
                }

				if($b['method'] == "userSocialRegister")
                {
                    if(strlen(trim($appid)) > 0)
                    {
                    	$db = Db::init();        
				        $user = $db->user_commons;
				        $muser = $user->findOne(array("email" => $email));
						
						if(strlen(trim($muser['email'])) > 0)
						{
							if(strlen(trim($muser['userid'])) > 0){
								$var = array( 'userid' => $userid );
								$user->update(array('_id' => new MongoId(trim($muser['_id']))), array('$set' => $var));
							}
							$p = array(
								'userid' => $muser['ssoid'],
								'result' => 'SUCCESS'
							);
							$pp[] = $p;
							$hasil = array(
								'id' => 5,
								'error' => null,
								'result' => $pp
							);
							echo json_encode($hasil);
							return;
						}
						else
						{
							$u = array(
								'ssoid' => $ssoid,
								'userid' => $userid,
								"name" => $name,
								"image" => $image,
		                        "password" => md5($password),
		                        "login_type" => "common",
		                        "email" => $email,
		                        "gender" => $gender,
		                        "birthdate" => $date,
		                        "time_created" => time(),
							);
							$user->insert($u);
							
							$p = array(
								'userid' => $hh['user_id'],
								'result' => 'SUCCESS'
							);
							$pp[] = $p;
							$hasil = array(
								'id' => 5,
								'error' => null,
								'result' => $pp
							);
							echo json_encode($hasil);
							return;
						}
                    }
                }
            }
        }
		$p = array(
			'userid' => 'Undefined',
			'result' => 'ERROR'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
    }

	public function login()
    {
        $body = @file_get_contents('php://input');
        $b = json_decode($body, true);
		if(is_array($b))
        {
            if(isset($b['method']))
            {
                if($b['method'] == "login")
                {
                    $par = $b['params'];
                    $p = $par[0];
                    $appid = "";
                    if(isset($p['appid']))
                        $appid = $p['appid'];
                    $email = "";
                    if(isset($p['email']))
                        $email = $p['email'];
                    $password = "";
                    if(isset($p['password']))
                        $password = $p['password'];
                    
                    if(strlen(trim($appid)) > 0)
                    {
                    	$db = Db::init();
				        $user = $db->user_commons;
				        $muser = $user->findOne(array("email" => $email, "password" => md5($password)));
						if(strlen(trim($muser['email'])) > 0){
							$data = array(
								'appid' => $appid,
								'password' => $password,
								'email' => $email
							);
							
							$djson = array(
								'method' => 'loginUser',
								'params' => array($data)
							);
							print_r($djson);
							$json = json_encode($djson);
							$curl = new Curl();
							$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
							$curl->post(SSO_URL.'user/loginjson', $json);
							
							$h = json_decode($curl->response, true);
							$hh = $h['result'][0];
							print_r($h);
							if(isset($hh['user_id']))
							{
								if(strlen(trim($hh['user_id'])) > 0)
								{
									$cookie = trim($hh['cookie']);
									$name = trim($hh['name']);
									$type = trim($hh['type']);
									$memail = trim($hh['email']);
									$id = trim($hh['user_id']);
									$gender ="";
									if (isset($muser['gender']))
										$gender =$muser['gender'];
									
									$p = array(
										'userid' => $id,
										'name' => $name,
										'type' => $type,
										'email' => $memail,
										'username' => $memail,
										'image' => "",
										'gender' => $gender,
										'cookie' => $cookie,
										'result' => 'SUCCESS'
									);
									$pp[] = $p;
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => $pp
									);
									echo json_encode($hasil);
									return;
									
								}
							}
						}
                    }
                }
            }
        }
		$p = array(
			'userid' => '',
			'name' => '',
			'type' => '',
			'email' => '',
			'cookie' => '',
			'result' => 'ERROR'
		);
		$pp[] = $p;
		$hasil = array(
			'id' => 5,
			'error' => 204,
			'result' => $pp
		);
		echo json_encode($hasil);
    }
	
	public function loginjson()
	{
		
		if(!empty($_POST))
		{	
			$appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$email = "";
			if(isset($_POST['email']))
				$email = $_POST['email'];
			$name = "";
			if(isset($_POST['name']))
				$name = $_POST['name'];
			$password = "";
			if(isset($_POST['password']))
				$password = $_POST['password'];
			$type = "";
			if(isset($_POST['type']))
				$type = $_POST['type'];
			$facebookid = "";
			if(isset($_POST['facebookid']))
				$facebookid = $_POST['facebookid'];
			$googleid = "";
			if(isset($_POST['googleid']))
				$googleid = $_POST['googleid'];
			
								
			$validator = new Validator();	
			$validator->addRule('appid', array('require', 'minlength' => 3));
			$validator->addRule('type', array('require'));
			
			//echo 'masuk1';
			$validator->setData(
	           array(
				  'appid' => $appid,
				  'type' => $type
	        ));
			
			$db = Db::init();
			$user = $db->users;
			$de = $db->devices;
			$du = $db->device_users;
			if ($validator->isValid())
			{
				//echo 'masuk2';
				$he = $de->findone(array('appid' => $appid));
				if(isset($he['_id']))
				{
					if(strtolower($type)=="google")
					{
						if(strlen($googleid)>0)
						{
							$datuser = $user->findOne(array("email" => $email));
							if(isset($datuser['_id']))
							{
								$p = array(
									"email"=>$email,
									"app_id" => trim($appid)
								);
								$resultLogin = $this->dologin($p,$type);
								$resultLogin = $resultLogin['result'][0];
								$p = array(
									'cookie' => trim($resultLogin['cookie']),
									'name' => $resultLogin['name'],
									'type' => 'common',
									'email' => $resultLogin['email'],
									'user_id' => trim($resultLogin['user_id'])
								);
								$hasil[] = $p;
								$pp = array(
									'id' => 10,
									'result' => "SUCCESS",
									'data' => $hasil,
									'error' => null
								);
								echo json_encode($pp);
								return;
								
							}
							else
							{
								//register
								$p = array(
									"email" => $email,
									"name" => $name,
									"googleid" => $googleid,
									"time_created" => time(),
									"last_login" => time(),
									"status" => "verivied"
								);
								$user->insert($p);
								
								$p = array("email" => $email,"app_id" => trim($appid));
								$resultLogin = $this->dologin($p,$type);
								$resultLogin = $resultLogin['result'][0];
								$p = array(
									'cookie' => trim($resultLogin['cookie']),
									'name' => $resultLogin['name'],
									'type' => $type,
									'email' => $resultLogin['email'],
									'user_id' => trim($resultLogin['user_id'])
								);
								$hasil[] = $p;
								$pp = array(
									'id' => 10,
									'result' => "SUCCESS",
									'data' => $hasil,
									'error' => null
								);
								echo json_encode($pp);
								return;
								
							}
						}
					}
					else if(strtolower($type)=="facebook")
					{
						
						if(strlen($facebookid)>0)
						{
							$datuser = $user->findOne(array("email" => $email));
							if(isset($datuser['_id']))
							{
								$p = array("email" => $email,"app_id" => trim($appid));
								$resultLogin = $this->dologin($p,$type);
								$resultLogin = $resultLogin['result'][0];
								$p = array(
									'cookie' => trim($resultLogin['cookie']),
									'name' => $resultLogin['name'],
									'type' => $type,
									'email' => $resultLogin['email'],
									'user_id' => trim($resultLogin['user_id'])
								);
								$hasil[] = $p;
								$pp = array(
									'id' => 10,
									'result' => "SUCCESS",
									'data' => $hasil,
									'error' => null
								);
								echo json_encode($pp);
								return;
								
							}
							else
							{
								//register
								$p = array(
									"email" => $email,
									"name" => $name,
									"facebookid" => $facebookid,
									"time_created" => time(),
									"last_login" => time(),
									"status" => "verivied"
								);
								$user->insert($p);
								
								$p = array("email" => $email,"app_id" => trim($appid));
								$resultLogin = $this->dologin($p,$type);
								$resultLogin = $resultLogin['result'][0];
								$p = array(
									'cookie' => trim($resultLogin['cookie']),
									'name' => $resultLogin['name'],
									'type' => $type,
									'email' => $resultLogin['email'],
									'user_id' => trim($resultLogin['user_id'])
								);
								$hasil[] = $p;
								$pp = array(
									'id' => 10,
									'result' => "SUCCESS",
									'data' => $hasil,
									'error' => null
								);
								echo json_encode($pp);
								return;
								
								
							}
						}
					}
					else
					{
						if(strlen($email)>0)
						{
							$datuser = $user->findOne(array("email" => $email));
							if(isset($datuser['_id']))
							{
								$p = array(
									"email" => $email,
									"password" => $password,
									"app_id" => trim($appid)
					
								);
								$resultLogin = $this->dologin($p,$type);
								$resultLogin = $resultLogin['result'][0];
								$p = array(
									'cookie' => trim($resultLogin['cookie']),
									'name' => $resultLogin['name'],
									'type' => 'common',
									'email' => $resultLogin['email'],
									'user_id' => trim($resultLogin['user_id'])
								);
								$hasil[] = $p;
								$pp = array(
									'id' => 10,
									'result' => "SUCCESS",
									'data' => $hasil,
									'error' => null
								);
								echo json_encode($pp);
								return;
								
							}
						}
					}
				}
				
				
				
			}
		}
			
		$pp = array(
			'result' => "FAILED",
			'error' => 204
		);
		echo json_encode($pp);
	}
	
	private function dologin($data,$type)
	{
		$db = Db::init();
        
        $userTbl = $db->users;
        $userDetail = $userTbl->findOne(array("email" => $data['email']));//,'password'=>  Base::Instance()->encrypt($data['pass'])
		
			//print_r($data);print_r($userDetail);return;
       	$cek = FALSE;
		if(strtolower($type)=="login")
		{
			$cek = $this->check_password($userDetail['password'], $data['password']);
		}
		else
		{
			$cek =TRUE;	
		}
		
		
	    if($cek)
	    {
	    	$cookie = helper::createCookie();
	        $dbcookie = $db->cookies;

	        $var = array(
	                    'cookie' => trim($cookie),
	                    'user_id' => trim($userDetail['_id']),
	                    'time_created' => time(),
	                    'app_id' => $data['app_id'],
	                    'last_time' => time(),
	                    'expired' => 3600
	        );
	        $dbcookie->insert($var);

	        $p = array(
	                    'cookie' => trim($cookie),
	                    'name' => $userDetail['name'],
	                    'email' => $userDetail['email'],
	                    'user_id' => trim($userDetail['_id'])
	        );
		    
	      	$hasil[] = $p;
	        $pp = array(
	                    'id' => 10,
	                    'result' => $hasil,
	                    'error' => null
	        );
	            //echo json_encode($pp);
	        return $pp;
	    }
	    else
			return 0;
        
	}
	
	
	private function check_password($password_hash, $password)
	{
		include_once(PASSCRYPT);
		//echo PHPASS_HASH_STRENGTH.'--'.PHPASS_HASH_PORTABLE;return;
		$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);

		return $hasher->CheckPassword($password, $password_hash) ? TRUE : FALSE;
	}
	
	public function registerjson()
	{
		if(!empty($_POST))
		{
			$appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$email = "";
			if(isset($_POST['email']))
				$email = $_POST['email'];
			$name = "";
			if(isset($_POST['name']))
				$name = $_POST['name'];
			$password = "";
			if(isset($_POST['password']))
				$password = $_POST['password'];
			$type = "";
			$birthday = "";
			if(isset($_POST['birthday']))
				$birthday = $_POST['birthday'];
			$gender = "";
			if(isset($_POST['gender']))
				$gender = $_POST['gender'];
			
			
			$validator = new Validator();
	        $validator->addRule('email', array('require', 'email', 'minlength' => 3));
	        $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('appid', array('require', 'minlength' => 3));
	
			$validator->setData(
	           array(
				  'email' => $email,
				  'name' => $name,
				  'appid' => $appid,
	        ));
			
			$db = Db::init();
			$user = $db->users;
			$de = $db->devices;
			$cekEmail = "valid";
			//$cekEmail = $this->verifyEmail($_POST['email'], 'digitamaindonesia@gmail.com');
			
			if ($validator->isValid() ) //&& ("valid" == $cekEmail)
			{
				$he = $de->findone(array('appid' => $appid));
				if(isset($he['_id']))
				{
					if(strlen($email)>0)
					{
						$datuser = $user->findOne(array("email" => $email));
						if(isset($datuser['_id']))
						{
							$pp = array(
								'result' => "FAILED",
								'error' => "User Already"
							);
							echo json_encode($pp);
							return;
							
						}
						else
						{
							//Register
							$aktivasi_code = helper::createCookie();
							include_once(PASSCRYPT);
							$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
							$hashed_password = $hasher->HashPassword($password);
							//$aktivasi_code = Base::Instance()->createCookie();
							$data = array(
								'name' => $name,
							  	'email' => $email,
							  	'aktivasikode' => $aktivasi_code,
							  	'password' => $hashed_password,//Base::Instance()->encrypt($password),
								'birthday' => strtotime($birthday),
								'gender' => $gender,
							  	'active' => "unverivied",
							  	'time_created' => time()
							);
							
							$user->insert($data);
							
							$dbcookie = $db->cookies;
					        $var = array(
					                    'cookie' => trim($aktivasi_code),
					                    'user_id' => trim($data['_id']),
					                    'time_created' => time(),
					                    'app_id' => $appid,
					                    'last_time' => time(),
					                    'expired' => 3600
					        );
					        $dbcookie->insert($var);
							
							//verivikasi email
							/*$emailContent = "Please Klik link below to Activated your digitama account's:".
								  DOMAIN.'user/userActivation/?act='.$aktivasi_code;
							$this->sendEmail("Activation of Digitama account's",$emailContent,$email);*/
							
							$p = array(
								'user_id' => trim($data['_id']),
								'cookie' => $aktivasi_code,
								'email' => $email,
								'name' => $name
							);
							
							$hasil[] = $p;
							$pp = array(
								'result' => "SUCCESS",
								'data' => $hasil,
								'error' => null
							);
							echo json_encode($pp);
							return;
						}
					}
					
				}
				
			   }
			}
				
			
		$pp = array(
			
			'result' => "FAILED",
			'error' => 204
		);
		echo json_encode($pp);
	}
	
	
	public function registerplayid()
    {
        $body = @file_get_contents('php://input');
        $b = json_decode($body, true);
        if(is_array($b))
        {
            if(isset($b['method']))
            {
                if($b['method'] == "registerPlayId")
                {
                    $par = $b['params'];
                    $p = $par[0];
                    $userid = "";
                    if(isset($p['userid']))
                        $userid = $p['userid'];
                    $email = "";
                    if(isset($p['email']))
                        $email = $p['email'];
					                             
                    if(strlen(trim($userid)) > 0 && strlen(trim($email)) > 0)
                    {
                    	$db = Db::init();        
				        $user = $db->user_commons;
				        $muser = $user->findOne(array("email" => $email));
						
						if(strlen(trim($muser['_id'])) > 0)
						{
							$var = array( 'userid' => $userid );
							$user->update(array('_id' => new MongoId(trim($muser['_id']))), array('$set' => $var));
							$p = array( 'result' => 'SUCCESS' );
							$pp[] = $p;
							$hasil = array(
								'id' => 5,
								'error' => null,
								'result' => $pp
							);
							echo json_encode($hasil);
							return;
						}
                    }
                }
            }
        }
		$p = array( 'result' => 'ERROR' );
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
    }
    
    public function reg()
    {
        $body = @file_get_contents('php://input');
        $b = json_decode($body, true);
        if(is_array($b))
        {
            if(isset($b['method']))
            {
                if($b['method'] == "userRegister")
                {
                    $par = $b['params'];
                    $p = $par[0];
                    $appid = "";
                    if(isset($p['appid']))
                        $appid = $p['appid'];
                    $name = "";
                    if(isset($p['name']))
                        $name = $p['name'];
                    $password = "";
                    if(isset($p['password']))
                        $password = $p['password'];
                    $email = "";
                    if(isset($p['email']))
                        $email = $p['email'];
                    $gender = "";
                    if(isset($p['gender']))
                        $gender = $p['gender'];
                    $date = "";
                    if(isset($p['birthdate']))
                        $date = $p['birthdate'];
                             
                    if(strlen(trim($appid)) > 0)
                    {
                        $app = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
                        
                        $mapp = json_decode($app, true);
                        
                        if(count($mapp) > 2)
                        {                            
                            $kval = Http::getStr(DOMAIN."evalue/search?id=".$appid."&type=hexa");
                           
                            $kvalue = json_decode($kval, true);
                            
                            if(strlen(trim($kvalue[$appid])) > 0)
                            {
                                $name = $this->encript($kvalue[$appid], $name, "decrypt");
                                $pass = $this->encript($kvalue[$appid], $password, "decrypt");
                                $email = $this->encript($kvalue[$appid], $email, "decrypt");
                                //$gender = $this->encript($kvalue[$appid], $gender, "decrypt");
                                //$birthdate = $this->encript($kvalue[$appid], $date, "decrypt");                                                                
                                
                                $emailcheck = Http::getStr(DOMAIN."email/search?id=".$email."&type=string");
                                
                                $jemail = json_decode($emailcheck, true);
                                if(strtolower($jemail['search']) == "not found")
                                {
                                    $arr = array(
                                        "email" => $email
                                    );                                    
                                    $usercheck = Http::kirimJson(DOMAIN."user_register/search", json_encode($arr));
                               
                                    if(strtolower($usercheck) == "not found")
                                    {
                                        $cookie = $this->createCookie();
                                        $id = $this->ambilHexTgl();
                                        $tgl = $this->ambilTgl();
                                        
                                        $jsonText = array(
                                            "name" => $name,
                                            "password" => $pass,
                                            "email" => $email,
                                            "gender" => $gender,
                                            "birthdate" => $date,
                                            "time_created" => $tgl,
                                            "code_verifikasi" => $cookie
                                        );
                                        
                                        $usreg = Http::kirimJson(DOMAIN."user_register/insert?id=".$id, json_encode($jsonText));
                                        
                                        $dt = json_decode($usreg, true);
                                        if(strtolower($dt['insert']) == "sukses")
                                        {
                                            $arr2 = array(
                                                "code_verifikasi" => $cookie
                                            );
                                            $getid = Http::kirimJson(DOMAIN."user_register/search", json_encode($arr2));
                                            
                                            $jid = json_decode($getid, true); 
                                                                                      
                                            if(isset($jid['result']))
                                            {
                                                if(count($jid['result']) > 0)
                                                {
                                                    $uid = "";
                                                    foreach($jid['result'] as $jj)
                                                    {
                                                        $uid = $jj['id'];
                                                    }                                                                                                
                                                    
                                                    $link = DOMAIN_API;
                                                    $link .= "user/verifikasi?uid=".$uid;
                                                    $link .= "&code=".$cookie;
                                                    
                                                    $rpc = array(
                                                        "uid" => $uid,
                                                        "link" => $link
                                                    );
                                                    
                                                    echo json_encode($rpc);    
                                                }                                                
                                            }
                                        }    
                                    }                                    
                                }                                                                
                            }
                        }                       
                    }
                }
            }
        }
    }

    public function verifikasi()
    {
        $uid = Base::instance()->get('GET.uid');
        $code = Base::instance()->get('GET.code');
        
        if((strlen(trim($uid)) > 0) && (strlen(trim($code)) > 0))
        {
            $ckode = Http::getStr(DOMAIN."user_register/search?id=".$uid."&type=hexa");            
            $cc = json_decode($ckode, true);
            
            if(count($cc) > 2)
            {
                $id = $this->ambilHexTgl();
                $time = $this->ambilTgl();
                $jsonText = array(
                    "email" => $cc['email'],
                    "time_created" => $time,
                    "gender" => $cc['gender'],
                    "birthdate" => $cc['birthdate'],
                    "password" => $cc['password'],
                    "name" => $cc['name'],
                    "city" => "",
                    "country" => "",
                    "status_member" => "new",
                    "telp" => ""
                );
                
                $usr = Http::kirimJson(DOMAIN."user/insert?id=".$id, json_encode($jsonText));
                $mus = json_decode($usr, true);
                
                if(strtolower($mus['insert']) == "sukses")
                {
                    $eml = Http::kirimJson(DOMAIN."email/insert?id=".$cc['email'], $id);
                    $meml = json_decode($eml, true);
                    
                    if(strtolower($meml['insert']) == "sukses")
                    {
                        $del = Http::getStr(DOMAIN."user_register/delete?id=".$uid);            
                        $cdel = json_decode($del, true);
                        
                        if(strtolower($cdel['delete']) == "sukses")
                        {
                            $rpc = array(
                                "uid" => $id
                            );
                            
                            echo json_encode($rpc);
                        }
                    }
                }
            }
        }
    }
    
    public function log()
    {
        $body = @file_get_contents('php://input');
        $b = json_decode($body, true);
        if(is_array($b))
        {
            if(isset($b['method']))
            {
                if($b['method'] == "login")
                {
                    $par = $b['params'];
                    $p = $par[0];
                    $appid = "";
                    if(isset($p['appid']))
                        $appid = $p['appid'];
                    $email = "";
                    if(isset($p['email']))
                        $email = $p['email'];
                    $password = "";
                    if(isset($p['password']))
                        $password = $p['password'];
                    
                    if(strlen(trim($appid)) > 0)
                    {
                        $app = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
                        
                        $mapp = json_decode($app, true);
                        
                        if(count($mapp) > 2)
                        {                            
                            $kval = Http::getStr(DOMAIN."evalue/search?id=".$appid."&type=hexa");
                           
                            $kvalue = json_decode($kval, true);
                            
                            if(strlen(trim($kvalue[$appid])) > 0)
                            {
                                $pass = $this->encript($kvalue[$appid], $password, "decrypt");
                                $email = $this->encript($kvalue[$appid], $email, "decrypt");
                                
                                $emailcheck = Http::getStr(DOMAIN."email/search?id=".$email."&type=string");
                                
                                $jemail = json_decode($emailcheck, true);
                                
                                if(is_array($jemail))
                                {
                                    if(count($jemail) > 0)
                                    {
                                        $userid = "";
                                        foreach($jemail as $k=>$v)
                                        {
                                            $userid = $v;
                                        }
                                        
                                        if(strlen(trim($userid)) > 0)
                                        {
                                            $usr = Http::getStr(DOMAIN."user/search?id=".$userid."&type=hexa");
                                            $musr = json_decode($usr, true);
                                            
                                            if(is_array($musr))
                                            {
                                                if(count($musr) > 2)
                                                {
                                                    $user_id = $this->ambilHexTgl();
                                                    $cookie = $this->createCookie();
                                                    $time = $this->ambilTgl();
                                                    
                                                    $arr2 = array(
                                                        "userid" => intval(base_convert($user_id, 16, 10)),
                                                        "cookie" => $cookie,
                                                        "time_created" => $time,
                                                        "appid" => intval(base_convert($appid, 16, 10))
                                                    );
                                                    
                                                    $cook = Http::kirimJson(DOMAIN."session/insert?id=".$user_id, json_encode($arr2));                                                    
                                                    $mcook = json_decode($cook, true);
                                                    
                                                    if(is_array($mcook))
                                                    {
                                                        if(strtolower($mcook['insert']) == "sukses")
                                                        {
                                                            $arr = array(
                                                                "id" => $userid,
                                                                "email" => $musr['email'],
                                                                "cookie" => $user_id."_".$cookie
                                                            );
                                                            
                                                            $rpc = array(
                                                                "error" => null,
                                                                "result" => array($arr)
                                                            );
                                                            
                                                            echo json_encode($rpc);    
                                                        }
                                                    }                                                                                                        
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }   
    }

    public function account()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
        
        if((strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))
        {
            $h = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
            $hh = json_decode($h, true);
            if(isset($hh['appname']))
            {
                $ex = "";
                if(strpos($cookie, "_") == true)
                {
                    $dc = explode("_", $cookie);
                    $ex = $dc[0];
                }
                
                $c = Http::getStr(DOMAIN."session/search?id=".$ex."&type=hexa");
                $cc = json_decode($c, true);
                
                if(is_array($cc))
                {
                    if(count($cc) > 0)
                    {
                        $userid = base_convert($cc['userid'], 10, 16);
                        
                        $fb = array();
                        $tweet = array();
                        $google = array();
                        
                        $f = Http::getStr(DOMAIN."facebook/search?id=".$userid);
                        $ff = json_decode($f, true);
                        
                        if(count($ff) > 2)
                        {
                            $fff = array(
                                "email" => $ff['email'],
                                "type" => "facebook"
                            );
                            $fb[] = $fff;
                        }
                        
                        $g = Http::getStr(DOMAIN."google/search?id=".$userid);
                        $gg = json_decode($g, true);
                        
                        if(count($gg) > 2)
                        {
                            $ggg = array(
                                "email" => $gg['email'],
                                "type" => "google"
                            );
                            $google[] = $ggg;
                        }
                        
                        $t = Http::getStr(DOMAIN."twitter/search?id=".$userid);
                        $tw = json_decode($t, true);
                        
                        if(count($tw) > 2)
                        {
                            $tt = array(
                                "email" => $tw['email'],
                                "type" => "twitter"
                            );
                            $tweet[] = $tt;
                        }
                        
                        $rpc = array(
                            "facebook" => $fb,
                            "google" => $google,
                            "twitter" => $tweet
                        );
                        
                        echo json_encode($rpc);
                    }
                }
            }
        }
    }

    private function encript($key, $data, $type="encrypt")
    {
        $crypt = new CkCrypt2();
        $success = $crypt->UnlockComponent('WILLAWCrypt_KM8tJZPHMRLn');
        if ($success != true) {
            print $crypt->lastErrorText() . "\n";
            return;
        }
        
        $crypt->put_EncodingMode('hex');
        $crypt->put_HashAlgorithm('md5');
        
        $sessionKey = $crypt->hashStringENC($key);
        
        //  Encrypt something...
        $crypt->put_CryptAlgorithm('aes');
        $crypt->put_KeyLength(128);
        $crypt->put_CipherMode('cbc');
        
        //  Use an IV that is the MD5 hash of the session key...
        
        $iv = $crypt->hashStringENC($sessionKey);
        
        $crypt->SetEncodedKey($sessionKey,'hex');
        $crypt->SetEncodedIV($iv,'hex');
        
        //  Encrypt some text:
        
        $crypt->put_EncodingMode('base64');
        $cipherText64 = $crypt->encryptStringENC($data);
        
        if($type == 'decrypt')
            $cipherText64 = $crypt->decryptStringENC($data);
        
        return $cipherText64;               
    }

    private function ambilTgl()
    {
        $a = explode(" ", microtime());
        $b = explode(".", $a[0]);
        $c = substr($b[1], 0, 6);
        $d = $a[1].$c;
        return $d;
    }
    
    private function ambilHexTgl()
    {
        $a = explode(" ", microtime());
        $b = explode(".", $a[0]);
        $c = substr($b[1], 0, 6);
        $d = $a[1].$c;
        return base_convert($d, 10, 16);
    }

    private function createCookie()
    {
        $crypt = new CkCrypt2() ;

        $success = $crypt->UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
        if ($success != true) {
            printf("%s\n",$crypt->lastErrorText());
            return "";
        }

        $crypt->put_CryptAlgorithm("aes");
        $crypt->put_CipherMode("cbc");
        $crypt->put_KeyLength(256);
        $crypt->put_PaddingScheme(0);
        $crypt->put_EncodingMode("hex");

        return $crypt->genRandomBytesENC(32);
    }
}
