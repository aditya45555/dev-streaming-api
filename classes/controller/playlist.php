<?php

class playlist_controller extends  controller
{
	public function hapus()
	{
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "deletePlaylist")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$playlistid = "";
					if(isset($p['playlistid']))
						$playlistid = $p['playlistid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if(strlen(trim($playlistid)) > 0)
					{
						$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$db = Db::init();
									$pl = $db->playlists;
									$mpl = $db->music_playlists;
									$mmpl = $mpl->find(array('playlist_id' => new MongoId($playlistid)));
									
									if($mmpl->count() > 0)
									{
										foreach($mmpl as $dpl)
										{
											$mpl->remove(array('_id' => new MongoId($dpl['_id'])));
										}
									}									
									
									$pl->remove(array('_id' => new MongoId($playlistid)));
									
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array('delete' => 'SUKSES')
									);
									echo json_encode($hasil);
									return;
			                    }
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if((strlen(trim($playlistid)) > 0) && (strlen(trim($appid))> 0))
							{
								$ahhga = Http::getStr(DOMAIN."playlist/delete?id=".$playlistid);
								$ahhgar= json_decode($ahhga, true);
								if(is_array($ahhgar))
								{
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array($ahhgar)
									);
									echo json_encode($hasil);
									return;
								}
							}
						}
					}*/
				}
			}
		}
		$hasil = array(
			'id' => 5,
			'error' => 204,
		);
		echo json_encode($hasil);
	}
	
	public function get()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getPlaylistByName")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$name = "";
					if(isset($p['name']))
						$name = $p['name'];
					//$ex = explode("_", $cookie);	
					//$arr = array(
					//	"cookie" => trim($ex[1])
					//);
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					//echo $res;
					//$r = json_decode(json_encode($res), true);
					$r = json_decode($res, true);
					if(is_array($r))
					//$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					//$ddd = json_decode($hhh, true);
					//if(isset($ddd["result"]))
					{
						if($r['error'] == null)
						//if(count($ddd["result"]) > 0)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
							//echo 'isi cek ADALAH '.$cek;
	                        $ada = false;
	                        if($cek == "OK")
	                        {
								if(strlen(trim($name)) > 0)
								{
									$db = Db::init();
									$tun = $db->playlists;
									$arrg = array(
										"name" => $name
									);
									$dp = $tun->findone($arrg);
									$arr = array();
									if(isset($dp['_id']))
									{
										$p = array(
											'id' => trim($dp['_id']),
											'name' => $dp['name'],
											'description' => $dp['description']
										);
										$arr[] = $p;
									}

									$pp = array(
										'id' => 10,
										'result' => $arr,
										'error' => null
									);
									echo json_encode($pp);
									return;
									/*$hhhg = Http::kirimJson(DOMAIN."playlist/search", json_encode($arrg));
									$dddg = json_decode($hhhg, true);
									if(is_array($dddg))
									{
										echo json_encode($dddg);
									}*/
								}
							}
						}
					}
				}                
			}
		}

		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}

	public function insert()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "insertPlaylist")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$name = "";
					if(isset($p['name']))
						$name = $p['name'];
					$description = "";
					if(isset($p['description']))
						$description = $p['description'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if((strlen(trim($name)) > 0) && (strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        			{
        				$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$arrg = array(
										"name" => trim($name),
										"description" => trim($description),
										"userid" => trim($userid)
									);
									$db = Db::init();
									$pl = $db->playlists;
									$hs = $pl->findone(array('name' => trim($name), "userid" => trim($userid)));
									if(!isset($hs['_id']))
									{
										$pl->insert($arrg);
										$arr = array(
											"id" => trim($arrg['_id'])
										);
									}
									else
									{
										$arr = array(
											"id" => trim($hs['_id'])
										);
									}
									$hasil = array(
										"error" => null,
										"result" => array($arr)
									);
									echo json_encode($hasil);
			                    }
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if(strlen(trim($name)) > 0)
							{
							    $ada = true;
								$userid = 0;
								foreach($ddd["result"] as $res)
								{
									$userid = $res["userid"];
								}

								$arrg = array(
									"name" => trim($name),
									"description" => trim($description),
									"userid" => intval($userid)
								);
								$hhhg = Http::kirimJson(DOMAIN."playlist/insert", json_encode($arrg));
								echo $hhhg;
							}
						}
					}*/
				}
			}
		}
	}

	public function getdetail()
	{
		
	}
	
	public function getbyuser()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getAllPlaylistByUser")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$name = "";
					if(isset($p['name']))
						$name = $p['name'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if((strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        			{
        				$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$db = Db::init();
									$play = $db->playlists;
									
									$quer = array(
										'userid' => trim($userid)
									);
									$mplay = $play->find($quer);
									
									$hasil = array(
										'error' => null,
										'result' => array()
									);
									if($mplay->count() > 0)
									{
										$data = array();
										foreach($mplay as $dt)
										{
											$h = array(
												'id' => trim($dt['_id']),
												'userid' => trim($dt['userid']),
												'name' => trim($dt['name']),
												'description' => trim($dt['description'])
											);
											$data[] = $h;
										}
										$hasil = array(
											'error' => null,
											'result' => $data
										);
									}
									echo json_encode($hasil);
			                    }
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							foreach($ddd["result"] as $res)
							{
								$userid = $res["userid"];
								$arrg = array(
									"userid" => intval($userid)
								);
								$hhhg = Http::kirimJson(DOMAIN."playlist/search", json_encode($arrg));
								$dddg = json_decode($hhhg, true);
                                $ada = false;
								if(is_array($dddg))
								{
								    $ada = true;
									echo json_encode($dddg);
								}
								if(!$ada)
                                {
                                    echo "Not Found";
                                }
							}
						}
					}*/
				}
			}
		}
	}
	
	public function getbyfeatured()
	{
		if(!empty($_POST))
		{
			$appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$cookie = "";
			if(isset($_POST['cookie']))
				$cookie = $_POST['cookie'];
			$userid = "";
			if(isset($_POST['userid']))
				$userid = $_POST['userid'];
			
			$validator = new Validator();
	        $validator->addRule('userid', array('require'));
	        $validator->addRule('cookie', array('require'));
	        $validator->addRule('appid', array('require'));
	        $validator->setData(array(
	        	'userid' => $userid,
	            'cookie' => $cookie,
	            'appid' => $appid,
	        ));		
			
			if($validator->isValid())
			{
				$cekcookie = helper::cekCookie($userid, $cookie);
				if($cekcookie)
				{
					$db = Db::init();
					$tun = $db->playlists;
					$arrg = array(
						"featured" => 'yes'
					);
					
					$countdp = $tun->count($arrg);
					if($countdp > 0)
					{
						$dp = $tun->find($arrg);
						$data = array();
						foreach($dp as $dt)
						{
							$image ="";
							if(isset($dt['image']))
							{
								$image = $dt['image'];
							}
	
							$h = array(
								'id' => trim($dt['_id']),
								'name' => trim($dt['name']),
								'imageurl' => CDN_IMAGE.'/image/',
	                    		'imagename' => $image,
								'description' => trim($dt['description'])
							);
							$data[] = $h;
						}
						$hasil = array(
							'error' => null,
							'result' => "SUCCESS",
							'data' => $data
						);
						echo json_encode($hasil);
						return;
					}
					
				}
			}
		}
		
		$hasil = array(
			'error' => 204,
			'result' => "FAILED",
			'data' => array()
		);
	echo json_encode($hasil);
	return;

	}
	
	
	public function getbyfeatured2()
	{
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getPlaylistByFeatured")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
						'userid' => $userid,
						'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))
					{
						if($r['error'] == null)
						//if(count($ddd["result"]) > 0)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
							//echo 'isi cek ADALAH '.$cek;
	                        $ada = false;
	                        if($cek == "OK")
	                        {
								$db = Db::init();
									$tun = $db->playlists;
									$arrg = array(
										"featured" => 'yes'
									);
									$dp = $tun->find($arrg);
									
									$hasil = array(
										'error' => null,
										'result' => array()
									);
									if($dp->count() > 0)
									{
										$data = array();
										foreach($dp as $dt)
										{
											$image ="";
											if(isset($dt['image']))
											{
												$image = $dt['image'];
											}

											$h = array(
												'id' => trim($dt['_id']),
												'name' => trim($dt['name']),
												'imageurl' => CDN_IMAGE.'/image/',
		                                		'imagename' => $image,
												'description' => trim($dt['description'])
											);
											$data[] = $h;
										}
										$hasil = array(
											'error' => null,
											'result' => $data
										);
									}
									echo json_encode($hasil);
									return;
							}
						}
					}
				}                
			}
		}

		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}

    public function radio()
    {
        $body = @file_get_contents('php://input');
        $b = json_decode($body, true);
        if(is_array($b))
        {
            if(isset($b['method']))
            {
                if($b['method'] == "getAllPlaylistRadio")
                {
                    $par = $b['params'];
                    $p = $par[0];
                    $appid = "";
                    if(isset($p['appid']))
                        $appid = $p['appid'];
					$userid = "";
                    if(isset($p['userid']))
                        $userid = $p['userid'];
                    $cookie = "";
                    if(isset($p['cookie']))
                        $cookie = $p['cookie'];
					
                    $curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))
					{
						//if(count($ddd["result"]) > 0)
						if($r['error'] == null)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
							//echo 'isi cek ADALAH '.$cek;
	                        $ada = false;
	                        if($cek == "OK")
	                        {
	                        	$db = Db::init();
								$pl = $db->playlists;
								$gen = $db->genres;
								$muspl = $db->music_playlists;
								
	                        	$quer = array(
	                                "type" => "radio",
	                                "status" => "active"
	                            );
								$mpl = $pl->find($quer);
								
								if($mpl->count() > 0)
								{
									$bygenre = array();									
                            		$byactivity = array();
									foreach($mpl as $dat)
									{
										$c = array(
											'playlistid' => trim($dat['_id']),
											'playlistname' => trim($dat['name'])
										);
										$byactivity[] = $c;
									}
									
									if(count($byactivity) > 0)
									{
										$arrg = array();
										foreach($byactivity as $act)
										{
											$mmpl = $muspl->find(array('playlist_id' => new MongoId($act['playlistid'])));
											
											if($mmpl->count() > 0)
											{
												foreach($mmpl as $dmmpl)
												{
													if(count($arrg) > 0)
													{
														$no = 0;
                                                        $i = 0;
														foreach($arrg as $darr)
														{
															if(trim($darr['genreid']) == trim($dmmpl['genre_id']))
															{
																$da[$i]['genreid'] = trim($dmmpl['genre_id']);
                                                                $no++;
															}
															$i++;
														}
														if($no == 0)
                                                        {
                                                            $d = array(
                                                                "genreid" => trim($dmmpl['genre_id'])
                                                            );
                                                            $arrg[] = $d;
                                                        }
													}
													else {
														$d = array(
                                                            "genreid" => trim($dmmpl['genre_id'])
                                                        );
                                                        $arrg[] = $d;
													}
												}
											}
										}

										if(count($arrg) > 0)
										{
											foreach($arrg as $dgen)
											{
												$mgen = $gen->findOne(array('_id' => new MongoId($dgen['genreid'])));
												if($mgen)
												{
													if($mgen['status' == 'yes'])
													{
														$logo = '';
														if(isset($mgen['logo']))
														{
															if(strlen(trim($mgen['logo'])) > 0)
																$logo = trim($mgen['logo']); 
														}
															
														$dd = array(
                                                            "genreid" => trim($mgen['_id']),
                                                            "name" => trim(preg_replace("/#?[a-z0-9]+;/i", '', trim($mgen['name']))),
                                                            "imageurl" => CDN_IMAGE.'/image/',
                                                            'imagename' => $logo
                                                        );
                                                        $bygenre[] = $dd;
													}
												}
											}
										}										
									}
									$rpc = array(
                                        "activity" => $byactivity,
                                        "genre" => $bygenre
                                    );                                                                                
                                    
                                    echo json_encode($rpc);
								}
	                        }
						}
					}
                    /*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
                    $ddd = json_decode($hhh, true);
                    if(isset($ddd["result"]))
                    {
                        if(count($ddd["result"]) > 0)
                        {
                            $arr = array(
                                "type" => "radio",
                                "status" => "active"
                            );
                            
                            $rad = Http::kirimJson(DOMAIN."playlist/search", json_encode($arr));
                            $ddr = json_decode($rad, true);
                            
                            $bygenre = array();
                            $byactivity = array();
                            if(isset($ddr['result']))
                            {
                                if(count($ddr['result']) > 0)
                                {
                                    $id = array();
                                    $genre = array();
                                    foreach($ddr['result'] as $rs)
                                    {
                                        $c = array(
                                            "playlistid" => $rs['id'],
                                            "playlistname" => $rs['name'],
                                        );
                                        $id[] = $c;
                                        $byactivity[] = $c;
                                    }
                                                                        
                                    if(count($id) > 0)
                                    {                                        
                                        $arrg = array();
                                        foreach($id as $cg)
                                        {
                                            $arr2 = array(
                                                "playlistid" => intval(base_convert($cg['playlistid'], 16, 10))
                                            );
                                            
                                            $sp = Http::kirimJson(DOMAIN."song_playlist/search", json_encode($arr2));
                                            $jsp = json_decode($sp, true);
                                            
                                            if(isset($jsp['result']))
                                            {
                                                if(count($jsp['result']) > 0)
                                                {                                                    
                                                    foreach($jsp['result'] as $jj)
                                                    {
                                                        if($cg['playlistid'] == base_convert($jj['playlistid'], 10, 16))
                                                        {
                                                            if(count($arrg) > 0)
                                                            {
                                                                $no = 0;
                                                                $i = 0;
                                                                foreach($arrg as $da)
                                                                {
                                                                    if($da['genreid'] == base_convert($jj['genreid'], 10, 16))
                                                                    {
                                                                        $da[$i]['genreid'] = base_convert($jj['genreid'], 10, 16);
                                                                        $no++;
                                                                    }
                                                                    $i++;
                                                                }
                                                                if($no == 0)
                                                                {
                                                                    $d = array(
                                                                        "genreid" => base_convert($jj['genreid'], 10, 16)
                                                                    );
                                                                    $arrg[] = $d;
                                                                }
                                                            }
                                                            else {
                                                                $d = array(
                                                                    "genreid" => base_convert($jj['genreid'], 10, 16)
                                                                );
                                                                $arrg[] = $d;
                                                            }
                                                        }                                                                                                               
                                                    }
                                                }
                                            }
                                        }

                                        if(count($arrg) > 0)
                                        {
                                            foreach($arrg as $dr)
                                            {
                                                $hasil = Http::getStr2(DOMAIN."genre/search?id=".$dr['genreid']);
                                                $dh = json_decode($hasil, true);
                                                
                                                if(is_array($dh))
                                                {
                                                    if(count($dh) > 2)
                                                    {
                                                        if(strtolower($dh['status']) == "yes")
                                                        {
                                                            $imagelink = "";
                                                            if(isset($dh['imagelink']))
                                                            {
                                                                $imagelink .= DOMAIN_STREAM;
                                                                $imagelink .= "images.jpg?id=".$dh['imagelink'];
                                                                $imagelink .= "&appid=".$appid;
                                                                $imagelink .= "&cookie=".$cookie;
                                                            }
                                                            
                                                            $dd = array(
                                                                "genreid" => $dh['id'],
                                                                "name" => $dh['name'],
                                                                "imagelink" => $imagelink
                                                            );
                                                            $bygenre[] = $dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        $rpc = array(
                                            "activity" => $byactivity,
                                            "genre" => $bygenre
                                        );                                                                                
                                        
                                        echo json_encode($rpc);                                                                                
                                    }
                                }
                            }
                            else {
                                $rpc = array(
                                    "result" => "playlist not found"
                                );
                                
                                echo json_encode($rpc);
                            }
                        }
                    }
                    else {
                        $rpc = array(
                            "result" => "session not found"
                        );
                        
                        echo json_encode($rpc);
                    }*/
                }
            }
        }
    }
}