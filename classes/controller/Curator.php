<?php

class Curator extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        
    }
    
    public function getwaitingtrack()
    {
        $appid = Base::instance()->get('GET.appid');
        $cookie = Base::instance()->get('GET.cookie');
		$userid = Base::instance()->get('GET.userid');
        
        $db = Db::init();
        $mus = $db->musics;
        
        if((strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        {
        	$curl = new Curl();
			$curl->get('https://sso.dboxid.com/login/cekcookie', array(
					'userid' => $userid,
					'cookie' => $cookie,
			));
			$res = $curl->response;
			$r = json_decode($res, true);
			if(is_array($r))
            {
				if($r['error'] == null)
                {
                	$cek = '';
	                foreach($r['result'] as $rt)
					{
						$cek = $rt['cek'];
					}
	                if($cek == "OK")
                    {
                        $mmus = $mus->find(array("approve_status" => "waiting"));
						$arrdata = array();
						
						foreach ($mmus as $key) 
						{
							
							$arralbum = array();
							$arrartist = array();
							$arrtrack = array();
							if(isset($key['_id']))
							{
								$albrts = $db->album_artists;
								$art = $db->artists;
								$artm = $db->music_artists;
								$albgen = $db->album_genres;
								$albtipe = $db->album_tipes;
								$albcat = $db->album_categories;
								$musgen = $db->music_genres;
								$gen = $db->genres;
								$tags = $db->album_tags;
								$tagm = $db->music_tags;
								$alb = $db->albums;
								$cont = $db->contributors;
								$plcountry = $db->place_countries;
								$pltown = $db->place_towns;
								
								$malb = $alb->findone( array("_id" => new MongoId($key['album_id'])) );
								$albumid = trim($malb['_id']);
								
								$marts = $albrts->find(array('album_id' => new MongoId($albumid)));
								$artist = '';
								foreach ($marts as $k) {				
									$mart = $art->findOne(array('_id' => new MongoId($k['artist_id'])));
									$artist .= $mart['name'] . ', ';	
								}
								
								$malbgen = $albgen->find(array('album_id' => new MongoId($albumid)));
								$genre = '';
								$subgenre = ''; 
								foreach ($malbgen as $k) {				
									if($k['status'] == 'main_genre'){
										$mgen =  $gen->findOne(array('_id' => new MongoId($k['genre_id'])));
										$genre = $mgen['name']; 					
									}
									if($k['status'] == 'sub_genre'){
										$dgen = $gen->findOne(array('_id' => new MongoId($k['genre_id'])));
										$subgenre .= $dgen['name'] . ', ';					
									}	
								}
								
								$mtag = $tags->find(array('album_id' => new MongoId($albumid)));
								$tag = ''; 
								foreach ($mtag as $k) {				
									$tag .= $k['tag'] . ', ';	
								}
								$mcont = $cont->findOne(array('_id' => new MongoId( trim($malb['contributor_id']) )));
								$malbtipe = $albtipe->findOne(array('_id' => new MongoId( trim($malb['album_tipe_id']) )));
								$malbcat = $albcat->findOne(array('_id' => new MongoId( trim($malb['album_category_id']) )));
								$arr = array(
									"albumid" => $albumid,
									"artist" => $artist,
									"genre" => $genre,
									"subgenre" => $subgenre,
									"tag" => $tag,
									"title" => $malb['title'],
									"description" => $malb['description'],
									"contributorid" => trim( $mcont['_id'] ),
									"contributor" => $mcont['name'],
									"recorded" => $malb['recorded'],
									"released" => date('d-m-Y', $malb['released']),
									"albumtipe" => $malbtipe['name'],
									"albumcategory" => $malbcat['name'],
									"coverfront" => $malb['cover_front'],
									"coverback" => $malb['cover_back'],
									"publishstatus" => $malb['status_publish'],
									"regional" => $malb['regional'],
									"document" => $malb['document'],
								);
								$arralbum[] = $arr;
								
								
								$martm = $artm->find(array('music_id' => new MongoId(trim($key['_id']))));
								$mainartist = '';
								$additionalartist = ''; 
								foreach ($martm as $k) {				
									if($k['status'] == 'main'){
										$mart = $art->findOne(array('_id' => new MongoId($k['artist_id'])));
										$mainartist = $mart['name'];
										$artisid = trim($mart['_id']);
									} 					
									else
									{
										$mart = $art->findOne(array('_id' => new MongoId($k['artist_id'])));
										$additionalartist .= $mart['name'].', ';					
									}	
								}
								
								$personel_name = '';
								foreach ($mart['personel_name'] as $k) {
									$personel_name .= $k['name'] . ', ';
								}
								$personel_description = '';
								foreach ($mart['personel_description'] as $k) {
									$personel_description .= $k['description'] . ', ';
								}
								
								$foto_default = 0;
								foreach ($mart['foto_default'] as $k) {
									$foto_default = $k['no'];
									break;
								}
								
								$genre = '';
								if(isset($mart['genre_id']))
								{
									foreach($mart['genre_id'] as $k)
									{
										$mgen = $gen->findOne(array("_id" => new MongoId($k['genre_id'])));
										$genre .= $mgen['name'] . ', ';
									}
								}
								
								$date = time();
								if(isset($mart['date_of_birth']))
									$date = $mart['date_of_birth'];
								$mplcountry = $plcountry->findOne(array('_id' => new MongoId( trim($mart['nationality']) )));
								$mpltown = $pltown->findOne(array('_id' => new MongoId( trim($mart['from']) )));
								$arr = array(
									"artisid" => trim($mart['_id']),
									"dateofbirth" => date('d-m-Y', $date),
									"name" => $mart['name'],
									"birthplace" => $mart['birthplace'],
									"genre" => $genre,
									"nationality" => $mplcountry['name'],
									"religion" => $mart['religion'],
									"type" => $mart['type'],
									"died" => $mart['died'],
									"from" => $mpltown['name'],
									"blog" => $mart['blog'],
									"genre" => $genre,
									"website" => $mart['website'],
									"description" => $mart['description'],
									"background" => $mart['background'],
									"facebook_id" => $mart['facebook_id'],
									"google_id" => $mart['google_id'],
									"twitter_id" => $mart['twitter_id'],
									"personel_name" => $personel_name,
									"personel_description" => $mart['personel_description'],
									"foto" => $mart['foto'][$foto_default]['foto'],
									"foto_title" => $mart['foto_title'][$foto_default]['title'],
									"foto_description" => $mart['foto_description'][$foto_default]['deskripsi'],
									"foto_default" => $foto_default,
								);
								$arrartist[] = $arr;
								
								$gens = $musgen->find(array('music_id' => new MongoId($key['_id'])));
								$genre = '';
								$genreid = '';
								$subgenre = ''; 
								foreach ($gens as $k) {				
									if($k['status'] == 'main_genre')
									{
										$mgen = $gen->findOne(array('_id' => new MongoId($k['genre_id'])));
										$genre = $mgen['name'];
										$genreid = trim($k['genre_id']);
									} 					
									if($k['status'] == 'sub_genre'){
										$mgen = $gen->findOne(array('_id' => new MongoId($k['genre_id'])));
										if (isset($k['name']))
											$subgenre .= $k['name'] . ', ';					
									}	
								}

								$mtag = $tagm->find(array('music_id' => new MongoId($key['_id'])));
								$tag = ''; 
								foreach ($mtag as $k) {				
									$tag .= $k['tag'] . ', ';	
								}

								$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$key['key']."/index.m3u8";
                            	$streamV0 .= "?userid=".$userid."&songid=".$key['key']."&cookie=".$cookie."&appid=".$appid;
								
								$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$key['key']."/index.m3u8";
                            	$streamV2 .= "?userid=".$userid."&songid=".$key['key']."&cookie=".$cookie."&appid=".$appid;
								
								$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$key['key']."/index.m3u8";
                            	$streamV4 .= "?userid=".$userid."&songid=".$key['key']."&cookie=".$cookie."&appid=".$appid;

					    		$zipnew  = "http://track.digibeat.co.id/get.zip";
                            	$zipnew .= "?userid=".$userid."&songid=".$key['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
								
								$collection = array(
									"artisid" => trim($mart['_id']),
									"artist" => $mainartist,
									"additionalartist" => $additionalartist,
									"genreid" => $genreid,
									"genre" => $genre,
									"subgenre" => $subgenre,
									"tag" => $tag,
									"albumid" => trim($key['album_id']),
                                    "albumname" => $malb['title'],
									"collectionid" => trim($key['_id']),
									"title" => $key['title'],
                                    "seo" => $key['seo'],
                                    "track" => $key['track'],
                                    "year" => $key['year'],
                                    "writer" => $key['writer'],
                                    "lyrics" => $key['lyrics'],
                                    "composer" => $key['composer'],
                                    "regional" => $key['regional'],
                                    "description" => $key['description'],
                                    "length" => intval($key['length']),
                                    "imageurl" => CDN_IMAGE.'/image/',
	                                "imagename" => $malb["cover_front"],
                                    "streamv0" => $streamV0,
                                    "streamv2" => $streamV2,
                                    "streamv4" => $streamV4,
                                    "zip_file" => $zipnew,
                                );
                                $arrtrack[] = $collection;
								
								$arr = array(
									'album' => $arralbum,
									'artist' => $arrartist,
									'track' => $arrtrack,
								);
								$arrdata[] = $arr;
							}
						}
						$pp = array(
							'id' => 10,
							'result' => $arrdata,
							'error' => null
						);
						echo json_encode($pp);
						return;
						//end foreach
                    }
                }
            }
        }

		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
    }

    public function validateemailcurator()
	{
		$body = @file_get_contents('php://input');
		//if(empty($body) && !empty($params)) $body=$params;
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "validateEmailCurator")
				{
					$par = $b['params'];
					$p = $par[0];
					$email = "";
					if(isset($p['email']))
						$email = $p['email'];
					if(!empty($email)){
						$db = Db::init();
						$dbm = $db->user_contributor;
						$dt = array("email"=>$email);
						$data = $dbm->findOne($dt);
						$p = $data;
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => 'SUCCESS',
							'error' => null
						);
							
					}else{
						$p = array(
							'ERROR' => 'User is Not Recognized'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => 'FAILED',
							'error' => 204
						);
					}
				}else{
					$p = array(
							'ERROR' => 'Wrong method!'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => $hasil,
							'error' => 204
						);
					}
			}else{
				$p = array(
							'ERROR' => 'Empty Method!'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => $hasil,
							'error' => 204
						);
			}
		}else{

			$p = array(
				'ERROR' => 'Undefined'
			);
			$hasil[] = $p;
			$pp = array(
				'id' => 10,
				'result' => $hasil,
				'error' => 204
			);
		}
		
		echo json_encode($pp);
	}

	public function getlistreason()
	{
		$body = @file_get_contents('php://input');
		//if(empty($body) && !empty($params)) $body=$params;
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getListReason")
				{
					$par = $b['params'];
					$p = $par[0];
					$email = "";
					if(isset($p['email']))
						$email = $p['email'];
					$db = Db::init();
					$dbm = $db->user_contributor;
					$dt = array("email"=>$email);
					$dataEmail = $dbm->findOne($dt);
					if(!empty($dataEmail)){
						
						
						$dbm = $db->reason_for_reject;
						$data = $dbm->find();
						$returnData = array();
						foreach ($data as $key=>$k) 
						{				
							$returnData[]=array('id'=>$k['_id']->{'$id'},'title'=>$k['title'],'description'=>$k['description']);
							
						}
					
						$hasil = $returnData;
						$pp = array(
							'id' => 10,
							'result' => $hasil,
							'error' => null
						);
							
					}else{
						$p = array(
							'ERROR' => 'User is Not Recognized'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => 'FAILED',
							'error' => 204
						);
					}

				}else{
					$p = array(
							'ERROR' => 'Wrong method!'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => $hasil,
							'error' => 204
						);
					}
			}else{
				$p = array(
							'ERROR' => 'Empty Method!'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => $hasil,
							'error' => 204
						);
			}
		}else{

			$p = array(
				'ERROR' => 'Undefined'
			);
			$hasil[] = $p;
			$pp = array(
				'id' => 10,
				'result' => $hasil,
				'error' => 204
			);
		}
		
		echo json_encode($pp);
	}

	function ambilHexTgl()
        {
            $a = explode(" ", microtime());
            $b = explode(".", $a[0]);
            $c = substr($b[1], 0, 6);
            $d = $a[1].$c;
            return base_convert($d, 10, 16);
        }

	public function kurasi()
	{		
               
		$body = @file_get_contents('php://input');
		//if(empty($body) && !empty($params)) $body=$params;
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "setStatus")
				{
					/*
						parameters:
						email:email kurator
						id : id track
						statusApproval : approved/rejected
						denied =0(user may edit track detail), 1 (user denied to access track)
						reason : array reason -> array('id1','id2','id3');
						newReason : string new reason description (ex. format tidak valid, karena...)
						newReasonTitle : string title reason (ex. salah judul)
						saveReason : 0 (not to saved), 1(save as ne reason)

					*/
					$par = $b['params'];
					$p = $par[0];
					$email = "";
					if(isset($p['email']))
						$email = $p['email'];
					$id = "";
					if(isset($p['id']))
						$id = $p['id'];
					$statusApproval = "";
					if(isset($p['statusApproval']))
						$statusApproval = $p['statusApproval'];
					$denied = 0;
					if(isset($p['denied']))
						$denied = $p['denied'];
					$reason = "";
					if(isset($p['reason']))
						$reason = $p['reason'];
					$newReason = "";
					if(isset($p['newReason']))
						$newReason = $p['newReason'];
					$saveReason = 0;
					if(isset($p['saveReason']))
						$saveReason = $p['saveReason'];

					$newReasonTitle = "";
					if(isset($p['newReasonTitle']))
						$newReasonTitle = $p['newReasonTitle'];
					$db = Db::init();
					
					
					if(!empty($email)){
						
						if(!empty($id)&&!empty($statusApproval)){
							$dbm = $db->musics;
							$dmus = $dbm->findOne(array('_id' => new MongoId($id)));
							if(empty($dmus['key'])){
							    $idRec = $this->ambilHexTgl();
							}  else {
							    $idRec = $dmus['key'];
							}
							$ddt = array(
								"filename" => trim($dmus['key']),
								"key"=> trim($idRec),
								"title" => trim($dmus['title']),
								"track" => intval($dmus['track']),
								"year" => trim($dmus['year']),
								"length" => intval($dmus['length']),
								"samplerate" => intval($dmus['samplerate']),
								"bitrate" => intval($dmus['bitrate']),
								"channels" => intval($dmus['channels']),
								"writer" => trim($dmus['writer']),
								"composer" => trim($dmus['composer']),
								"status_publish" => trim($dmus['status_publish']),
								"streaming_status" => trim($dmus['streaming_status']),
								"regional" => trim($dmus['regional']),
								"price_download" => intval($dmus['price_download']),
								"diskon_price_download" => floatval($dmus['diskon_price_download']),
								"description" => trim($dmus['description']),
								"lyrics" => trim($dmus['lyrics']),
								"contributor_id" => new MongoId($dmus['contributor_id']),
								"artist_id" => new MongoId($dmus['artist_id']),
								"album_id" => new MongoId($dmus['album_id']),
								"genre_id" => new MongoId($dmus['genre_id']),
								"seo" => trim($dmus['seo']),
								"approve_status" => $statusApproval,
								"created_by" => new MongoId($dmus['created_by']),
								"time_created" => $dmus['time_created'],
								"time_edited" => time(),
							);
							
							$newdata = array('$set' => $ddt);
							$dbm->update(array('_id' => new MongoId($id)), $newdata);
							if($statusApproval=='rejected'){
								if(!empty($newReason)){
								$db = Db::init();
								$dbr = $db->reason_for_reject;
								$reasonOld = array();
								foreach($reason as $key=>$RO){
									$dtR = array("_id"=> new MongoId($RO));
									$dataReasonOld = $dbr->findOne($dtR);
									$reasonOld[$key]=array('title'=>$dataReasonOld['title'],
											    'description'=>$dataReasonOld['description']);
								}
								
								 $allReason = array_merge($reasonOld,array($newReason)); 
									if($saveReason==1){
										$dbm = $db->reason_for_reject;
										$dt = array('title'=>$newReasonTitle,
											    'description'=>$newReason,
											    'time_created'=>time());
										$data = $dbm->insert($dt);
									}
								}else $allReason = $reason;
								$dbm = $db->music_rejected;
								$newdata = array('music_id' => $id,
										 'reason'=>$allReason,
										 'denied'=>@$denied);
								$dbm->insert($newdata);
							}

							$pp = array(
									'id' => 10,
									'result' => "SUCCESS",
									'error' => null
								);
						    }else{
							$pp = array(
								'id' => 10,
								'result' => "parameters not completed",
								'error' => 100
							);
						}
							
					}else{
						$p = array(
							'ERROR' => 'User is Not Recognized'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => 'FAILED',
							'error' => 204
						);
					}
					

				}else{
					$p = array(
							'ERROR' => 'Wrong method!'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => $hasil,
							'error' => 204
						);
					}
			}else{
				$p = array(
							'ERROR' => 'Empty Method!'
						);
						$hasil[] = $p;
						$pp = array(
							'id' => 10,
							'result' => $hasil,
							'error' => 204
						);
			}
		}else{

			$p = array(
				'ERROR' => 'Undefined'
			);
			$hasil[] = $p;
			$pp = array(
				'id' => 10,
				'result' => $hasil,
				'error' => 204
			);
		}
		
		echo json_encode($pp);
		
		
		
			
		
                
	}


	

}
