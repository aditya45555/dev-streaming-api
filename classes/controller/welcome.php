<?php

class welcome_controller extends  controller
{
	
	public function index()
	{
		echo "index";
	}
	
	public function postfacebook()
	{
		$appid = Base::instance()->get("GET.appid");
		$cookie = Base::instance()->get("GET.cookie");
		$message = Base::instance()->get("POST.message");
		$link = Base::instance()->get("POST.link");
		$picture = Base::instance()->get("POST.picture");
		
		$hybridauth = new Hybrid_Auth( $config );
 
		// try to authenticate with twitter
		$adapter = $hybridauth->authenticate( "Facebook" );
		 
		 $ex = explode("_", $cookie);	
		$arr = array(
			"cookie" => trim($ex[1])
		);
		$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
		$ddd = json_decode($hhh, true);
		if(isset($ddd["result"]))
		{
			if(count($ddd["result"]) > 0)
			{
				// update the user status
				$adapter->setUserStatus(
				array(
						"message" => $message, // status or message content
						"link" => $link, // webpage link
						"picture" => $picture, // a picture link
					)
				); 	
			}
		}
	}
	
	public function posttwitter()
	{
		$appid = Base::instance()->get("GET.appid");
		$cookie = Base::instance()->get("GET.cookie");
		$message = Base::instance()->get("POST.message");
		
		$ex = explode("_", $cookie);	
		$arr = array(
			"cookie" => trim($ex[1])
		);
		$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
		$ddd = json_decode($hhh, true);
		if(isset($ddd["result"]))
		{
			if(count($ddd["result"]) > 0)
			{
				$hybridauth = new Hybrid_Auth( $config );
				$adapter = $hybridauth->authenticate( "Twitter" );
				$adapter->setUserStatus( $message ); 		
			}
		} 
	}
	
	private function google($appid, $redirect)
	{
		$config = DOCROOT . 'classes/hybridauth/config.php';
		$red = "";
		try
		{
			$googleid = "";
			$hybridauth = new Hybrid_Auth( $config );
			
			$google = $hybridauth->authenticate( "Google" );
			//$is_user_logged_in = $google->isUserConnected();
			
			//if($is_user_logged_in)
			//{
				$user_profile = $google->getUserProfile();
				$user_profile=  (array) $user_profile;
				$googleid = $user_profile['identifier'];
				//cari twitter_id di databse twitter
				// jika ada ambil id = user id
				//jika belum ada create twitter dan user
				$arr = array(
					"sid" => $googleid
				);
				$hhh = Http::kirimJson(DOMAIN."google/search", json_encode($arr));
				//echo "facebook/search : ". $hhh ."<br />";
				//echo "for : ". json_encode($arr) ."<br />";
				$ss = json_decode($hhh, true);
                
				$userid = "";
				$adajawaban = false;
				if($hhh !== "Not found")
				{
					if(isset($ss['result']))
					{
						if(count($ss['result']) > 0) // berarti ada data nya
						{						    
						    foreach($ss['result'] as $ddt)
                            {
                                $adajawaban = true;
                                $userid = $ddt['id'];
                            }													
							// kemudian timpa datanya
							$user_profile["sid"] = $googleid;
							$kkk = Http::kirimJson(DOMAIN."google/insert?id=".$userid, json_encode($user_profile));
						}	
					}
				}
				if(!$adajawaban)
				{
				    $cemail = Http::getStr(DOMAIN."email/search?id=".$user_profile["email"]."&type=string");
                    $emldec = json_decode($cemail, true);
                    if(strtolower($emldec['search']) == "not found")
                    {
                        Http::kirimJson(DOMAIN."email/insert?id=".$user_profile["email"], $userid);
                    }
					$user_profile["sid"] = $googleid;
					$userid = $this->ambilHexTgl();
					$kkk = Http::kirimJson(DOMAIN."google/insert?id=".$userid, json_encode($user_profile));
							
					$arru = array(
								"email"=> $user_profile["email"], 
								"time_created" => intval($this->ambilTgl()), 
								"gender" => $user_profile["gender"],
							 	"birthdate" => $user_profile["birthDay"]."-".$user_profile['birthMonth']."-".$user_profile["birthYear"], 
							 	"password" => "", 
							 	"name" => $user_profile["firstName"]." ".$user_profile["lastName"], 
							 	"city" => $user_profile["city"],  
							 	"country" => $user_profile["country"],
							  	"status_member" => "new", 
							  	"telp" => $user_profile["phone"],
					); 
					$kkk = Http::kirimJson(DOMAIN."user/insert?id=".$userid, json_encode($arru));
				}		
							//base_convert($hexadecimal, 16, 2);
				$sesid = $this->ambilHexTgl();
				$cookie = $this->createCookie();
				$sess = array(
							"userid" => intval(base_convert($userid, 16, 10)), 
							"cookie" => $cookie, 
							"time_created" => intval($this->ambilTgl()), 
							"appid" => intval(base_convert($appid, 16, 10))
				);
				$kkk = Http::kirimJson(DOMAIN."session/insert?id=".$sesid, json_encode($sess));
				//echo $kkk."<br />";
				$red = $redirect."?id=".$userid."&type=google&cookie=".$sesid."_".$cookie;
			//}
		}
		catch( Exception $e )
		{  

			switch( $e->getCode() ){ 
				case 0 : echo "Unspecified error."; break;
				case 1 : echo "Hybridauth configuration error."; break;
				case 2 : echo "Provider not properly configured."; break;
				case 3 : echo "Unknown or disabled provider."; break;
				case 4 : echo "Missing provider application credentials."; break;
				case 5 : echo "Authentication failed. " 
						  . "The user has canceled the authentication or the provider refused the connection."; 
					   break;
				case 6 : echo "User profile request failed. Most likely the user is not connected "
						  . "to the provider and he should to authenticate again."; 
					   $google->logout();
					   break;
				case 7 : echo "User not connected to the provider."; 
					   $google->logout();
					   break;
				case 8 : echo "Provider does not support this feature."; break;
			} 
		}
		
		if(strlen(trim($red)) > 0)
		{
			//if (!headers_sent()) {
			//	  foreach (headers_list() as $header)
			//	    header_remove($header);
			//	}
				//var_dump(headers_list());
				//die;
			header_remove(); 
			header($_SERVER["SERVER_PROTOCOL"]." 301 Moved Permanently"); 
			header('Location: '.$red);

			die;
		}
	}
	
	private function facebook($appid, $redirect)
	{
		$config = DOCROOT . 'classes/hybridauth/config.php';
		$red = "";
		try
		{
			$hybridauth = new Hybrid_Auth( $config );
			$facebook = $hybridauth->authenticate( "Facebook" );
			$is_user_logged_in = $facebook->isUserConnected();
			$facebookid = "";
			if($is_user_logged_in)
			{
				$user_profile = $facebook->getUserProfile();
				$user_profile=  (array) $user_profile;
				$facebookid = $user_profile['identifier'];
				//cari twitter_id di databse twitter
				// jika ada ambil id = user id
				//jika belum ada create twitter dan user
				//echo $facebook->id."<br />";
				//print_r($user_profile);
				$arr = array(
					"sid" => $facebookid
				);
				$hhh = Http::kirimJson(DOMAIN."facebook/search", json_encode($arr));
				//echo "facebook/search : ". $hhh ."<br />";
				//echo "for : ". json_encode($arr) ."<br />";
				$ss = json_decode($hhh, true);
				$userid = "";
				$adajawaban = false;
				if($hhh !== "Not found")
				{
					if(isset($ss['result']))
					{
						if(count($ss['result']) > 0) // berarti ada data nya
						{
							foreach($ss['result'] as $ddt)
                            {
                                $adajawaban = true;
                                $userid = $ddt['id'];
                            }
							// kemudian timpa datanya
							$user_profile["sid"] = $facebookid;
							$kkk = Http::kirimJson(DOMAIN."facebook/insert?id=".$userid, json_encode($user_profile));
						}	
					}
				}
				if(!$adajawaban)
				{
				    $cemail = Http::getStr(DOMAIN."email/search?id=".$user_profile["email"]."&type=string");
                    $emldec = json_decode($cemail, true);
                    if(strtolower($emldec['search']) == "not found")
                    {
                        Http::kirimJson(DOMAIN."email/insert?id=".$user_profile["email"], $userid);
                    }
					$user_profile["sid"] = $facebookid;
					$userid = $this->ambilHexTgl();
					$kkk = Http::kirimJson(DOMAIN."facebook/insert?id=".$userid, json_encode($user_profile));
							
					$arru = array(
								"email"=> $user_profile["email"], 
								"time_created" => intval($this->ambilTgl()), 
								"gender" => $user_profile["gender"],
							 	"birthdate" => $user_profile["birthDay"]."-".$user_profile['birthMonth']."-".$user_profile["birthYear"], 
							 	"password" => "", 
							 	"name" => $user_profile["firstName"]." ".$user_profile["lastName"], 
							 	"city" => $user_profile["city"],  
							 	"country" => $user_profile["country"],
							  	"status_member" => "new", 
							  	"telp" => $user_profile["phone"],
					); 
					$kkk = Http::kirimJson(DOMAIN."user/insert?id=".$userid, json_encode($arru));
				}		
							//base_convert($hexadecimal, 16, 2);
				$sesid = $this->ambilHexTgl();
				$cookie = $this->createCookie();
				$sess = array(
							"userid" => intval(base_convert($userid, 16, 10)), 
							"cookie" => $cookie, 
							"time_created" => intval($this->ambilTgl()), 
							"appid" => intval(base_convert($appid, 16, 10))
				);
				$kkk = Http::kirimJson(DOMAIN."session/insert?id=".$sesid, json_encode($sess));
				//echo $kkk."<br />";
				$red = $redirect."?id=".$userid."&type=facebook&cookie=".$sesid."_".$cookie;
				//echo $red."<br />";
				//die;
				//Base::instance()->reroute($red);
				
			}
		}
		catch( Exception $e )
		{  

			switch( $e->getCode() ){ 
				case 0 : echo "Unspecified error."; break;
				case 1 : echo "Hybridauth configuration error."; break;
				case 2 : echo "Provider not properly configured."; break;
				case 3 : echo "Unknown or disabled provider."; break;
				case 4 : echo "Missing provider application credentials."; break;
				case 5 : echo "Authentication failed. " 
						  . "The user has canceled the authentication or the provider refused the connection."; 
					   break;
				case 6 : echo "User profile request failed. Most likely the user is not connected "
						  . "to the provider and he should to authenticate again."; 
					   $facebook->logout();
					   break;
				case 7 : echo "User not connected to the provider."; 
					   $facebook->logout();
					   break;
				case 8 : echo "Provider does not support this feature."; break;
			} 
		}
		
		if(strlen(trim($red)) > 0)
		{
			//if (!headers_sent()) {
			//	  foreach (headers_list() as $header)
			//	    header_remove($header);
			//	}
				//var_dump(headers_list());
				//die;
			header_remove(); 
			header($_SERVER["SERVER_PROTOCOL"]." 301 Moved Permanently"); 
			header('Location: '.$red);

			die;
		}
	}
	
	private function twitter($appid, $redirect)
	{
		$config = DOCROOT . 'classes/hybridauth/config.php';
		$red = "";
		try
		{
			$hybridauth = new Hybrid_Auth( $config );
			$twitter = $hybridauth->authenticate( "Twitter" );
			$is_user_logged_in = $twitter->isUserConnected();
			$twitterid = "";
			if($is_user_logged_in)
			{
				$user_profile = $twitter->getUserProfile();
				$user_profile=  (array) $user_profile;
				$twitterid = $user_profile['identifier'];
				//cari twitter_id di databse twitter
				// jika ada ambil id = user id
				//jika belum ada create twitter dan user
				//echo $facebook->id."<br />";
				//print_r($user_profile);
				$arr = array(
					"sid" => $twitterid
				);
				$hhh = Http::kirimJson(DOMAIN."twitter/search", json_encode($arr));
				//echo "facebook/search : ". $hhh ."<br />";
				//echo "for : ". json_encode($arr) ."<br />";
				$ss = json_decode($hhh, true);
				$userid = "";
				$adajawaban = false;
				if($hhh !== "Not found")
				{
					if(isset($hhh['result']))
					{
						if(count($hhh['result']) > 0) // berarti ada data nya
						{
							$adajawaban = true;
							$userid = $hhh['id'];
							// kemudian timpa datanya
							$user_profile["sid"] = $twitterid;
							$kkk = Http::kirimJson(DOMAIN."twitter/insert?id=".$userid, json_encode($user_profile));
						}	
					}
				}
				if(!$adajawaban)
				{
					$user_profile["sid"] = $twitterid;
					$userid = $this->ambilHexTgl();
					$kkk = Http::kirimJson(DOMAIN."twitter/insert?id=".$userid, json_encode($user_profile));
							
					$arru = array(
								"email"=> $user_profile["email"], 
								"time_created" => intval($this->ambilTgl()), 
								"gender" => $user_profile["gender"],
							 	"birthdate" => $user_profile["birthDay"]."-".$user_profile['birthMonth']."-".$user_profile["birthYear"], 
							 	"password" => "", 
							 	"name" => $user_profile["firstName"]." ".$user_profile["lastName"], 
							 	"city" => $user_profile["city"],  
							 	"country" => $user_profile["country"],
							  	"status_member" => "new", 
							  	"telp" => $user_profile["phone"],
					); 
					$kkk = Http::kirimJson(DOMAIN."user/insert?id=".$userid, json_encode($arru));
				}		
							//base_convert($hexadecimal, 16, 2);
				$sesid = $this->ambilHexTgl();
				$cookie = $this->createCookie();
				$sess = array(
							"userid" => intval(base_convert($userid, 16, 10)), 
							"cookie" => $cookie, 
							"time_created" => intval($this->ambilTgl()), 
							"appid" => intval(base_convert($appid, 16, 10))
				);
				$kkk = Http::kirimJson(DOMAIN."session/insert?id=".$sesid, json_encode($sess));
				//echo $kkk."<br />";
				$red = $redirect."?id=".$userid."&type=twitter&cookie=".$sesid."_".$cookie;
			}
		}
		catch( Exception $e )
		{  

			switch( $e->getCode() ){ 
				case 0 : echo "Unspecified error."; break;
				case 1 : echo "Hybridauth configuration error."; break;
				case 2 : echo "Provider not properly configured."; break;
				case 3 : echo "Unknown or disabled provider."; break;
				case 4 : echo "Missing provider application credentials."; break;
				case 5 : echo "Authentication failed. " 
						  . "The user has canceled the authentication or the provider refused the connection."; 
					   break;
				case 6 : echo "User profile request failed. Most likely the user is not connected "
						  . "to the provider and he should to authenticate again."; 
					   $twitter->logout();
					   break;
				case 7 : echo "User not connected to the provider."; 
					   $twitter->logout();
					   break;
				case 8 : echo "Provider does not support this feature."; break;
			} 
		}
		
		if(strlen(trim($red)) > 0)
		{
			//if (!headers_sent()) {
			//	  foreach (headers_list() as $header)
			//	    header_remove($header);
			//	}
				//var_dump(headers_list());
				//die;
			header_remove(); 
			header($_SERVER["SERVER_PROTOCOL"]." 301 Moved Permanently"); 
			header('Location: '.$red);

			die;
		}
	}
	
	public function loginoauth()
	{
		Hybrid_Endpoint::process();		
	}
	
	public function logintwitter()
	{
		$appid = Base::instance()->get("GET.appid");
		$redirect = Base::instance()->get("GET.redirect");
		if((strlen(trim($appid)) > 0) && (strlen(trim($redirect)) > 0))
		{
			$this->twitter($appid, $redirect);
		}
	}
	
	public function logingoogle()
	{
		$appid = Base::instance()->get("GET.appid");
		$redirect = Base::instance()->get("GET.redirect");
		if((strlen(trim($appid)) > 0) && (strlen(trim($redirect)) > 0))
		{
			$this->google($appid, $redirect);
		}
	}
	
	public function loginfacebook()
	{
		$appid = Base::instance()->get("GET.appid");
		$redirect = Base::instance()->get("GET.redirect");
		if((strlen(trim($appid)) > 0) && (strlen(trim($redirect)) > 0))
		{
			$this->facebook($appid, $redirect);
		}
	}
	
	private function ambilTgl()
	{
		$a = explode(" ", microtime());
		$b = explode(".", $a[0]);
		$c = substr($b[1], 0, 6);
		$d = $a[1].$c;
		return $d;
	}
	
	private function ambilHexTgl()
	{
		$a = explode(" ", microtime());
		$b = explode(".", $a[0]);
		$c = substr($b[1], 0, 6);
		$d = $a[1].$c;
		return base_convert($d, 10, 16);
	}
	
	private function createCookie()
	{
		$crypt = new CkCrypt2() ;

	    $success = $crypt->UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
	    if ($success != true) {
	        printf("%s\n",$crypt->lastErrorText());
	        return "";
	    }

		$crypt->put_CryptAlgorithm("aes");
	    $crypt->put_CipherMode("cbc");
	    $crypt->put_KeyLength(256);
	    $crypt->put_PaddingScheme(0);
	    $crypt->put_EncodingMode("hex");

    	return $crypt->genRandomBytesENC(32);
	}
	
	public function testJson()
	{
		echo "test";
		/*$arr = array(
			"email" => "skripc@gmail.com"
		);
		$hhh = Http::kirimJson("http://192.168.88.40:4000/google/search", json_encode($arr));
		echo $hhh;*/
		//Base::instance()->reroute("http://www.dboxid.com/");
	}
}