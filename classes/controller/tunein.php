<?php

class tunein_controller extends controller
{
    
    public function getall()
    {
    	if(!empty($_POST))
    	{
		    $appid = "";
		    if(isset($_POST['appid']))
		        $appid = $_POST['appid'];
		    $cookie = "";
		    if(isset($_POST['cookie']))
		        $cookie = $_POST['cookie'];
			$userid = "";
		    if(isset($_POST['userid']))
		        $userid = $_POST['userid'];
		    $skip = 0;
		    if(isset($_POST['skip']))
		        $skip = $_POST['skip'];
		    $limit = 0;
		    if(isset($_POST['limit']))
		        $limit = $_POST['limit'];
		    
			$validator = new Validator();
	        $validator->addRule('userid', array('require'));
	        $validator->addRule('cookie', array('require'));
	        $validator->addRule('appid', array('require'));
	        $validator->setData(array(
	        	'userid' => $userid,
	            'cookie' => $cookie,
	            'appid' => $appid,
	        ));
			
			if($validator->isValid())
			{
				$cekcookie = helper::cekCookie($userid, $cookie);
				if($cekcookie)
				{
			    	$now = time(); 
			    	$db = Db::init();
					$ai = $db->artist_images;
					$tun = $db->tune_in;
					$tunin = $tun->find(
						array('status' => 'active')
					)->sort(array("time_created" => -1));
							
			        $arr = array();
			        foreach($tunin as $dtn)
			        {
			        	//http://cdn.digibeat.co.id/contributor
						$arrtuneindata = array();
			        	$imagealbum = '';
			            $id = $dtn['detailid'];
						
						if($dtn['type'] == 'track')
						{
							$trk = $db->musics;
							$trck = $trk->findone(
								array('_id' => new MongoId($dtn['detailid']))
							);
							if(isset($trck['_id']))
							{
								$alb = $db->albums;
								$album = $alb->findone(
									array('_id' => new MongoId($trck['album_id']))
								);
								if(isset($album['_id']))
								{
									if(strlen(trim($album["cover_front"])) > 0)
										$imagealbum = $album["cover_front"];
								}
								
								
								$title = $trck['title'];
			                    $seo = $trck['seo'];
			                    $description = $trck['description'];
			                    $track = $trck['track'];
			                    $length = $trck['length'];
			                    
								
								$name = "";                                
			                    $artisid = "";
			                    $genreid = "";
			                    $imagelink = "";
			                    $albumname = "";
								
								$alb = $db->albums;
								$aal = $alb->findone(
									array("_id" => new MongoId($trck['album_id']))
								);
								
								$art = $db->artists;
								$artis = $art->findone(
									array("_id" => new MongoId($trck['artist_id']))
								);
								
								$mai = $ai->find(array("artist_id" => new MongoId($trck['artist_id']), "foto_default" => "yes"));
						
								$fotoartist = '';
								foreach($mai as $dai) {
									$fotoartist = $dai['foto'];
									break;
								}
								
			                    if(isset($aal['_id']))
			                    {
			                        $albumname = $aal['title'];
									$albumid = trim($trck['album_id']);
									$genreid = trim($trck['genre_id']);
									
									$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$trck['key']."/index.m3u8";
			                    	$streamV0 .= "?userid=".$userid."&songid=".$trck['key']."&cookie=".$cookie."&appid=".$appid;
									
									$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$trck['key']."/index.m3u8";
			                    	$streamV2 .= "?userid=".$userid."&songid=".$trck['key']."&cookie=".$cookie."&appid=".$appid;
									
									$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$trck['key']."/index.m3u8";
			                    	$streamV4 .= "?userid=".$userid."&songid=".$trck['key']."&cookie=".$cookie."&appid=".$appid;
			
						    		$zipnew  = "http://track.digibeat.co.id/get.zip";
			                    	$zipnew .= "?userid=".$userid."&songid=".$trck['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
			                        
			                        $collection = array(
			                            "collectionid" => $id,
			                            "title" => $title,
			                            "seo" => $seo,
			                            "track" => $track,
			                            "length" => intval($length),
			                            "artistname" => trim($artis['name']),
			                            "artistid" => trim($artis['_id']),
			                            "genreid" => $genreid,
			                            "albumid" => $albumid,
			                            "albumname" => $albumname,
			                            "imageurl" => CDN_IMAGE.'/image/',
								        "imagename" => $aal["cover_front"],
			                            "streamv0" => $streamV0,
			                            "streamv2" => $streamV2,
			                            "streamv4" => $streamV4,
			                            "zip_file" => $zipnew,    
			                            "imageartist" => $fotoartist,                                
			                        );
			                        $arrtuneindata[] = $collection;
			                    }
							}
						}
						if($dtn['type'] == 'album')
						{
							if (strlen($dtn['detailid']) > 0){
								$alb = $db->albums;
								$albart = $db->album_artists;
								$albgen = $db->album_genres;
								$art = $db->artists;
								$album = $alb->findone(
									array('_id' => new MongoId($dtn['detailid']))
								);
								if(isset($album['_id']))
								{
									if(strlen(trim($album["cover_front"])) > 0)
										$imagealbum = $album["cover_front"];
									
									//nambah data
									$malbart = $albart->findOne(array('album_id' => new MongoId($album['_id'])));
									$malbgen = $albgen->findOne(array('album_id' => new MongoId($album['_id'])));
									
									$fotoartist = '';
									if(isset($malbart['_id']))
									{
										$dart = $art->findOne(array('_id' => new MongoId($malbart['artist_id'])));
										$mai = $ai->find(array("artist_id" => new MongoId($malbart['artist_id']), "foto_default" => "yes"));
						
										$fotoartist = '';
										foreach($mai as $dai) {
											$fotoartist = $dai['foto'];
											break;
										}
										/*$fotoartist = '';
										if(is_array($dart['foto']))
										{
											foreach($dart['foto'] as $dft)
											{
												$fotoartist = $dft['foto'];
												break;
											}
										}*/
									}
									$arrdata = array(
			                            "albumid" => trim($album['_id']),
			                            "title" => $album['title'],
			                            "seo" => $album['seo'],
			                            "artistname" => $dart['name'],
			                            "description" => $album['description'],
			                            "artistid" => trim($malbart['artist_id']),
			                            "genreid" => trim($malbgen['genre_id']),
			                            "imageurl" => CDN_IMAGE.'/image/',
								        "imagename" => $album['cover_front'],
			                            "imageartist" => $fotoartist,
			                        );
			                        $arrtuneindata[] = $arrdata;
									
								}
							}
							
						}
						if($dtn['type'] == 'playlist')
						{
								
						}
			           
						if(strlen(trim($dtn["image"])) > 0)
						{
			                $a = array(
			                    "imageurl" => CDN_IMAGE.'/image/',
		                        "imagename" => $dtn["image"],
			                    "id" => $id,
			                    "description" => $dtn['description'],
			                    "name" => $dtn['name'],
			                    "type" => $dtn['type'],
			                    "data" => $arrtuneindata
			                );
			                    
			                $arr[] = $a;
						}
			        }
			        
					$pp = array(
						'result' => "SUCCESS",
						'data' => $arr,
						'error' => null
					);
			        
			        echo json_encode($pp);
					return;
			         
			    }
			}
		}
	
		$pp = array(
			'result' => "FAILED",
			'data' => array(),
			'error' => 204
		);
		echo json_encode($pp);
	}
}
