<?php

class Application extends  Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
	}
	
	public function login()
	{
	}
	
	public function register()
	{
	}
	
	public function linkgoogle()
	{
		$appid = Base::instance()->get("GET.appid");
		$redirect = Base::instance()->get("GET.redirect");
		if((strlen(trim($appid)) > 0) && (strlen(trim($redirect)) > 0))
		{
			$h = Http::getStr(DOMAIN."appid/search?id=".$appid);
			//{"version": "0.1", "appname": "appIphone", "id": "4f3635f40386b", "time_created": 1393508032198763}
			$hh = json_decode($h, true);
			if(isset($hh['appname']))
			{
				$arr = array(
					"error" => null,
					//"result" => array(array("url" => "http://api.streaming.dboxid.com/loginoauth?hauth.start=Google&hauth.time=".time())),
					"result" => array(array("url" => "http://api.streaming.dboxid.com/logingoogle?appid=".$appid."&redirect=".$redirect)),
					"id" => 5
				);
				
				echo json_encode($arr);
			}
		}
	}
	
	public function linkfacebook()
	{
		$appid = Base::instance()->get("GET.appid");
		$redirect = Base::instance()->get("GET.redirect");
		if((strlen(trim($appid)) > 0) && (strlen(trim($redirect)) > 0))
		{
			$h = Http::getStr(DOMAIN."appid/search?id=".$appid);
			$hh = json_decode($h, true);
			if(isset($hh['appname']))
			{
				$arr = array(
					"error" => null,
					//"result" => array(array("url" => "http://api.streaming.dboxid.com/loginoauth?hauth.start=Facebook&hauth.time=".time())),
					"result" => array(array("url" => "http://api.streaming.dboxid.com/loginfacebook?appid=".$appid."&redirect=".$redirect)),
					"id" => 5
				);
				echo json_encode($arr);
			}
		}
	}
	
	public function linktwitter()
	{
		$appid = Base::instance()->get("GET.appid");
		$redirect = Base::instance()->get("GET.redirect");
		if((strlen(trim($appid)) > 0) && (strlen(trim($redirect)) > 0))
		{
			$h = Http::getStr(DOMAIN."appid/search?id=".$appid);
			$hh = json_decode($h, true);
			if(isset($hh['appname']))
			{
				$arr = array(
					"error" => null,
					//"result" => array(array("url" => "http://api.streaming.dboxid.com/loginoauth?hauth.start=Twitter&hauth.time=".time())),
					"result" => array(array("url" => "http://api.streaming.dboxid.com/logintwitter?appid=".$appid."&redirect=".$redirect)),
					"id" => 5
				);
				echo json_encode($arr);
			}
		}
	}
	
	public function hasillogin()
	{
		$id = Base::instance()->get("GET.id");
		$type = Base::instance()->get("GET.type");
		$appid = Base::instance()->get("GET.appid");
		$cookie = Base::instance()->get("GET.cookie");
		if((strlen(trim($id))> 0) && 
			(strlen(trim($type))> 0) &&
			(strlen(trim($appid))> 0)  && 
			(strlen(trim($cookie))> 0))
		{
			$ex = explode("_", $cookie);	
			$arr = array(
				"cookie" => trim($ex[1])
			);
			$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
			$ddd = json_decode($hhh, true);
			if(isset($ddd["result"]))
			{
				if(count($ddd["result"]) > 0)
				{
					$h = Http::getStr(DOMAIN.$type."/search?id=".$id);
					$h = utf8_encode ( $h );
					$ss = json_decode($h, true);
					if(is_array($ss))
					{
						$ss["id"] = $id;
						$json = json_encode($ss);
						//$error = json_last_error();
						//echo json_last_error_msg();
						echo $json;
						return;
					}
				}
			}
		}
		echo "Not found";
	}
}