 <?php

class songalbum_controller extends  controller
{
	
	
	public function getsongalbum()
	{
		if(!empty($_POST))
		{
			$appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$cookie = "";
			if(isset($_POST['cookie']))
				$cookie = $_POST['cookie'];
			$userid = "";
			if(isset($_POST['userid']))
				$userid = $_POST['userid'];
			$albumid = "";
			if(isset($_POST['albumid']))
				$albumid = $_POST['albumid'];
			
			$validator = new Validator();
	        $validator->addRule('userid', array('require'));
	        $validator->addRule('cookie', array('require'));
	        $validator->addRule('appid', array('require'));
			$validator->addRule('albumid', array('require'));
			
	        $validator->setData(array(
	        	'userid' => $userid,
	            'cookie' => $cookie,
	            'appid' => $appid,
	            'albumid' => $albumid
	        ));		
			
			if($validator->isValid())
			{
				$cekcookie = helper::cekCookie($userid, $cookie);
				if($cekcookie)
				{
					$db = Db::init();
					$mu = $db->musics;
					$mus = $mu->find(
						array('album_id' => new MongoId($albumid), "approve_status"=> "approved", "status_publish"=> "yes")
					)->sort(array('track'=>1));
					
					$arg = array();
					foreach($mus as $m)
					{
						$al = $db->albums;
						$alb = $al->findone(
								array("_id" => new MongoId($m['album_id']))
						);
						
						$art = $db->artists;
						$aat = $art->findone(
							array("_id" => new MongoId($m['artist_id']))
						);
							
						//$userid = USER_SEMENTARA; // sementara supaya bisa setel
						$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$m['key']."/index.m3u8";
                    	$streamV0 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
						
						$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$m['key']."/index.m3u8";
                    	$streamV2 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
						
						$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$m['key']."/index.m3u8";
                    	$streamV4 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;

			    		$zipnew  = "http://track.digibeat.co.id/get.zip";
                    	$zipnew .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
								
						$artimg = $db->artist_images;
						$aatimg = $artimg->findone(
							array("artist_id" => new MongoId($m['artist_id']), 'foto_default' => 'yes')
						);			
						$fotoartist = '';
						if(isset($aatimg['_id']))
						{
							$fotoartist = $aatimg['foto'];
						}
						//foreach($aat['foto'] as $dft)
						//{
						//	$fotoartist = $dft['foto'];
						//	break;
						//}
						
						if(strlen(trim($alb["cover_front"])) > 0)
						{
							$hss = array(
                                "id" => trim($m["_id"]),
                                "track" => intval($m['track']),
                                "albumid" => trim($m['album_id']),
                                "albumname" => $alb['title'],
                                "collectionid" => trim($m["key"]),
                                "length" => intval($m['length']),
                                "description" => $m['description'],
                                "title" => $m['title'],
                                "seo" => $m['seo'],
                                "comment" => $m['description'],
                                "artistid" => trim($m['artist_id']),
                                "imageurl" => CDN_IMAGE.'/image/',
                                "imagename" => $alb["cover_front"],
                                "artistname" => $aat['name'],
                                "streamv4" => $streamV4,
                                "streamv2" => $streamV2,
                                "streamv0" => $streamV0,
								"zip_file" => $zipnew,
								"imageartisturl" => CDN_IMAGE.'/image/',
                    			"imageartistname" => $fotoartist,
                            );
                            $arg[] = $hss;
                        }
					}

					$pp = array(
						'result' => "SUCCESS",
						"data" => $arg,
						'error' => null
					);
					echo json_encode($pp);
					return;
				}
			}
		}
		
		$pp = array(
			'result' => "FAILED",
			"data" => array(),
			'error' => null
		);
		echo json_encode($pp);
		return;
		
	}
	
	public function index()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getAllSongAlbum")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$albumid = "";
					if(isset($p['albumid']))
						$albumid = $p['albumid'];
					$skip = 0;
					if(isset($p['skip']))
						$skip = $p['skip'];
					$limit = 0;
					if(isset($p['limit']))
						$limit = $p['limit'];
					//--
					$userid = "aa";
					if(isset($p['userid']))
						$userid = $p['userid'];
					//--
					//$ex = explode("_", $cookie);	
					//$arr = array(
					//	"cookie" => trim($ex[1])
					//);
					
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					//echo $res;
					//$r = json_decode(json_encode($res), true);
					$r = json_decode($res, true);
					if(is_array($r))
					//$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					//$ddd = json_decode($hhh, true);
					//if(isset($ddd["result"]))
					{
						if($r['error'] == null)
						//if(count($ddd["result"]) > 0)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
							//echo 'isi cek ADALAH '.$cek;
	                        $ada = false;
	                        if($cek == "OK")
							{
								$db = Db::init();
								$mu = $db->musics;
								$mus = $mu->find(
									array('album_id' => new MongoId($albumid), "approve_status"=> "approved", "status_publish"=> "yes")
								)->sort(array('track'=>1));
								
								$arg = array();
								foreach($mus as $m)
								{
									$al = $db->albums;
									$alb = $al->findone(
											array("_id" => new MongoId($m['album_id']))
									);
									
									$art = $db->artists;
									$aat = $art->findone(
										array("_id" => new MongoId($m['artist_id']))
									);
										
									//$userid = USER_SEMENTARA; // sementara supaya bisa setel
									$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$m['key']."/index.m3u8";
	                            	$streamV0 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
									
									$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$m['key']."/index.m3u8";
	                            	$streamV2 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
									
									$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$m['key']."/index.m3u8";
	                            	$streamV4 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
	
						    		$zipnew  = "http://track.digibeat.co.id/get.zip";
	                            	$zipnew .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
											
									$artimg = $db->artist_images;
									$aatimg = $artimg->findone(
										array("artist_id" => new MongoId($m['artist_id']), 'foto_default' => 'yes')
									);			
									$fotoartist = '';
									if(isset($aatimg['_id']))
									{
										$fotoartist = $aatimg['foto'];
									}
									//foreach($aat['foto'] as $dft)
									//{
									//	$fotoartist = $dft['foto'];
									//	break;
									//}
									
									if(strlen(trim($alb["cover_front"])) > 0)
									{
										$hss = array(
		                                    "id" => trim($m["_id"]),
		                                    "track" => intval($m['track']),
		                                    "albumid" => trim($m['album_id']),
		                                    "albumname" => $alb['title'],
		                                    "collectionid" => trim($m["key"]),
		                                    "length" => intval($m['length']),
		                                    "description" => $m['description'],
		                                    "title" => $m['title'],
		                                    "seo" => $m['seo'],
		                                    "comment" => $m['description'],
		                                    "artistid" => trim($m['artist_id']),
		                                    "imageurl" => CDN_IMAGE.'/image/',
			                                "imagename" => $alb["cover_front"],
		                                    "artistname" => $aat['name'],
		                                    "streamv4" => $streamV4,
		                                    "streamv2" => $streamV2,
		                                    "streamv0" => $streamV0,
											"zip_file" => $zipnew,
											"imageartisturl" => CDN_IMAGE.'/image/',
                                			"imageartistname" => $fotoartist,
		                                );
		                                $arg[] = $hss;
	                                }
								}

								$pp = array(
									'id' => 10,
									'result' => $arg,
									'error' => null
								);
								echo json_encode($pp);
								return;
								
							}
						}
					}
				}
			}
		}
				
		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}
}
