<?php

class Share extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function social()
    {
        $body = @file_get_contents('php://input');
        $b = json_decode($body, true);
        if(is_array($b))
        {
            if(isset($b['method']))
            {
                if($b['method'] == "shareSocial")
                {
                    $par = $b['params'];
                    $p = $par[0];
                    $appid = "";
                    if(isset($p['appid']))
                        $appid = $p['appid'];
                    $type = "";
                    if(isset($p['type']))
                        $type = $p['type'];
                    $location = "";
                    if(isset($p['location']))
                        $location = $p['location'];
                    $id = "";
                    if(isset($p['id']))
                        $id = $p['id'];
                    $cookie = "";
                    if(isset($p['cookie']))
                        $cookie = $p['cookie'];
                    
                    $adauser = false;
                    $userid = "";
                    $appname = "";
                    if(strlen(trim($appid)) > 0)
                    {
                        $app = Http::getStr(DOMAIN."appid/search?id=".$appid."&type=hexa");
                        
                        $mapp = json_decode($app, true);
                        
                        if(count($mapp) > 2)
                        {
                            $appname = $mapp['appname'];
                            $ex = explode("_", $cookie);    
                            $arr = array(
                                "cookie" => trim($ex[1])
                            );
                            $hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
                            $ddd = json_decode($hhh, true);
                            if(isset($ddd["result"]))
                            {
                                if(count($ddd["result"]) > 0)
                                {                                    
                                    foreach($ddd['result'] as $rsl)
                                    {
                                        $adauser = true;
                                        $userid = base_convert($rsl['userid'], 10, 16);
                                    }
                                }
                            }
                        }
                    }

                    if($adauser)
                    {
                        $insert = false;
                        if((strlen(trim($type)) > 0) && (strlen(trim($id)) > 0))
                        {
                            $data = array(
                                "appname" => $appname,
                                "location" => $location,
                                "userid" => $userid,
                                "type" => $type,
                                "id" => $id,
                            );
                            
                            $db = Db::init();
                            $shr = $db->share_users;
                            $shr->insert($data);
                            
                            $insert = true;
                            $rpc = array(
                                "insert" => "sukses"
                            );
                            
                            echo json_encode($rpc);    
                        }
                        
                        if(!$insert)
                        {
                            $rpc = array(
                                "insert" => "gagal"
                            );
                            
                            echo json_encode($rpc);
                        }                        
                    }
                }
            }
        }
    }
}
