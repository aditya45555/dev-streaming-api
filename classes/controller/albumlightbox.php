<?php

class albumlightbox_controller extends  controller
{
	
	public function hapus()
	{
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "deleteAlbumLightbox")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$id = "";
					if(isset($p['id']))
						$id = $p['id'];
					$albumid = "";
					if(isset($p['albumid']))
						$albumid = $p['albumid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))			
					{
						if($r['error'] == null)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
	                        $ada = false;
	                        if($cek == "OK")
							{
								if(strlen(trim($albumid)) > 0)
								{
									$db = Db::init();
									$mlight = $db->album_lightboxs;
									$q = array(
										'album_id' => new MongoId($albumid),
										'lightbox_id' => new MongoId($id)
									);
									$mmlight = $mlight->findOne($q);
									if($mmlight)
										$mlight->remove(array('_id' => new MongoId($mmlight['_id'])));
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array('delete' => 'SUKSES')
									);
									echo json_encode($hasil);
									return;
								}								
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if((strlen(trim($albumid)) > 0) && (strlen(trim($id))> 0) && (strlen(trim($appid))> 0))
							{
								$ahhga = Http::getStr(DOMAIN."album_lightbox/delete?id=".$id);
								$ahhgar= json_decode($ahhga, true);
								if(is_array($ahhgar))
								{
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array($ahhgar)
									);
									echo json_encode($hasil);
									return;
								}
							}
						}
					}*/
				}
			}
		}
		$hasil = array(
			'id' => 5,
			'error' => 204,
		);
		echo json_encode($hasil);
	}
	
	public function getdetail()
	{
		$appid = Base::instance()->get("GET.appid");
		$userid = Base::instance()->get("GET.userid");
		$cookie = Base::instance()->get("GET.cookie");
		$lightboxid = Base::instance()->get("GET.lightboxid");
		$skip = Base::instance()->get("GET.skip");
		$limit = Base::instance()->get("GET.limit");
		if(strlen(trim($lightboxid)) > 0)
		{
			$curl = new Curl();
			$curl->get('http://track.digibeat.co.id/albumlightbox/list', array(
					'skip' => $skip,
					'limit' => $limit,
					'appid' => $appid,
					'userid' => $userid,
					'lightboxid' => $lightboxid,
			));
			$res = $curl->response;
			$r = json_decode(json_encode($res), true);
			if(is_array($r))
			{
				if($r['error'] == null)
			    {
			    	$arg = array();
					$db = Db::init();
					foreach($r['result'] as $rt)
					{
						$ab = $db->albums;
						$alb = $ab->findone(
							array("_id" => new MongoId($rt['albumid']))
						);
						
						if(isset($alb['_id']))
						{
							$art = $db->album_artists;
							$aat = $art->findone(
								array("album_id" => new MongoId($alb['_id']))
							);
									
							$art2 = $db->artists;
							$aat2 = $art2->findone(
								array("_id" => new MongoId($aat['artist_id']))
							);
							$ai = $db->artist_images;
							$mai = $ai->find(array("artist_id" => new MongoId($aat['artist_id']), "foto_default"=>"yes") );
							$fotoartist = '';
							foreach($mai as $ft)
	                        {
	                            if(isset($ft['foto']))
	                            {
	                                $fotoartist  =$ft['foto'];
									break;
	                            }                                                                               
	                        }
							
		                    $album = array(
		                        "albumid" => trim($alb['_id']),
		                        "albumname" => $alb['title'],
		                        "title" => $alb['title'],
		                        "seo" => $alb['seo'],
		                        "artistname" => $aat2['name'],
		                        "description" => $alb['description'],
		                        "artistid" => trim($aat2['_id']),
		                        "genreid" => trim($alb['genre_id']),
		                        "imageurl" => CDN_IMAGE."/image/",
		                        "imagename" => $alb['cover_front'],
		                        "imageartist" => $fotoartist,
		                     );
		                     $arg[] = $album;
	                     }
					}
					$pp = array(
						'id' => 10,
						'result' => $arg,
						'error' => null
					);
					echo json_encode($pp);
					return;
				}
			}
		}
		
		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}
	
	public function insert()
	{
		//$appid = Base::instance()->get("GET.appid");
		//$cookie = Base::instance()->get("GET.cookie");
		//$genreid = Base::instance()->get("GET.genreid");
		//$userid = Base::instance()->get("GET.userid");
		//$albumid = Base::instance()->get("GET.albumid");
		//$lightboxid = Base::instance()->get("GET.lightboxid");
		
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "insertAlbumLightbox")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$genreid = "";
					if(isset($p['genreid']))
						$genreid = $p['genreid'];
					$albumid = "";
					if(isset($p['albumid']))
						$albumid = $p['albumid'];
					$lightboxid = "";
					if(isset($p['lightboxid']))
						$lightboxid = $p['lightboxid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if((strlen(trim($lightboxid)) > 0) && (strlen(trim($albumid))> 0) && (strlen(trim($userid))> 0))
					{
						$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$db = Db::init();
									$albl = $db->album_lightboxs;
									$alb = $db->albums;
									$malb = $alb->findOne(array('_id' => new MongoId($albumid)));
									
									if(strlen(trim($genreid)) > 0)
										$genre = new MongoId($genreid);									
									else
										$genre = new MongoId($malb['genre_id']);
									
									$ddt = array(
										'lightbox_id' => new MongoId($lightboxid),
										'album_id' => new MongoId($albumid),
										'genre_id' => $genre,
										'album_category_id' => new MongoId($malb['album_category_id']),
										'userid' => trim($userid),
									);
									
									$albl->insert($ddt);
									$arr = array(
										"id" => trim($ddt['_id'])
									);
									$hasil = array(
										'error' => null,
										'result' => array($arr)
									);
									echo json_encode($hasil);
			                    }
							}
						}
					}
					
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if((strlen(trim($lightboxid)) > 0) && (strlen(trim($albumid))> 0) && (strlen(trim($genreid))> 0))
							{
								$arrg = array(
									"lightboxid" => intval(base_convert($lightboxid, 16, 10)),
									"albumid" => intval(base_convert($albumid, 16, 10)),
									"genreid" => intval(base_convert($genreid, 16, 10)),
									"time_created" => intval($this->ambilTgl())
								);
								$hhhg = Http::kirimJson(DOMAIN."album_lightbox/insert", json_encode($arrg));
								echo $hhhg;
							}
						}
					}*/
				}
			}
		}
	}

	public function index()
	{
		//$body = http_get_request_body();
		//$appid = Base::instance()->get("GET.appid");
		//$cookie = Base::instance()->get("GET.cookie");
		//$skip = Base::instance()->get("GET.skip");
		//$limit = Base::instance()->get("GET.limit");
		//$lightboxid = Base::instance()->get("GET.lightboxid");
		
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getAllAlbumLightbox")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$skip = 0;
					if(isset($p['skip']))
						$skip = $p['skip'];
					$limit = 0;
					if(isset($p['limit']))
						$limit = $p['limit'];
					$lightboxid = "";
					if(isset($p['lightboxid']))
						$lightboxid = $p['lightboxid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))
					{
						if($r['error'] == null)
		                {
		                	$cek = '';
			                foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
									
							//echo 'isi cek ADALAH '.$cek;			                
			                if($cek == "OK")
		                    {
		                    	$db = Db::init();
								$albl = $db->album_lightboxs;
								$alb = $db->albums;
								$albart = $db->album_artists;
								$art = $db->artists;
								
								$malbl = $albl->find(array('lightbox_id' => new MongoId($lightboxid)));
								
								if($malbl->count() > 0)
								{
									$result = array();
									foreach($malbl as $dt)
									{
										$malb = $alb->findOne(array('_id' => new MongoId($dt['album_id'])));
										
										if($malb)
										{
											$malbart = $albart->findOne(array('album_id' => new MongoId($dt['album_id'])));
											if($malbart)
											{
												$mart = $art->findOne(array('_id' => new MongoId($malbart['artist_id'])));
												
												$album = array(
													"id" => trim($dt['_id']),
													"title" => preg_replace('/[^[:print:]]/', '', trim($malb["title"])),
													"description" => $malb["description"],
													"albumid" => trim($malb["_id"]),
													"albumname" => preg_replace('/[^[:print:]]/', '', trim($malb["title"])),
													"artistid" => trim($mart["_id"]),
													"lightboxid" => trim($lightboxid),
													"genreid" => trim($dt['genre_id']),
													"artistname" => preg_replace('/[^[:print:]]/', '', trim($mart["name"])),
													"imageurl" => CDN_IMAGE.'/image/',
													"imagename" => trim($malb['cover_front']),
												);
												$result[] = $album;
											}
										}																				
									}
									$rpc = array(
										"result" => $result,
										"error" => null,
										"id"=> 5
									);
									echo json_encode($rpc);
									return;
								} 
		                    }
						}
					}
				}

				if($b['method'] == "getAllAlbumLightboxByGenre")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$skip = 0;
					if(isset($p['skip']))
						$skip = $p['skip'];
					$limit = 0;
					if(isset($p['limit']))
						$limit = $p['limit'];
					$lightboxid = "";
					if(isset($p['lightboxid']))
						$lightboxid = $p['lightboxid'];
					$genreid = "";
					if(isset($p['genreid']))
						$genreid = $p['genreid'];
					//$ex = explode("_", $cookie);	
					//$arr = array(
					//	"cookie" => trim($ex[1])
					//);
					
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					//echo $res;
					//$r = json_decode(json_encode($res), true);
					$r = json_decode($res, true);
					if(is_array($r))
					//$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					//$ddd = json_decode($hhh, true);
					//if(isset($ddd["result"]))
					{
						if($r['error'] == null)
						//if(count($ddd["result"]) > 0)
						{
							$cek = '';
		                    foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
								
		                    $ada = false;
		                    if($cek == "OK")
							{
								if((strlen(trim($genreid))> 0) && (strlen(trim($lightboxid))> 0))
								{
									$db = Db::init();
									$lg = $db->album_lightboxs;
									$lb = $lg->find(
										array("lightbox_id" => new MongoId($lightboxid))
									);
									$arg = array();
									foreach($lb as $lbd)
									{
										$al = $db->albums;
										$alb = $al->findone(
											array("_id" => new MongoId($lbd['album_id']))
										);
										if(isset($alb['_id']))
										{
											if(trim($alb['genre_id']) == trim($genreid))
											{
												$ar = $db->album_artists;
												$aa = $ar->findone(
													array("album_id" => new MongoId($alb['_id']))
												);
												
												$art = $db->artists;
												$aat = $art->findone(
													array("_id" => new MongoId($aa['artist_id']))
												);
										
												$fotoartist = '';
												if(isset($aat['foto']))
												{
													foreach($aat['foto'] as $dft)
													{
														$fotoartist = $dft['foto'];
														//break;
													}
												}
									
												$p = array(
														"title" => $alb["title"],
														"description" => $alb["description"],
														"albumid" => trim($alb["_id"]),
														"albumname" => preg_replace('/[^[:print:]]/', '', trim($alb["title"])),
														"artistid" => trim($aa['_id']),
														"lightboxid" => trim($lightboxid),
														"genreid" => trim($genreid),
														"artistname" => preg_replace('/[^[:print:]]/', '', trim($aat['name'])),
														"imageurl" => CDN_IMAGE.'/image/',
		                                                "imagename" => $alb["cover_front"],
		                                                "imageartist" => $fotoartist,
												);
												$arg[] = $p;
											} //else echo "genre gak sama";
										} //else echo "album gak ada";
									}

									$pp = array(
										'id' => 10,
										'result' => $arg,
										'error' => null
									);
									echo json_encode($pp);
									return;
									/*
									$arrg = array(
										"lightboxid" => intval(base_convert($lightboxid, 16, 10)),
										"genreid" => intval(base_convert($genreid, 16, 10))
									);
									$hhhg = Http::kirimJson(DOMAIN."album_lightbox/search", json_encode($arrg));
									$dddg = json_decode($hhhg, true);
									if(is_array($dddg))
									{
										if(isset($dddg["result"]))
										{
											$result = array();
											foreach($dddg["result"] as $ddf)
											{
												$n = Http::getStr(DOMAIN."album/search?id=".base_convert($ddf['albumid'], 10, 16));
	                                            $n = preg_replace('/[^[:print:]]/', '', $n);
												$nn = json_decode($n, true);
												if(is_array($nn))
												{
													$a = Http::getStr(DOMAIN."artist/search?id=".base_convert($nn["artistid"], 10, 16));
													$aa = json_decode($a, true);
													
													$imagelink = DOMAIN_STREAM;
													$imagelink .= "images.jpg?id=".$nn["image"];
													$imagelink .= "&appid=".$appid;
													$imagelink .= "&cookie=".$cookie;
													$album = array(
														"title" => $nn["title"],
														"description" => $nn["description"],
														"albumid" => $nn["id"],
														"albumname" => $nn["title"],
														"artistid" => $aa["id"],
														"lightboxid" => $lightboxid,
														"genreid" => $genreid,
														"artistname" => $aa["name"],
														"imagelink" => $imagelink
													);
													$result[] = $album;
												}
											}
											$rpc = array(
												"result" => $result,
												"error" => null,
												"id"=> 5
											);
											echo json_encode($rpc);
										}
									}*/
								}
							}
						}
					}
				}
				//EndGenreDefault
				if($b['method'] == "getAllAlbumLightboxByGenreExplore")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$skip = 0;
					if(isset($p['skip']))
						$skip = $p['skip'];
					$limit = 0;
					if(isset($p['limit']))
						$limit = $p['limit'];
					$lightboxid = "";
					if(isset($p['lightboxid']))
						$lightboxid = $p['lightboxid'];
					$genreid = "";
					if(isset($p['genreid']))
						$genreid = $p['genreid'];
					
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;

					$r = json_decode($res, true);
					if(is_array($r))
					{
						if($r['error'] == null)
						{
							$cek = '';
		                    foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
								
		                    $ada = false;
		                    if($cek == "OK")
							{
								if((strlen(trim($genreid))> 0) && (strlen(trim($lightboxid))> 0))
								{
									$db = Db::init();
									$lg = $db->album_lightboxs;
									$q= array(
											"lightbox_id" => new MongoId($lightboxid),
											"genre_explore_id" => new MongoId($genreid)
										);
									$lb = $lg->find($q);
									$arg = array();
									foreach($lb as $lbd)
									{
										$al = $db->albums;
										$alb = $al->findone(
											array("_id" => new MongoId($lbd['album_id']))
										);
										if(isset($alb['_id']))
										{
											$ar = $db->album_artists;
											$aa = $ar->findone(
												array("album_id" => new MongoId($alb['_id']))
											);
											
											$art = $db->artists;
											$aat = $art->findone(
												array("_id" => new MongoId($aa['artist_id']))
											);
									
											$fotoartist = '';
											if(isset($aat['foto']))
											{
												foreach($aat['foto'] as $dft)
												{
													$fotoartist = $dft['foto'];
												}
											}
								
											$p = array(
												"title" => $alb["title"],
												"description" => $alb["description"],
												"albumid" => trim($alb["_id"]),
												"albumname" => preg_replace('/[^[:print:]]/', '', trim($alb["title"])),
												"artistid" => trim($aa['_id']),
												"lightboxid" => trim($lightboxid),
												"genreid" => trim($genreid),
												"artistname" => preg_replace('/[^[:print:]]/', '', trim($aat['name'])),
												"imageurl" => CDN_IMAGE.'/image/',
                                                "imagename" => $alb["cover_front"],
                                                "imageartist" => $fotoartist,
											);
											$arg[] = $p;
										} //else echo "album gak ada";
									}

									$pp = array(
										'id' => 10,
										'result' => $arg,
										'error' => null
									);
									echo json_encode($pp);
									return;
								}
							}
						}
					}
				}
				//End Genre Explore
				
			}
		}
		$p = array(
			
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => null
		);
		echo json_encode($pp);
	}
	
	public function getallalbumlightboxbygenreexplore()
	{
		if(!empty($_POST))
		{
			$appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$cookie = "";
			if(isset($_POST['cookie']))
				$cookie = $_POST['cookie'];
			$userid = "";
			if(isset($_POST['userid']))
				$userid = $_POST['userid'];
			$skip = 0;
			if(isset($_POST['skip']))
				$skip = $_POST['skip'];
			$limit = 0;
			if(isset($_POST['limit']))
				$limit = $_POST['limit'];
			$lightboxid = "";
			if(isset($_POST['lightboxid']))
				$lightboxid = $_POST['lightboxid'];
			$genreid = "";
			if(isset($_POST['genreid']))
				$genreid = $_POST['genreid'];
			
			$validator = new Validator();
	        $validator->addRule('userid', array('require'));
	        $validator->addRule('cookie', array('require'));
	        $validator->addRule('appid', array('require'));
			$validator->addRule('lightboxid', array('require'));
			$validator->addRule('genreid', array('require'));
	        $validator->setData(array(
	        	'userid' => $userid,
	            'cookie' => $cookie,
	            'appid' => $appid,
	            'lightboxid' => $lightboxid,
	            'genreid' => $genreid
	        ));		
			
			if($validator->isValid())
			{
				$cekcookie = helper::cekCookie($userid, $cookie);
				if($cekcookie)
				{
					$db = Db::init();
					$lg = $db->album_lightboxs;
					$q= array(
							"lightbox_id" => new MongoId($lightboxid),
							"genre_explore_id" => new MongoId($genreid)
						);
					$lb = $lg->find($q);
					$arg = array();
					foreach($lb as $lbd)
					{
						$al = $db->albums;
						$alb = $al->findone(
							array("_id" => new MongoId($lbd['album_id']))
						);
						if(isset($alb['_id']))
						{
							$ar = $db->album_artists;
							$aa = $ar->findone(
								array("album_id" => new MongoId($alb['_id']))
							);
							
							$art = $db->artists;
							$aat = $art->findone(
								array("_id" => new MongoId($aa['artist_id']))
							);
					
							$fotoartist = '';
							if(isset($aat['foto']))
							{
								foreach($aat['foto'] as $dft)
								{
									$fotoartist = $dft['foto'];
								}
							}
				
							$p = array(
								"title" => $alb["title"],
								"description" => $alb["description"],
								"albumid" => trim($alb["_id"]),
								"albumname" => preg_replace('/[^[:print:]]/', '', trim($alb["title"])),
								"artistid" => trim($aa['_id']),
								"lightboxid" => trim($lightboxid),
								"genreid" => trim($genreid),
								"artistname" => preg_replace('/[^[:print:]]/', '', trim($aat['name'])),
								"imageurl" => CDN_IMAGE.'/image/',
                                "imagename" => $alb["cover_front"],
                                "imageartist" => $fotoartist,
							);
							$arg[] = $p;
						} //else echo "album gak ada";
					}

					$pp = array(
						'result' => "SUCCESS",
						'data' => $arg,
						'error' => null
					);
					echo json_encode($pp);
					return;
				}
			}
		}
		
		$pp = array(
			'result' => "FAILED",
			'data' => array(),
			'error' => 204
		);
		echo json_encode($pp);
		
	}
	
	private function ambilTgl()
	{
		$a = explode(" ", microtime());
		$b = explode(".", $a[0]);
		$c = substr($b[1], 0, 6);
		$d = $a[1].$c;
		return $d;
	}
}