<?php

class genre_controller extends controller
{
	public function getallgenredefault()
	{
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				$par = $b['params'];
				$p = $par[0];
				
				$appid = "";
				if(isset($p['appid']))
					$appid = $p['appid'];
				$cookie = "";
				if(isset($p['cookie']))
					$cookie = $p['cookie'];
				$userid = "";
                if(isset($p['userid']))
                    $userid = $p['userid'];
				$skip = 0;
				if(isset($p['skip']))
					$skip = $p['skip'];
				$limit = 0;
				if(isset($p['limit']))
					$limit = $p['limit'];
					
				if($b['method'] == "getAllGenreDefault")
				{
					
					//$ex = explode("_", $cookie);	
					//$arr = array(
					//	"cookie" => trim($ex[1])
					//);
					//$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					//$ddd = json_decode($hhh, true);
					
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					//echo $res;
					//$r = json_decode(json_encode($res), true);
					$r = json_decode($res, true);
					if(is_array($r))
					//if(isset($ddd["result"]))
					{
						//if(count($ddd["result"]) > 0)
						if($r['error'] == null)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
	                        $ada = false;
	                        if($cek == "OK")
							{
								$db = Db::init();
								$gen = $db->genres;
								$tunin = $gen->find(
									array('menu_status' => 1)
								);
								
								/*$arrg = array(
									"status" => "yes"
								);
								$hhhg = Http::kirimJson(DOMAIN."genre/search", json_encode($arrg));
	                            $ahhg = preg_replace("/#?[a-z0-9]+;/i", '', $hhhg);
								$dddg = json_decode($ahhg, true);
								if(is_array($dddg))
								{
									echo json_encode($dddg);
								}*/
								
								$arr = array();
								foreach($tunin as $dd)
								{
									$p = array(
										'id' => trim($dd['_id']),
										'name' => preg_replace("/#?[a-z0-9]+;/i", '', trim($dd['name'])),
										'description' => trim($dd['description']),
									);
									$arr[] = $p;
								}
								$pp = array(
									'id' => 10,
									'result' => $arr,
									'error' => null
								);
	                                        
	                            echo json_encode($pp);
								return;
							}
						}
					}
				}
				//End Genre Default
				
			}
		}

		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}

	public function getallgenreexplore()
	{
		if(!empty($_POST))
		{
		    $appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$cookie = "";
			if(isset($_POST['cookie']))
				$cookie = $_POST['cookie'];
			$userid = "";
	        if(isset($_POST['userid']))
	            $userid = $_POST['userid'];
			$skip = 0;
			if(isset($_POST['skip']))
				$skip = $_POST['skip'];
			$limit = 0;
			if(isset($_POST['limit']))
				$limit = $_POST['limit'];
				
		    $validator = new Validator();
	        $validator->addRule('userid', array('require'));
	        $validator->addRule('cookie', array('require'));
	        $validator->addRule('appid', array('require'));
	        $validator->setData(array(
	        	'userid' => $userid,
	            'cookie' => $cookie,
	            'appid' => $appid,
	        ));		
			
			if($validator->isValid())
			{
				$cekcookie = helper::cekCookie($userid, $cookie);
				if($cekcookie)
				{
					$db = Db::init();
					$gen = $db->genre_explores;
					$tunin = $gen->find(
						array('status' => 'active')
					);
					
					$arr = array();
					foreach($tunin as $dd)
					{
						$p = array(
							'id' => trim($dd['_id']),
							'name' => preg_replace("/#?[a-z0-9]+;/i", '', trim($dd['name'])),
							'description' => trim($dd['description']),
						);
						$arr[] = $p;
					}
					
					$pp = array(
						'result' => "SUCCESS",
						'data' => $arr,
						'error' => null
					);
                                
                    echo json_encode($pp);
					return;		
				}
			}
		}

		$pp = array(
			'result' => "FAILED",
			'data' => array(),
			'error' => 204
		);
		echo json_encode($pp);

	}

}