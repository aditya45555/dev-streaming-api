<?php

class songplaylist_controller extends  controller
{
	
	public function hapus()
	{
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "deleteSongPlaylist")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$id = "";
					if(isset($p['id']))
						$id = $p['id'];
					$collectionid = "";
					if(isset($p['collectionid']))
						$collectionid = $p['collectionid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if(strlen(trim($id)) > 0)
					{
						$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$db = Db::init();
									$pl = $db->music_playlists;
									$q = array(
										'music_id' => new MongoId($collectionid),
										'playlist_id' => new MongoId($id)
									);
									
									$mpl = $pl->findOne($q);
									if($mpl)
										$pl->remove(array('_id' => new MongoId($mpl['_id'])));
									
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array('delete' => 'SUKSES')
									);
									echo json_encode($hasil);
									return;
			                    }
							}
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if((strlen(trim($collectionid)) > 0) && (strlen(trim($id))> 0) && (strlen(trim($appid))> 0))
							{
								$ahhga = Http::getStr(DOMAIN."song_playlist/delete?id=".$id);
								$ahhgar= json_decode($ahhga, true);
								if(is_array($ahhgar))
								{
									$hasil = array(
										'id' => 5,
										'error' => null,
										'result' => array($ahhgar)
									);
									echo json_encode($hasil);
									return;
								}
							}
						}
					}*/
				}
			}
		}
		$hasil = array(
			'id' => 5,
			'error' => 204,
		);
		echo json_encode($hasil);
	}
	
	public function getdetail()
	{
		$appid = Base::instance()->get("GET.appid");
		$userid = Base::instance()->get("GET.userid");
		$cookie = Base::instance()->get("GET.cookie");
		$playlistid = Base::instance()->get("GET.playlistid");
		$skip = Base::instance()->get("GET.skip");
		$limit = Base::instance()->get("GET.limit");
		if(strlen(trim($playlistid)) > 0)
		{
			$curl = new Curl();
			$curl->get('http://track.digibeat.co.id/songplaylist/list', array(
					'skip' => $skip,
					'limit' => $limit,
					'appid' => $appid,
					'userid' => $userid,
					'playlistid' => $playlistid,
			));
			$res = $curl->response;
			//$r = json_decode($res, true);
			$r = json_decode(json_encode($res), true);
			if(is_array($r))
			{
				if($r['error'] == null)
			    {
			    	$arg = array();
					foreach($r['result'] as $rt)
					{
						
						$songid = $rt['songid'];
						$db = Db::init();
						$mm = $db->musics;
						//$m = $mm->findone(array('key' => $songid));
						$i = strlen($songid);
						if ($i==24)
							$q = array('_id' => new MongoId($songid));
						else
							$q = array('key' => $songid);
						
						$m = $mm->findone($q);
						if(isset($m['_id']))
						{
							$al = $db->albums;
							$alb = $al->findone(
								array("_id" => new MongoId($m['album_id']))
							);
									
							$art = $db->artists;
							$aat = $art->findone(
								array("_id" => new MongoId($m['artist_id']))
							);
										
							$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$m['key']."/index.m3u8";
	                        $streamV0 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
									
							$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$m['key']."/index.m3u8";
	                        $streamV2 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
									
							$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$m['key']."/index.m3u8";
	                        $streamV4 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
	
						   	$zipnew  = "http://track.digibeat.co.id/get.zip";
	                        $zipnew .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
												
							if(strlen(trim($alb["cover_front"])) > 0)
							{
								$hss = array(
		                        	"id" => trim($m["_id"]),
		                            "track" => intval($m['track']),
		                            "albumid" => trim($m['album_id']),
		                            "albumname" => $alb['title'],
		                            "collectionid" => trim($m["_id"]),
		                            "length" => intval($m['length']),
		                            "description" => $m['description'],
		                            "title" => $m['title'],
		                            "seo" => $m['seo'],
		                            "comment" => $m['description'],
		                            "artistid" => trim($m['artist_id']),
		                            "imageurl" => CDN_IMAGE.'/image/',
			                        "imagename" => $alb["cover_front"],
		                            "artistname" => $aat['name'],
		                            "streamv4" => $streamV4,
		                            "streamv2" => $streamV2,
		                            "streamv0" => $streamV0,
									"zip_file" => $zipnew,
		                        );
		                        $arg[] = $hss;
	                        }
						}
					}

					$pp = array(
						'id' => 10,
						'result' => $arg,
						'error' => null
					);
					echo json_encode($pp);
					return;
				}
			}
		}
		
		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}

	public function getfeatureddetail()
	{
		$appid = Base::instance()->get("GET.appid");
		$userid = Base::instance()->get("GET.userid");
		$cookie = Base::instance()->get("GET.cookie");
		$playlistid = Base::instance()->get("GET.playlistid");
		$skip = Base::instance()->get("GET.skip");
		$limit = Base::instance()->get("GET.limit");
		if(strlen(trim($playlistid)) > 0)
		{
			$curl = new Curl();
			$curl->get('https://sso.dboxid.com/login/cekcookie', array(
				'userid' => $userid,
				'cookie' => $cookie,
			));
			$res = $curl->response;
			$r = json_decode($res, true);
			if(is_array($r))
			{
				if($r['error'] == null)
				{
					$arg = array();
					$db = Db::init();
					$tun = $db->music_playlists;
					$arrg = array("playlist_id" => new MongoId($playlistid));
					$dp = $tun->find($arrg);
					
					foreach($dp as $rt)
					{
						$songid = $rt['music_id'];
						$db = Db::init();
						$mm = $db->musics;
						$m = $mm->findone(array('_id' => new MongoId($songid)));
						if(isset($m['_id']))
						{
							$al = $db->albums;
							$alb = $al->findone(
								array("_id" => new MongoId($m['album_id']))
							);
									
							$art = $db->artists;
							$aat = $art->findone(
								array("_id" => new MongoId($m['artist_id']))
							);
										
							//$userid = USER_SEMENTARA; // sementara supaya bisa setel
							$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$m['key']."/index.m3u8";
	                        $streamV0 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
									
							$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$m['key']."/index.m3u8";
	                        $streamV2 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
									
							$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$m['key']."/index.m3u8";
	                        $streamV4 .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&appid=".$appid;
	
						   	$zipnew  = "http://track.digibeat.co.id/get.zip";
	                        $zipnew .= "?userid=".$userid."&songid=".$m['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
												
							if(strlen(trim($alb["cover_front"])) > 0)
							{
								$hss = array(
		                        	"id" => trim($m["_id"]),
		                            "track" => intval($m['track']),
		                            "albumid" => trim($m['album_id']),
		                            "albumname" => $alb['title'],
		                            "collectionid" => trim($m["_id"]),
		                            "length" => intval($m['length']),
		                            "description" => $m['description'],
		                            "title" => $m['title'],
		                            "seo" => $m['seo'],
		                            "comment" => $m['description'],
		                            "artistid" => trim($m['artist_id']),
		                            "imageurl" => CDN_IMAGE.'/image/',
			                        "imagename" => $alb["cover_front"],
		                            "artistname" => $aat['name'],
		                            "streamv4" => $streamV4,
		                            "streamv2" => $streamV2,
		                            "streamv0" => $streamV0,
									"zip_file" => $zipnew,
		                        );
		                        $arg[] = $hss;
	                        }
						}
					}

					$pp = array(
						'id' => 10,
						'result' => $arg,
						'error' => null
					);
					echo json_encode($pp);
					return;
				}
			}
		}
		
		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}
	
	
	public function insert()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "insertSongPlaylist")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$genreid = "";
					if(isset($p['genreid']))
						$genreid = $p['genreid'];
					$collectionid = "";
					if(isset($p['collectionid']))
						$collectionid = $p['collectionid'];
					$playlistid = "";
					if(isset($p['playlistid']))
						$playlistid = $p['playlistid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					if((strlen(trim($collectionid)) > 0) && (strlen(trim($playlistid))> 0) && (strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))        
        			{
        				$curl = new Curl();
						$curl->get('https://sso.dboxid.com/login/cekcookie', array(
								'userid' => $userid,
								'cookie' => $cookie,
						));
						$res = $curl->response;
						$r = json_decode($res, true);
						if(is_array($r))
						{
							if($r['error'] == null)
			                {
			                	$cek = '';
				                foreach($r['result'] as $rt)
								{
									$cek = $rt['cek'];
								}
										
								//echo 'isi cek ADALAH '.$cek;
				                $ada = false;
				                if($cek == "OK")
			                    {
			                    	$db = Db::init();
									$mus = $db->musics;
									$mmus = $mus->findOne(array('_id' => new MongoId($collectionid)));
									
									if(strlen(trim($genreid)) > 0)
										$genre = new MongoId($genreid);
									else
										$genre = new MongoId($mmus['genre_id']);
									
			                    	$arrg = array(
										"music_id" => new MongoId($collectionid),
										"playlist_id" => new MongoId($playlistid),
										"genre_id" => $genre,
										"userid" => trim($userid),
										"time_created" => trim($this->ambilTgl())
									);
																		
									$playlist = $db->music_playlists;
									$playlist->insert($arrg);
									$arr = array(
										"id" => trim($arrg['_id'])
									);
									$hasil = array(
										'error' => null,
										'result' => array($arr)
									);
									echo json_encode($hasil);
			                    }
							}
						}
        			}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							if((strlen(trim($collectionid)) > 0) && (strlen(trim($playlistid))> 0) && (strlen(trim($genreid))> 0) &&(strlen(trim($userid)) > 0) && (strlen(trim($appid)) > 0) && (strlen(trim($cookie)) > 0))
							{
								$arrg = array(
									"collectionid" => intval(base_convert($collectionid, 16, 10)),
									"playlistid" => intval(base_convert($playlistid, 16, 10)),
									"genreid" => intval(base_convert($genreid, 16, 10)),
									"time_created" => intval($this->ambilTgl())
								);
								$hhhg = Http::kirimJson(DOMAIN."song_playlist/insert", json_encode($arrg));
								echo $hhhg;
							}
						}
					}*/
				}
			}
		}
	}
	
	public function index()
	{
		//$body = http_get_request_body();
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "getAllSongPlaylist")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$playlistid = "";
					if(isset($p['playlistid']))
						$playlistid = $p['playlistid'];
					$skip = 0;
					if(isset($p['skip']))
						$skip = $p['skip'];
					$limit = 0;
					if(isset($p['limit']))
						$limit = $p['limit'];
					/*$ex = explode("_", $cookie);	
					$arr = array(
						"cookie" => trim($ex[1])
					);*/
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))
					{
						//if(count($ddd["result"]) > 0)
						if($r['error'] == null)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
							//echo 'isi cek ADALAH '.$cek;
	                        $ada = false;
	                        if($cek == "OK")
	                        {
	                        	$db = Db::init();
								$tun = $db->music_playlists;
								$arrg = array(
									"playlist_id" => new MongoId($playlistid)
								);
								$dp = $tun->find($arrg)->sort(array('no'=>1));
								$arr = array();
								foreach($dp as $dpp)
								{
									$tun = $db->musics;
									$arrg = array(
										"_id" => new MongoId($dpp['music_id'])
									);
									$dp = $tun->findone($arrg);
									if(isset($dp['_id']))
									{
										$al = $db->albums;
										$alb = $al->findone(
												array("_id" => new MongoId($dp['album_id']))
										);
										
										$art = $db->artists;
										$aat = $art->findone(
											array("_id" => new MongoId($dp['artist_id']))
										);
										
										//$userid = USER_SEMENTARA; // sementara supaya bisa setel
										$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$dp['key']."/index.m3u8";
		                            	$streamV0 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
										
										$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$dp['key']."/index.m3u8";
		                            	$streamV2 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
										
										$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$dp['key']."/index.m3u8";
		                            	$streamV4 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
		
							    		$zipnew  = "http://track.digibeat.co.id/get.zip";
		                            	$zipnew .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
								
										$hss = array(
											"id" => trim($dp["_id"]),
											"track" => intval($dp['track']),
											"albumid" => trim($dp["album_id"]),
											"genreid" => trim($dp['genre_id']),
											"collectionid" => trim($dp["_id"]),
											"length" => intval($dp['length']),
											"title" => $dp['title'],
											"seo" => $dp['seo'],
											"description" => $dp['description'],
											"comment" => $dp['description'],
											"artistid" => trim($dp['artist_id']),
											"albumname" => $alb['title'],
											"imageurl" => CDN_IMAGE.'/image/',
		                                	"imagename" => $alb["cover_front"],
											"artistname" => $aat['name'],
											"streamv4" => $streamV4,
											"streamv2" => $streamV2,
											"streamv0" => $streamV0,
											"playlistid" => $playlistid,
											"zip_file" => $zipnew,
										);
										
										$arr[] = $hss;
									}
								}
								$pp = array(
									'id' => 10,
									'result' => $arr,
									'error' => null
								);
								echo json_encode($pp);
								return;
	                        }
						}
					}
					/*$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					$ddd = json_decode($hhh, true);
					if(isset($ddd["result"]))
					{
						if(count($ddd["result"]) > 0)
						{
							$arrg = array(
								"playlistid" => intval(base_convert($playlistid, 16, 10)),
							);
							$hhhg = Http::kirimJson(DOMAIN."song_playlist/search", json_encode($arrg));
							$dddg = json_decode($hhhg, true);
							if(is_array($dddg))
							{
								//echo "albumid : ".$albumid;
								
								$db = Db::init();
                                $mus = $db->musics;
								
								$data = array();
								foreach($dddg["result"] as $res)
								{
									$collectionid = base_convert($res['collectionid'], 10, 16);
                                    
                                    $mmus = $mus->findOne(array("key" => $collectionid));
                                    
                                    $title = "";
                                    $seo = "";
                                    $description = "";
                                    $track = 0;
                                    $length = 0;
                                    if($mmus)
                                    {
                                        $title = $mmus['title'];
                                        $seo = $mmus['seo'];
                                        $description = $mmus['description'];
                                        $track = $mmus['track'];
                                        $length = $mmus['length'];
                                    }
                                    
									$ahhg = Http::getStr(DOMAIN."audio_collection/search?id=".$collectionid);
									$audio_colect = json_decode($ahhg, true);
									if(count($audio_colect) > 2)
									{
										$arrg = array(
											"collectionid" => intval(base_convert($collectionid, 16, 10)),
										);
										$hhhg = Http::kirimJson(DOMAIN."song_album/search", json_encode($arrg));
										$songalbum = json_decode($hhhg, true);
										$salbumid = "";
										if(isset($songalbum['result']))
										{
											foreach($songalbum['result'] as $ress)
											{
												$salbumid = $ress["albumid"];
											}
										}
										
										$albumid = base_convert($salbumid, 10, 16);
										$ahhg = Http::getStr(DOMAIN."album/search?id=".$albumid);
										$ahhg = preg_replace('/[^[:print:]]/', '', $ahhg);
										$album = json_decode($ahhg, true);
										$artistid = base_convert($album["artistid"], 10, 16);
                                        $albumname = $album["title"];
										$album_art = $album["image"];
										
										$imagelink = DOMAIN_STREAM;
										$imagelink .= "images.jpg?id=".$album_art;
										$imagelink .= "&appid=".$appid;
										$imagelink .= "&cookie=".$cookie;
													
										$ahhg = Http::getStr(DOMAIN."artist/search?id=".$artistid);
										$artist = json_decode($ahhg, true);
										$artistname = $artist["name"];
										
										$streamV0 = DOMAIN_STREAM;
										$streamV0 .= "stream.m3u8?id=";
										$streamV0 .= base_convert($res['collectionid'], 10, 16);
										$streamV0 .= "&type=audio&rate=V0&appid=";
										$streamV0 .= $appid;
										$streamV0 .= "&cookie=";
										$streamV0 .= $cookie;
	
										$streamV2 = DOMAIN_STREAM;
										$streamV2 .= "stream.m3u8?id=";
										$streamV2 .= base_convert($res['collectionid'], 10, 16);
										$streamV2 .= "&type=audio&rate=V2&appid=";
										$streamV2 .= $appid;
										$streamV2 .= "&cookie=";
										$streamV2 .= $cookie;
	
										$streamV4 = DOMAIN_STREAM;
										$streamV4 .= "stream.m3u8?id=";
										$streamV4 .= base_convert($res['collectionid'], 10, 16);
										$streamV4 .= "&type=audio&rate=V4&appid=";
										$streamV4 .= $appid;
										$streamV4 .= "&cookie=";
										$streamV4 .= $cookie;
						
                                        /*$chhg = Http::getStr(DOMAIN."collection/search?id=".$collectionid);
                                        $jhhg = json_decode($chhg, true);
                                        
                                        $title = "";
                                        if(count($jhhg) > 2)
                                            $title = $jhhg['title'];
                                        
										$hss = array(
											"id" => $res["id"],
											"track" => intval($track),
											"albumid" => $albumid,
											"collectionid" => $collectionid,
											"length" => intval($length),
											"title" => $title,
											"seo" => $seo,
											"description" => $description,
											"comment" => $audio_colect["comment"],
											"artistid" => $artistid,
											"genreid" => base_convert($album["genreid"], 10, 16),
											"albumname" => $albumname,
											"album_art" => $imagelink,
											"artistname" => $artistname,
											"streamv4" => $streamV4,
											"streamv2" => $streamV2,
											"streamv0" => $streamV0,
											"playlistid" => $playlistid,
										);
										$data[] = $hss;
									}
								}

								$hasiljson = array(
									"result" => $data,
									"error" => null,
									"id" =>	5
								);
								
								echo json_encode($hasiljson);
								return;
							}
						}
                        echo "Not Found";
                        return;
					}
                    echo "Not Found";
                    return;*/
				}
				if($b['method'] == "getAllSongPlaylistByGenre")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$playlistid = "";
					if(isset($p['playlistid']))
						$playlistid = $p['playlistid'];
					$genreid = "";
					if(isset($p['genreid']))
						$genreid = $p['genreid'];
					$skip = 0;
					if(isset($p['skip']))
						$skip = $p['skip'];
					$limit = 0;
					if(isset($p['limit']))
						$limit = $p['limit'];
					//$ex = explode("_", $cookie);	
					//$arr = array(
					//	"cookie" => trim($ex[1])
					//);
					//$hhh = Http::kirimJson(DOMAIN."session/search", json_encode($arr));
					//$ddd = json_decode($hhh, true);
					//if(isset($ddd["result"]))
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					//echo $res;
					//$r = json_decode(json_encode($res), true);
					$r = json_decode($res, true);
					if(is_array($r))
					{
						//if(count($ddd["result"]) > 0)
						if($r['error'] == null)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
							//echo 'isi cek ADALAH '.$cek;
	                        $ada = false;
	                        if($cek == "OK")
	                        {
	                        	$db = Db::init();
								$tun = $db->music_playlists;
								$arrg = array(
									"playlist_id" => new MongoId($playlistid)
								);
								$dp = $tun->find($arrg);
								$arr = array();
								foreach($dp as $dpp)
								{
									$tun = $db->musics;
									$arrg = array(
										"_id" => new MongoId($dpp['music_id'])
									);
									$dp = $tun->findone($arrg);
									if(isset($dp['_id']))
									{
										if(trim($dp['genre_id']) == $genreid)
										{
											$al = $db->albums;
											$alb = $al->findone(
													array("_id" => new MongoId($dp['album_id']))
											);
											
											$art = $db->artists;
											$aat = $art->findone(
												array("_id" => new MongoId($dp['artist_id']))
											);
											
											//$userid = USER_SEMENTARA; // sementara supaya bisa setel
											$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$dp['key']."/index.m3u8";
			                            	$streamV0 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
											
											$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$dp['key']."/index.m3u8";
			                            	$streamV2 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
											
											$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$dp['key']."/index.m3u8";
			                            	$streamV4 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
			
								    		$zipnew  = "http://track.digibeat.co.id/get.zip";
			                            	$zipnew .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
									
											$hss = array(
												"id" => trim($dp["_id"]),
												"track" => intval($dp['track']),
												"albumid" => trim($dp["album_id"]),
												"genreid" => $genreid,
												"collectionid" => trim($dp["_id"]),
												"length" => intval($dp['length']),
												"title" => $dp['title'],
												"seo" => $dp['seo'],
												"description" => $dp['description'],
												"comment" => $dp['description'],
												"artistid" => trim($dp['artist_id']),
												"albumname" => $alb['title'],
												"imageurl" => CDN_IMAGE.'/image/',
			                                	"imagename" => $alb["cover_front"],
												"artistname" => $aat['name'],
												"streamv4" => $streamV4,
												"streamv2" => $streamV2,
												"streamv0" => $streamV0,
												"playlistid" => $playlistid,
												"zip_file" => $zipnew,
											);
											$arr[] = $hss;
										}
									}
								}
								$pp = array(
									'id' => 10,
									'result' => $arr,
									'error' => null
								);
								echo json_encode($pp);
								return;
							}

							
							/*
							$arrg = array(
								"playlistid" => intval(base_convert($playlistid, 16, 10)),
								"genreid" => intval(base_convert($genreid, 16, 10))
							);
							$hhhg = Http::kirimJson(DOMAIN."song_playlist/search", json_encode($arrg));
							$dddg = json_decode($hhhg, true);
							if(is_array($dddg))
							{
								//echo "albumid : ".$albumid;
								
								$db = Db::init();
                                $mus = $db->musics;
								
								$data = array();
								foreach($dddg["result"] as $res)
								{
									$collectionid = base_convert($res['collectionid'], 10, 16);
                                    
                                    $mmus = $mus->findOne(array("key" => $collectionid));
                                    
                                    $title = "";
                                    $seo = "";
                                    $description = "";
                                    $track = 0;
                                    $length = 0;
                                    if($mmus)
                                    {
                                        $title = $mmus['title'];
                                        $seo = $mmus['seo'];
                                        $description = $mmus['description'];
                                        $track = $mmus['track'];
                                        $length = $mmus['length'];
                                    }
                                    
									$ahhg = Http::getStr(DOMAIN."audio_collection/search?id=".$collectionid);
									$audio_colect = json_decode($ahhg, true);
									if(count($audio_colect) > 2)
									{
										$arrg = array(
											"collectionid" => intval(base_convert($collectionid, 16, 10)),
										);
										$hhhg = Http::kirimJson(DOMAIN."song_album/search", json_encode($arrg));
										$songalbum = json_decode($hhhg, true);
										$salbumid = "";
										if(isset($songalbum['result']))
										{
											foreach($songalbum['result'] as $ress)
											{
												$salbumid = $ress["albumid"];
											}
										}
										
										$albumid = base_convert($salbumid, 10, 16);
										$ahhg = Http::getStr(DOMAIN."album/search?id=".$albumid);
										$album = json_decode($ahhg, true);
										$artistid = base_convert($album["artistid"], 10, 16);
                                        $albumname = $album["title"];
										$album_art = $album["image"];
										
										$imagelink = DOMAIN_STREAM;
										$imagelink .= "images.jpg?id=".$album_art;
										$imagelink .= "&appid=".$appid;
										$imagelink .= "&cookie=".$cookie;
													
										$ahhg = Http::getStr(DOMAIN."artist/search?id=".$artistid);
										$artist = json_decode($ahhg, true);
										$artistname = $artist["name"];
										
										$streamV0 = DOMAIN_STREAM;
										$streamV0 .= "stream.m3u8?id=";
										$streamV0 .= base_convert($res['collectionid'], 10, 16);
										$streamV0 .= "&type=audio&rate=V0&appid=";
										$streamV0 .= $appid;
										$streamV0 .= "&cookie=";
										$streamV0 .= $cookie;
	
										$streamV2 = DOMAIN_STREAM;
										$streamV2 .= "stream.m3u8?id=";
										$streamV2 .= base_convert($res['collectionid'], 10, 16);
										$streamV2 .= "&type=audio&rate=V2&appid=";
										$streamV2 .= $appid;
										$streamV2 .= "&cookie=";
										$streamV2 .= $cookie;
	
										$streamV4 = DOMAIN_STREAM;
										$streamV4 .= "stream.m3u8?id=";
										$streamV4 .= base_convert($res['collectionid'], 10, 16);
										$streamV4 .= "&type=audio&rate=V4&appid=";
										$streamV4 .= $appid;
										$streamV4 .= "&cookie=";
										$streamV4 .= $cookie;
                                        
                                        //$chhg = Http::getStr(DOMAIN."collection/search?id=".$collectionid);
                                        //$colect = json_decode($chhg, true);
						
										$hss = array(
											"id" => $res["id"],
											"track" => intval($track),
											"albumid" => $albumid,
											"genreid" => $genreid,
											"collectionid" => $collectionid,
											"length" => intval($length),
											"title" => $title,
											"seo" => $seo,
											"description" => $description,
											"comment" => $audio_colect["comment"],
											"artistid" => $artistid,
											"albumname" => $albumname,
											"album_art" => $imagelink,
											"artistname" => $artistname,
											"streamv4" => $streamV4,
											"streamv2" => $streamV2,
											"streamv0" => $streamV0,
											"playlistid" => $playlistid,
										);
										$data[] = $hss;
									}
								}

								$hasiljson = array(
									"result" => $data,
									"error" => null,
									"id" =>	5
								);
								
								echo json_encode($hasiljson);
								return;
							}*/
						}
					}
				}
				//End By Genre Default
				if($b['method'] == "getAllSongPlaylistByGenreExplore")
				{
					$par = $b['params'];
					$p = $par[0];
					$appid = "";
					if(isset($p['appid']))
						$appid = $p['appid'];
					$cookie = "";
					if(isset($p['cookie']))
						$cookie = $p['cookie'];
					$userid = "";
					if(isset($p['userid']))
						$userid = $p['userid'];
					$playlistid = "";
					if(isset($p['playlistid']))
						$playlistid = $p['playlistid'];
					$genreid = "";
					if(isset($p['genreid']))
						$genreid = $p['genreid'];
					$skip = 0;
					if(isset($p['skip']))
						$skip = $p['skip'];
					$limit = 0;
					if(isset($p['limit']))
						$limit = $p['limit'];

					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))
					{
						if($r['error'] == null)
						{
							$cek = '';
	                    	foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
							
	                        $ada = false;
	                        if($cek == "OK")
	                        {
	                        	$db = Db::init();
								$tun = $db->music_playlists;
								$arrg = array(
									"playlist_id" => new MongoId($playlistid),
									"genre_explore_id" => new MongoId($genreid)
								);
								$dp = $tun->find($arrg)->sort(array('no' => 1));
								$arr = array();
								foreach($dp as $dpp)
								{
									$tun = $db->musics;
									$arrg = array(
										"_id" => new MongoId($dpp['music_id'])
									);
									$dp = $tun->findone($arrg);
									if(isset($dp['_id']))
									{
										$al = $db->albums;
											$alb = $al->findone(
													array("_id" => new MongoId($dp['album_id']))
											);
											
											$art = $db->artists;
											$aat = $art->findone(
												array("_id" => new MongoId($dp['artist_id']))
											);
											
											//$userid = USER_SEMENTARA; // sementara supaya bisa setel
											$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$dp['key']."/index.m3u8";
			                            	$streamV0 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
											
											$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$dp['key']."/index.m3u8";
			                            	$streamV2 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
											
											$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$dp['key']."/index.m3u8";
			                            	$streamV4 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
			
								    		$zipnew  = "http://track.digibeat.co.id/get.zip";
			                            	$zipnew .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
									
											$hss = array(
												"id" => trim($dp["_id"]),
												"track" => intval($dp['track']),
												"albumid" => trim($dp["album_id"]),
												"genreid" => $genreid,
												"collectionid" => trim($dp["_id"]),
												"length" => intval($dp['length']),
												"title" => $dp['title'],
												"seo" => $dp['seo'],
												"description" => $dp['description'],
												"comment" => $dp['description'],
												"artistid" => trim($dp['artist_id']),
												"albumname" => $alb['title'],
												"imageurl" => CDN_IMAGE.'/image/',
			                                	"imagename" => $alb["cover_front"],
												"artistname" => $aat['name'],
												"streamv4" => $streamV4,
												"streamv2" => $streamV2,
												"streamv0" => $streamV0,
												"playlistid" => $playlistid,
												"zip_file" => $zipnew,
											);
											$arr[] = $hss;
									}
								}
								$pp = array(
									'id' => 10,
									'result' => $arr,
									'error' => null
								);
								echo json_encode($pp);
								return;
							}
						}
					}
				}
			}
		}
				
		$p = array(
			'ERROR' => 'Undefined'
		);
		$hasil[] = $p;
		$pp = array(
			'id' => 10,
			'result' => $hasil,
			'error' => 204
		);
		echo json_encode($pp);
	}
	
	public function getallsongplaylistbygenreexplore()
	{
		if(!empty($_POST))
		{
			$appid = "";
			if(isset($_POST['appid']))
				$appid = $_POST['appid'];
			$cookie = "";
			if(isset($_POST['cookie']))
				$cookie = $_POST['cookie'];
			$userid = "";
			if(isset($_POST['userid']))
				$userid = $_POST['userid'];
			$playlistid = "";
			if(isset($_POST['playlistid']))
				$playlistid = $_POST['playlistid'];
			$genreid = "";
			if(isset($_POST['genreid']))
				$genreid = $_POST['genreid'];
			
			$validator = new Validator();
	        $validator->addRule('userid', array('require'));
	        $validator->addRule('cookie', array('require'));
	        $validator->addRule('appid', array('require'));
			$validator->addRule('genreid', array('require'));
			$validator->addRule('playlistid', array('require'));
			
	        $validator->setData(array(
	        	'userid' => $userid,
	            'cookie' => $cookie,
	            'appid' => $appid,
	            'genreid' => $genreid,
	            'playlistid' => $playlistid
	        ));		
			
			if($validator->isValid())
			{
				$cekcookie = helper::cekCookie($userid, $cookie);
				if($cekcookie)
				{
					$db = Db::init();
					$tun = $db->music_playlists;
					$arrg = array(
						"playlist_id" => new MongoId($playlistid),
						"genre_explore_id" => new MongoId($genreid)
					);
					$dp = $tun->find($arrg)->sort(array('no' => 1));
					$arr = array();
					foreach($dp as $dpp)
					{
						$tun = $db->musics;
						$arrg = array(
							"_id" => new MongoId($dpp['music_id'])
						);
						$dp = $tun->findone($arrg);
						if(isset($dp['_id']))
						{
							$al = $db->albums;
								$alb = $al->findone(
										array("_id" => new MongoId($dp['album_id']))
								);
								
								$art = $db->artists;
								$aat = $art->findone(
									array("_id" => new MongoId($dp['artist_id']))
								);
								
								//$userid = USER_SEMENTARA; // sementara supaya bisa setel
								$streamV0 = "http://track.digibeat.co.id/music/m3u8/V0/".$dp['key']."/index.m3u8";
                            	$streamV0 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
								
								$streamV2 = "http://track.digibeat.co.id/music/m3u8/V2/".$dp['key']."/index.m3u8";
                            	$streamV2 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;
								
								$streamV4 = "http://track.digibeat.co.id/music/m3u8/V4/".$dp['key']."/index.m3u8";
                            	$streamV4 .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&appid=".$appid;

					    		$zipnew  = "http://track.digibeat.co.id/get.zip";
                            	$zipnew .= "?userid=".$userid."&songid=".$dp['key']."&cookie=".$cookie."&versi=V4&appid=".$appid;
						
								$hss = array(
									"id" => trim($dp["_id"]),
									"track" => intval($dp['track']),
									"albumid" => trim($dp["album_id"]),
									"genreid" => $genreid,
									"collectionid" => trim($dp["_id"]),
									"length" => intval($dp['length']),
									"title" => $dp['title'],
									"seo" => $dp['seo'],
									"description" => $dp['description'],
									"comment" => $dp['description'],
									"artistid" => trim($dp['artist_id']),
									"albumname" => $alb['title'],
									"imageurl" => CDN_IMAGE.'/image/',
                                	"imagename" => $alb["cover_front"],
									"artistname" => $aat['name'],
									"streamv4" => $streamV4,
									"streamv2" => $streamV2,
									"streamv0" => $streamV0,
									"playlistid" => $playlistid,
									"zip_file" => $zipnew,
								);
								$arr[] = $hss;
						}
					}
					$pp = array(
						'result' => "SUCCESS",
						'data' => $arr,
						'error' => null
					);
					echo json_encode($pp);
					return;
				}		
			}
		}
		
		$pp = array(		
			'result' => "FAILED",
			'data' => array(),
			'error' => null
		);
		echo json_encode($pp);
		return;

	}
	
	private function ambilTgl()
	{
		$a = explode(" ", microtime());
		$b = explode(".", $a[0]);
		$c = substr($b[1], 0, 6);
		$d = $a[1].$c;
		return $d;
	}
}