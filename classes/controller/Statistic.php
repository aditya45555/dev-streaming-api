<?php

class Statistic extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function playcounter()
	{
		$body = @file_get_contents('php://input');
		$b = json_decode($body, true);
		if(is_array($b))
		{
			if(isset($b['method']))
			{
				if($b['method'] == "playStat")
				{
					$appid = "";
					if(isset($b['appid']))
						$appid = $b['appid'];
					$userid = "";
					if(isset($b['userid']))
						$userid = $b['userid'];
					$cookie = "";
					if(isset($b['cookie']))
						$cookie = $b['cookie'];
					
					$curl = new Curl();
					$curl->get('https://sso.dboxid.com/login/cekcookie', array(
							'userid' => $userid,
							'cookie' => $cookie,
					));
					$res = $curl->response;
					$r = json_decode($res, true);
					if(is_array($r))
					{
						if($r['error'] == null)
		                {
		                	$cek = '';
			                foreach($r['result'] as $rt)
							{
								$cek = $rt['cek'];
							}
			                if($cek == "OK")
		                    {
		                    	$par = $b['params'];
								foreach ($par as $d) {
									$db = Db::init();
									$mus = $db->musics;
									$usr = $db->users;
									$sc = $db->statistic_contributors;
									$sp = $db->statistic_contributor_payments;
									$su = $db->statistic_users;
									
									$id = $d['id'];
									$total = $d['total'];
									$count = $d['count'];
									$time = $d['time'];
									
									$mmus = $mus->findOne(array('key'=> $id));
									if (!$mmus)
										$mmus = $mus->findOne(array('_id'=> new MongoId($id)));
												
									$s = $db->statistics;
									$p = array(
										'userid' => $userid,
										'contributor_id' => trim($mmus['contributor_id']),
										'music_id' => $mmus['_id'],
										'total' => intval($total),
										'count' => intval($count),
										'time_created' => intval($time)
									);
									
									$ms = $s->findOne($p);
									if (isset($ms['_id']))
									{
										$hasil = array(
											'id' => 5,
											'error' => null,
											'result' => array('stat' => 'SUKSES')
										);
										echo json_encode($hasil);
										return;
									}
									
									$s->insert($p);
									$statid = trim($p['_id']);
									
									$set = $db->setting_parameters;
									$mset = $set->find();
									$percent = 0;
									foreach ($mset as $v) {
										$percent = $v['percent'];
									}
							
									$percenttotal = round($total * ($percent/100));
									if ($count >= $percenttotal)
									{
										$q = array(
											'contributor_id' => trim($mmus['contributor_id']),
											'music_id' => new MongoId(trim($mmus['_id'])),
											'day' => intval(date('d', time())),
											'month' => intval(date('m', time())),
											'year' => intval(date('Y', time()))
										);
										$msc = $sc->findOne($q);
										if (isset($msc['_id']))
										{
											$totalcount = intval($msc['count']) + 1;
											$p = array(
												'contributor_id' => trim($mmus['contributor_id']),
												'music_id' => new MongoId(trim($mmus['_id'])),
												'count' => intval($totalcount),
												'day' => intval(date('d', $time)),
												'month' => intval(date('m', $time)),
												'year' => intval(date('Y', $time))
											);
											$sc->update(array('_id' => new MongoId($msc['_id'])),array('$set' => $p));
										}
										else
										{
											$totalcount = 1;
											$p = array(
													'contributor_id' => trim($mmus['contributor_id']),
													'music_id' => new MongoId(trim($mmus['_id'])),
													'count' => intval($totalcount),
													'day' => intval(date('d', $time)),
													'month' => intval(date('m', $time)),
													'year' => intval(date('Y', $time))
											);
											$sc->insert($p);
										}
										
										//Statisctic contributor payment
										$msp = $sp->findOne($q);
										if (isset($msp['_id']))
										{
											$totalcount = intval($msp['count']) + 1;
											$p = array(
												'contributor_id' => trim($mmus['contributor_id']),
												'music_id' => new MongoId(trim($mmus['_id'])),
												'count' => intval($totalcount),
												'day' => intval(date('d', $time)),
												'month' => intval(date('m', $time)),
												'year' => intval(date('Y', $time))
											);
											$sp->update(array('_id' => new MongoId($msp['_id'])),array('$set' => $p));
										}
										else
										{
											$totalcount = 1;
											$p = array(
													'contributor_id' => trim($mmus['contributor_id']),
													'music_id' => new MongoId(trim($mmus['_id'])),
													'count' => intval($totalcount),
													'day' => intval(date('d', $time)),
													'month' => intval(date('m', $time)),
													'year' => intval(date('Y', $time))
											);
											$sp->insert($p);
										}
										
										//User
										$q = array(
												'user_id' => $userid,
												'music_id' => new MongoId(trim($mmus['_id'])),
												'day' => intval(date('d', time())),
												'month' => intval(date('m', time())),
												'year' => intval(date('Y', time()))
										);
										$msu = $su->findOne($q);
										if (isset($msu['_id']))
										{
											$totalcount = intval($msu['count']) + 1;
											$p = array(
													'user_id' => $userid,
													'music_id' => new MongoId(trim($mmus['_id'])),
													'count' => intval($totalcount),
													'day' => intval(date('d', $time)),
													'month' => intval(date('m', $time)),
													'year' => intval(date('Y', $time))
											);
											$su->update(array('_id' => new MongoId($msu['_id'])),array('$set' => $p));
										}
										else
										{
											$totalcount = 1;
											$p = array(
													'user_id' => $userid,
													'music_id' => new MongoId(trim($mmus['_id'])),
													'count' => intval($totalcount),
													'day' => intval(date('d', $time)),
													'month' => intval(date('m', $time)),
													'year' => intval(date('Y', $time))
											);
											$su->insert($p);
										}
										
									}	
								}
								
								$hasil = array(
									'id' => 5,
									'error' => null,
									'result' => array('stat' => 'SUKSES')
								);
								echo json_encode($hasil);
								return;
		                    }
						}
					}
				}
			}
		}
		$hasil = array(
			'id' => 5,
			'error' => 204,
		);
		echo json_encode($hasil);
	}
	
}
