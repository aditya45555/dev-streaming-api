<?php
class device_controller extends controller
{
	private function buatKey()
	{
		$config = array(
			"digest_alg" => "sha512",
			"private_key_bits" => 1024,
			"private_key_type" => OPENSSL_KEYTYPE_RSA,
		);
		$res = openssl_pkey_new($config);
		$private_key = '';
		openssl_pkey_export($res, $private_key);
		$details = openssl_pkey_get_details($res);
		$public_key = $details["key"];
		
		$arr = array(
			'private_key' => $private_key,
			'public_key' => $public_key
		);
		return $arr;
	}
	
	public function sendkey()
	{
		if(!empty($_POST))
        {
			$appid='';
			$key='';
			if(isset($_POST['appid']))
				$appid=$_POST['appid'];
			if(isset($_POST['key']))
				$key=$_POST['key'];
			
			$validator = new Validator();
	        $validator->addRule('appid', array('require'));
	        $validator->addRule('key', array('require'));
	        $validator->setData(array(
	        	'appid' => $appid,
	            'key' => $key
	        ));
            
	        if($validator->isValid())
			{
				$db = Db::init();
				$device = $db->devices;
				$dbd = $device->findone(array('appid' => $appid));
				if(isset($dbd['_id']))
				{
					$private_key = $dbd['private_key'];
					$crypted = base64_decode($key);
					$hasil = '';
					for($i=0; $i<strlen($crypted); $i+=128){
						$src = substr($crypted, $i, 128);
						$ret = openssl_private_decrypt($src, $out, $private_key);
						$hasil .= $out;
					}
					
					if(strlen($hasil) > 0)
					{
						$p = array(
							'key' => $hasil,
							'last_time' => time()
						);
						$update = array('$set' => $p);
						$device->update(array('_id' => new MongoId(trim($dbd['_id']))),$update);
						
						$p = array(
							'result' => 'SUCCESS'
						);
						echo json_encode($p);
						return;
					}
				}
			}
		}
		
		$hasil = array(
			'result' => "FAILED"
		);
		echo json_encode($hasil);
		$this->response->status(400);
	}
	
	public function register()
	{
		if(!empty($_POST))
        {
			$version='';
			$hddid='';
			$os='';
			$model='';
			if(isset($_POST['version']))
				$version=$_POST['version'];
			if(isset($_POST['hddid']))
				$hddid=$_POST['hddid'];
			if(isset($_POST['os']))
				$os=$_POST['os'];
			if(isset($_POST['model']))
				$model=$_POST['model'];
			
			$validator = new Validator();
	        $validator->addRule('version', array('require'));
	        $validator->addRule('hddid', array('require'));
	        $validator->addRule('os', array('require'));
			$validator->addRule('model', array('require'));
	        $validator->setData(array(
	        	'version' => $version,
	            'hddid' => $hddid,
	            'os' => $os,
	            'model' => $model
	        ));
            
	        if($validator->isValid())
	        {
				$appid = "";
				$public_key = "";
		    	$db = Db::init();
				$device = $db->devices;
				$dbd = $device->findone(array('hddid' => $hddid));
				if(isset($dbd['_id']))
				{
					$p = array(
						'version' => $version,
						'os' => $os,
						'last_time' => time()
					);
					$update = array('$set' => $p);
					$device->update(array('_id' => new MongoId(trim($dbd['_id']))),$update);
					$appid = $dbd['appid'];
					if(isset($dbd['public_key']))
						$public_key = $dbd['public_key'];
					else
					{
						$arrkey = $this->buatKey();	
						$p = array(
							'private_key' => $arrkey['private_key'],
		       	 			'public_key' => $arrkey['public_key'],
						);
						$update = array('$set' => $p);
						$device->update(array('_id' => new MongoId(trim($dbd['_id']))),$update);
						$public_key = $arrkey['public_key'];
					}
				}
				else 
				{
					$arrkey = $this->buatKey();
					$appid = helper::createCookie();
					$p = array(
						'version' => $version,
		        		'hddid' => $hddid,
		        		'os' => $os,
		       	 		'model' => $model,
		       	 		'time_created' => time(),
		       	 		'last_time' => time(),
		       	 		'appid' => $appid,
		       	 		'private_key' => $arrkey['private_key'],
		       	 		'public_key' => $arrkey['public_key']
					);
					$device->insert($p);
					$public_key = $arrkey['public_key'];
				}
					
				$p = array(
					'result' => 'SUCCESS',
					'appid' => $appid,
					'public_key' => $public_key
				);
				echo json_encode($p);
				return;	
			}
			
		}
			
		$hasil = array(
			'appid' => "",
			'result' => "FAILED"
		);
		echo json_encode($hasil);
		$this->response->status(400);	
	}
}
?>