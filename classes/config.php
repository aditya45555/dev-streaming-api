<?php

class config
{
	public static function get($name)
	{
		$db = Db::init();
		$conf = $db->config;
		$config = $conf->findone();
		if(!isset($config['_id'])) // belum ada config
		{
			$p = array('name' => 'digitekno - Streaming  Api'); // buat default
			$conf->insert($p);
		}
		else { 
			if(isset($config[$name]))
			{
				return $config[$name];
			}
		}
		
		return "";
	}
	
	public static function set($name, $value)
	{
		$db = Db::init();
		$conf = $db->config;
		$config = $conf->findone();
		if(!isset($config['_id'])) // belum ada config
		{
			$p = array('name' => 'digitekno - Streaming  Api'); // buat default
			$conf->insert($p);
		}
		else { 
			$p = array($name => $value);
			$conf->update(array('_id' => new MongoId($config['_id'])), array('$set' => $p));
		}
	}
}
