<?php

class controller
{
	var $css;
	var $js;
	var $request;
	var $response;
	
	public function __construct($request, $response)
	{
		$this->css = array();
		$this->js = array();
		$this->request = $request;
		$this->response = $response;
		$t = config::get('name');
		
		$path = $_SERVER['path_info'];
		
		/*if(!isset($_SESSION['userid']))
		{
			if(strtolower($path) == "/controller/content/fb"||
			strtolower($path) == "/controller/faq/getcontent"||
			strtolower($path) == "/controller/content/aboutus"||
			strtolower($path) == "/controller/content/masalayanan"||
			strtolower($path) == "/controller/content/kebijakan"){
				//$response->cookie("DIGIBEAT", helper::randomKey(), time() + 3600);
				return;
			}
			
			$response->status(302);
			$response->header("Location", "controller/login/index");
				
		}*/
		
		
		/*if(empty($_COOKIE))
		{
			//$response->cookie("DIGIBEAT", helper::randomKey(), time() + 3600);
			$response->status(302);
			$response->header("Location", "/login/index");
		}
		if(isset($_COOKIE['DIGIBEAT']))
		{
			$db = Db::init();
			$cookie = $db->cookies;
			$dbc = $cookie->findone(array('cookie' => $_COOKIE['DIGIBEAT']));
			if(!isset($dbc['_id']))
			{
				$response->status(302);
				$response->header("Location", "/login/index");
				return;
			}
		}
		else {
			$response->status(302);
			$response->header("Location", "/login/index");
			return;
		}*/
		
		$response->header("Cache-Control", "no-cache, no-store, must-revalidate");
		$response->header("Pragma", "no-cache");
		$response->header("Expires", "0");
	}
	
	protected function before()
	{
		
	}
	
	public function sendEmail($sub,$data,$address)
	{
		$mailman = new CkMailMan();
		//  Any string argument automatically begins the 30-day trial.
		$success = $mailman->UnlockComponent('WILLAWMAILQ_V6R4qoSG0M4R');
		if ($success != true) {
		    print $mailman->lastErrorText() . "\n";
		    return;
		}
		
		$accuser = "admin@movie.dboxid.com";
		$accpass = "c1mah1";
		
		//  Set the SMTP server.
		$mailman->put_SmtpHost('192.168.89.39');
		
		//  Set the SMTP login/password (if required)
		$mailman->put_SmtpUsername($accuser);
		$mailman->put_SmtpPassword($accpass);
		$mailman->put_SmtpPort(25);
		$mailman->put_StartTLS(true);
		
		//  Create a new email object
		$email = new CkEmail();
		$email->put_Subject($sub);
		$email->AddHtmlAlternativeBody($data);
		$email->put_From('admin'.' < admin@movie.dboxid.com >');
		$email->AddTo($address, $address);
		//  To add more recipients, call AddTo, AddCC, or AddBcc once per recipient.
		
		//  Call SendEmail to connect to the SMTP server and send.
		//  The connection (i.e. session) to the SMTP server remains
		//  open so that subsequent SendEmail calls may use the
		//  same connection.
		$success = $mailman->SendEmail($email);
		if ($success != true) {
		    print $mailman->lastErrorText() . "\n";
		    return;
		}
		
		//  Some SMTP servers do not actually send the email until
		//  the connection is closed.  In these cases, it is necessary to
		//  call CloseSmtpConnection for the mail to be  sent.
		//  Most SMTP servers send the email immediately, and it is
		//  not required to close the connection.  We'll close it here
		//  for the example:
		$success = $mailman->CloseSmtpConnection();
		if ($success != true) {
		    print 'Connection to SMTP server not closed cleanly.' . "\n";
			return;
		}	
    }
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
		(include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}
	
	protected function redirect($page)
	{
		header( 'Location: '.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{
		$css[] = "/public/css/bootstrap.min.css";
		$css[] = "/public/css/bootstrap-theme.min.css";
		$css[] = "/public/css/theme.css";
				
		$js[] = "/public/js/jquery-1.11.2.min.js";					
		$js[] = "/public/js/bootstrap.min.js";
		$js[] = "/public/js/ie10-viewport-bug-workaround.js";
		
		$m = explode("/", $view);
		$mm = explode(".", $m[1]);
		$method = $mm[0];
		
		$topmenu = $this->getView(DOCVIEW."template/topmenu.php", array('controller' => $group, 'method' => $method));
		//$leftmenu = $this->getView(DOCVIEW."template/leftmenu.php", array('controller' => $group, 'method' => $method));
		
		//$a["shorcut"] = $shorcut;
		//$var = array_merge($var, $a);
		//$var = array_merge($var, $this->dta);
		
		$content = $this->getView(DOCVIEW.$view, $var);
		$controller = $group;
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);
		$playlist = $this->playlist;		
		include(DOCVIEW."template/template.php");
	}
}
?>