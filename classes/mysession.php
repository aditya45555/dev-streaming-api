<?php

class mysession
{
	var $cookie;
	
	public function __construct($cookie)
	{
		$this->cookie = $cookie;
		$db = Db::init();
		$sess = $db->sessions;
		$dta = $sess->findone(array('cookie' => $cookie));
		if(isset($dta['_id']))
		{
			foreach($dta as $key => $value)
			{
				if($key != '_id')
					$_SESSION[$key] = $value;
			}
		}
	}
	
	public function saveSession($key, $value)
	{
		$db = Db::init();
		$sess = $db->sessions;
		$dta = $sess->findone(array('cookie' => $this->cookie));
		if(!isset($dta['_id']))
		{
			$p = array(
				$key => $value,
				'cookie' => $this->cookie
			);
			$sess->insert($p);
		}
		else {
			$p = array(
				$key => $value,
			);
			$newdata = array('$set' => $p);
			$sess->update(array("cookie" => new MongoId($dta['_id'])), $newdata);
		}
	}
}